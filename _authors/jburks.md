---
short_name: jburks
name: jburks
position: Chief Editor
twitter: https://help.twitter.com/en/using-twitter/how-to-tweet-a-link
facebook: https://www.facebook.com/

image: /assets/img/160x160/img3.jpg

---
Jill is an avid fruit grower based in the south of France.