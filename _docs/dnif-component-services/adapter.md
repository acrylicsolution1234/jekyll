---
title: Adapter 
permalink: /docs/Adapter/

---

# Adapter

* Login to your **Adapter** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the Adapter services status using following command,

```text
$ supervisorctl 

adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         RUNNING   pid 304, uptime 2 days, 22:24:30
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

All is well if you see all services in the `RUNNING` state, however if the daemon is in the error state or has failed you are likely to see the state set to `FATAL.`

### Adapter component services and their works

* **config\_reporter**

This service will provide the devices and parser information from Datastore to the Adapter.

* **event\_parser\_processor**

This service receives the logs from various DNIF listeners, the **event\_parser\_processor** service is responsible for to parse and enrich those data and ingested in the Big data store.

* **httplistener and httpslistener**

This service will accept the data in a JSON format. This service is also used to accept the data from the Detector component.

* **netflowv5**

This service is work as a listener specially for the CISCO networking devices.

* **syslog\_listener\_tcp**

This service will receive the connection-oriented packets from the various log sources on port 514.

* **syslog\_listener\_udp**

This service will receive the datagram packets from the various log sources on port 514.

* **cef\_listerner\_udp**

This service will receive logs from CEF forwarders and extract valid log information and send it to EPP for further processing.

* **adinline**

This service will responsible for moving those events detected by inline to Datastore.

* **syncdump**

If the datastore get failed or loss the connectivity with the adapter, this service will be responsible to get the data from Adapter which are stored in json format and send it to the Datastore.

* **sshd**

This service will responsible for to login your docker container, you can connect your container on port 826.

\*\*\*\*

