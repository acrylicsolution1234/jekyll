---
title: Correlator 
permalink: /docs/Correlator/
---

# Correlator

* Login to your **Correlator** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the Correlator services status using following command,

```text
$ supervisorctl

celerycr                         RUNNING   pid 293, uptime 17 days, 17:56:01
crinline                         RUNNING   pid 290, uptime 17 days, 17:56:01
datastoreapi                     RUNNING   pid 284, uptime 17 days, 17:56:01
ilicrapi                         RUNNING   pid 590, uptime 17 days, 17:55:10
ilicrscheduler                   RUNNING   pid 265, uptime 17 days, 17:56:01
notifemail                       RUNNING   pid 292, uptime 17 days, 17:56:01
perf                             RUNNING   pid 268, uptime 17 days, 17:56:01
sshd                             RUNNING   pid 277, uptime 17 days, 17:56:01
```

### Correlator component services and their works <a id="adapter-component-services-and-their-works"></a>

* **celerycr**

This service is also known as "Task Executer". It executes all the scheduled task.

* **crinline**

This service will receive the inline events from Datastore and take actions on them.

* **ilicrapi**

This service is used to manage transport information from Correlator to Datastore.

* **ilicrscheduler**

This service schedules the reports and alerts and manages the queue.

* **notifemail**

This service is used for sending reports and alerts on designated address.

* **perf**

This service provides the information and statistics of the execution time of the queries.

* **sshd**

This service will responsible for to login your docker container, you can connect your container on port 826.

