---
title: Datastore 
permalink: /docs/Datastore/
---
# Datastore

* Login to your **Datastore** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the Datastore services status using following command,

```text
$ supervisorctl 

agent79                          RUNNING   pid 278, uptime 0:01:08
celeryds                         RUNNING   pid 269, uptime 0:01:08
dataprocessor:01                 RUNNING   pid 272, uptime 0:01:08
dataprocessor:02                 RUNNING   pid 270, uptime 0:01:08
dataprocessor:03                 RUNNING   pid 271, uptime 0:01:08
dataprocessor:04                 RUNNING   pid 273, uptime 0:01:08
datastoreapi                     RUNNING   pid 284, uptime 0:01:08
din                              RUNNING   pid 437, uptime 0:01:05
dsinline                         RUNNING   pid 285, uptime 0:01:08
error_collector                  RUNNING   pid 266, uptime 0:01:08
ilidsapi                         RUNNING   pid 276, uptime 0:01:08
importstore                      RUNNING   pid 291, uptime 0:01:08
ingestor                         RUNNING   pid 279, uptime 0:01:08
nameserver                       RUNNING   pid 294, uptime 0:01:08
notifemail                       RUNNING   pid 292, uptime 0:01:08
sshd                             RUNNING   pid 277, uptime 0:01:08
```

All is well if you see all services in the `RUNNING` state, however if the daemon is in the error state or has failed you are likely to see the state set to `FATAL.`

### Datastore component services and their works <a id="adapter-component-services-and-their-works"></a>

* **agent79**

This service manages reports building in pdf, xlsx, csv and json as a result of the export queries and the reports.

* **dataprocessor**

dataprocessor is a service that receive logs from redis queue and index to the database.

* **datastoreapi**

This service is generally used for running and connect the console, it will take the data from the datastore and will provide the output on the console.

* **din**

din is a service that collect system metrics data from Adapter, Datastore and correlator servers and store in to the `sysdata` index

* **error\_collector**

This service will collect the logs with log level errors, i.e. it will collect the centralized logs from all the services.

* **ilidsapi**

This service manages the communication from the Datastore to Adapter and from Datastore to Correlator.

* **importstore**

This service is responsible for importing static files into the Datastore.

* **ingestor**

Ingestor is a service that work as a receiver, it receives logs on `port 8352/TCP` from the Adapter.

* **nameserver**

This service is also known as “address manager”. It manages the addresses of all nodes in the cluster.

* **notifemail**

This service is used for sending reports and alerts on designated address.

* **celeryds**

This service is also known as a "task manager". It works for the query execution in pipelining\(f1, f2, f3,..\).

* **dsinline**

This service will transport inline event\(alerts\) from Datastore to Correlator.

* **sshd**

This service will responsible for to login your docker container, you can connect your container on port 826.

\*\*\*\*



