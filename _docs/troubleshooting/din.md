---
title: Din 
permalink: /docs/Troubleshooting/Din/
section: Troubleshooting
---
# din

## What is `din`

`din` is a service that collect system metrics data from Adapter, Datastore and correlator servers and store in to the `sysdata` index.This service runs on the Datastore \(DS\) and is also available on the All-in-One \(A10\) instances.

## Working with `din`

Let's understand the basic operations of the `din`.

## **Check if** `din` **is running**

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Check for the `din` service status using following command,

```text
$ supervisorctl 
agent79                          RUNNING   pid 278, uptime 0:01:08
celeryds                         RUNNING   pid 269, uptime 0:01:08
dataprocessor:01                 RUNNING   pid 272, uptime 0:01:08
dataprocessor:02                 RUNNING   pid 270, uptime 0:01:08
dataprocessor:03                 RUNNING   pid 271, uptime 0:01:08
dataprocessor:04                 RUNNING   pid 273, uptime 0:01:08
datastoreapi                     RUNNING   pid 284, uptime 0:01:08
din                              RUNNING   pid 437, uptime 0:01:05
dsinline                         RUNNING   pid 285, uptime 0:01:08
error_collector                  RUNNING   pid 266, uptime 0:01:08
ilidsapi                         RUNNING   pid 276, uptime 0:01:08
importstore                      RUNNING   pid 291, uptime 0:01:08
ingestor                         RUNNING   pid 279, uptime 0:01:08
nameserver                       RUNNING   pid 294, uptime 0:01:08
notifemail                       RUNNING   pid 292, uptime 0:01:08
sshd                             RUNNING   pid 277, uptime 0:01:08
```

## Checking the **`din`** logs

You could check the for log events from the host systems itself. When the daemon works as designed, it will give out below informational messages.

```text
$ tail -f /dnif/{Deploy-Key}/log/din.log
2019-02-15 10:46:15,852 INFO din: log level set to 1
2019-02-15 11:04:57,899 INFO din: log level set to 1
```

## **Restart** **`din`**

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Restart the `din` service status using `restart din`command,

```text
$ supervisorctl 
agent79                          RUNNING   pid 278, uptime 0:01:08
celeryds                         RUNNING   pid 269, uptime 0:01:08
dataprocessor:01                 RUNNING   pid 272, uptime 0:01:08
dataprocessor:02                 RUNNING   pid 270, uptime 0:01:08
dataprocessor:03                 RUNNING   pid 271, uptime 0:01:08
dataprocessor:04                 RUNNING   pid 273, uptime 0:01:08
datastoreapi                     RUNNING   pid 284, uptime 0:01:08
din                              RUNNING   pid 437, uptime 0:01:05
dsinline                         RUNNING   pid 285, uptime 0:01:08
error_collector                  RUNNING   pid 266, uptime 0:01:08
ilidsapi                         RUNNING   pid 276, uptime 0:01:08
importstore                      RUNNING   pid 291, uptime 0:01:08
ingestor                         RUNNING   pid 279, uptime 0:01:08
nameserver                       RUNNING   pid 294, uptime 0:01:08
notifemail                       RUNNING   pid 292, uptime 0:01:08
sshd                             RUNNING   pid 277, uptime 0:01:08

supervisor> restart din
din: stopped
din: started

supervisor> exit
```

## **How to figure if `din` service has stopped functioning**

We can diagnosis the issue with the help of din service ****logs.

### **Unable to add items to database**

If Database is not in running or is in error state, `din`reports following error in the logs,

```text
$ tail -f /dnif/{Deploy-Key}/log/din.log
NewConnectionError: <urllib3.connection.HTTPConnection object at 0x7fefc82bfe50>: Failed to establish a new connection: [Errno 111] Connection refused
2019-02-14 10:15:02,703 ERROR din: PWK4RI - Unable to add items to database <<ConnectionError(<urllib3.connection.HTTPConnection object at 0x7fefc82bfe50>: Failed to establish a new connection: [Errno 111] Connection refused) caused by: NewConnectionError(<urllib3.connection.HTTPConnection object at 0x7fefc82bfe50>: Failed to establish a new connection: [Errno 111] Connection refused)>>
2019-02-14 10:15:02,703 ERROR din: PWK4RI - Unable to add items to database <<ConnectionError(<urllib3.connection.HTTPConnection object at 0x7fefc82bfe50>: Failed to establish a new connection: [Errno 111] Connection refused) caused by: NewConnectionError(<urllib3.connection.HTTPConnection object at 0x7fefc82bfe50>: Failed to establish a new connection: [Errno 111] Connection refused)>>
```

If database is not running or `sysdata` index is in RED state then you can receive above error in the din logs and you can not fetch system metrics data from DNIF console.

**How to recover:**

Check the [database status](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYyzZcjQhSc-1bt9swp/managing-your-deployment/managing-indexes) and recover if its not running or is in RED state. After resolving database issues, restart `din` and check the `din` logs. 

