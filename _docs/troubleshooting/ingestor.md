---
title: ingestor 
permalink: /docs/Troubleshooting/Ingestor/
description: ingestor - log receives from Adapter
section: Troubleshooting
---

# ingestor

## What is `ingestor`

ingestor is a service that work as a receiver, it receives logs on `port 8352/TCP` from the Adapter. This service runs on the Datastore \(DS\) and is also available on the All-in-One \(A10\) instances.

## Working with `ingestor`

Let's understand the basic operations of the `ingestor`.

## **Check if** `ingestor` **is running**

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Check for the `ingestor` service status using following command,

```text
$ supervisorctl 
agent79                          RUNNING   pid 278, uptime 0:01:08
celeryds                         RUNNING   pid 269, uptime 0:01:08
dataprocessor:01                 RUNNING   pid 272, uptime 0:01:08
dataprocessor:02                 RUNNING   pid 270, uptime 0:01:08
dataprocessor:03                 RUNNING   pid 271, uptime 0:01:08
dataprocessor:04                 RUNNING   pid 273, uptime 0:01:08
datastoreapi                     RUNNING   pid 284, uptime 0:01:08
din                              RUNNING   pid 437, uptime 0:01:05
dsinline                         RUNNING   pid 285, uptime 0:01:08
error_collector                  RUNNING   pid 266, uptime 0:01:08
ilidsapi                         RUNNING   pid 276, uptime 0:01:08
importstore                      RUNNING   pid 291, uptime 0:01:08
ingestor                         RUNNING   pid 279, uptime 0:01:08
nameserver                       RUNNING   pid 294, uptime 0:01:08
notifemail                       RUNNING   pid 292, uptime 0:01:08
sshd                             RUNNING   pid 277, uptime 0:01:08
```

## Checking the`ingestor` logs

You could check the for log events from the host systems itself. When the daemon works as designed, it will give out informational messages, each message will give out the time taken required for process data to DataQueue.

```text
$ tail -f /dnif/{Deploy-Key}/log/ingestor.log 
2019-02-15 10:09:01,271 INFO ingestor: Compression in percentage 73.1086420901
2019-02-15 10:09:01,292 INFO ingestor: Time taken to process data 0.0238680839539
2019-02-15 10:09:01,294 INFO ingestor: Compression in percentage 72.5032479343
2019-02-15 10:09:01,297 INFO ingestor: Time taken to process data 0.00466513633728
2019-02-15 10:09:06,263 INFO ingestor: Compression in percentage 72.8791051637
2019-02-15 10:09:06,265 INFO ingestor: Time taken to process data 0.00413203239441
```

## **Restart** `ingestor`

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Restart the dataprocessor service status using `restart dataprocessor:*`command,

```text
$ supervisorctl 
agent79                          RUNNING   pid 278, uptime 0:01:08
celeryds                         RUNNING   pid 269, uptime 0:01:08
dataprocessor:01                 RUNNING   pid 272, uptime 0:01:08
dataprocessor:02                 RUNNING   pid 270, uptime 0:01:08
dataprocessor:03                 RUNNING   pid 271, uptime 0:01:08
dataprocessor:04                 RUNNING   pid 273, uptime 0:01:08
datastoreapi                     RUNNING   pid 284, uptime 0:01:08
din                              RUNNING   pid 437, uptime 0:01:05
dsinline                         RUNNING   pid 285, uptime 0:01:08
error_collector                  RUNNING   pid 266, uptime 0:01:08
ilidsapi                         RUNNING   pid 276, uptime 0:01:08
importstore                      RUNNING   pid 291, uptime 0:01:08
ingestor                         RUNNING   pid 279, uptime 0:01:08
nameserver                       RUNNING   pid 294, uptime 0:01:08
notifemail                       RUNNING   pid 292, uptime 0:01:08
sshd                             RUNNING   pid 277, uptime 0:01:08

supervisor> restart ingestor 
ingestor: stopped
ingestor: started

supervisor> exit
```

## **How to figure if `ingestor` service has stopped indexing data**

There are following scenario in which`ingestor`service stopped indexing data, we can diagnosis the issue with the help of `ingestor` logs.

### **If `ingestor` is in error state**

If **ingestor** is in error state, `ingestor` reports following error in the logs,

```text
$ tail -f /dnif/{Deploy-Key}/log/ingestor.log 
2019-02-13 14:31:00,376 INFO ingestor: Time taken to process data 0.00329899787903
2019-02-13 14:31:05,263 INFO ingestor: Compression in percentage 72.4384518806
2019-02-13 14:31:05,264 CRITICAL ingestor: Indexing has been stopped , please contact DNIF support
2019-02-13 14:31:05,273 INFO ingestor: Time taken to process data 0.0115721225739
2019-02-13 14:31:05,367 INFO ingestor: Compression in percentage 73.4310527041
2019-02-13 14:31:05,367 CRITICAL ingestor: Indexing has been stopped , please contact DNIF support
```

If you have received "Indexing has been stopped" error in the ingestor logs, perform following checks on Data store or A10 server.

Check for the license files are available or not on the DNIF Datastore/A10 server using following command,

```text
$ cd /dnif/{Deploy-Key}/LICENSE/
$ ls
license  signature.bin
```

If the license files were not present in the LICENSE folder, due to you are getting "Indexing has been stopped" error in the ingestor logs, click here to [apply license](https://docs.dnif.it/admin-guide/~/drafts/-LYyzZcjQhSc-1bt9swp/primary/configuration/installing-licenses).

If license files were present in the LICENSE folder, check for the ****data usage limit of the current month is exceeded beyond limit.

For to check current month data usage, login into DNIF console and click on **MANAGEMENT &gt;&gt; USAGE** tab.

![](https://lh6.googleusercontent.com/uY80dgJhIp6tEEoDtU9ZuvCVjlANb7w3wVcusXntjj6pO5EYPbn8uN4VWfrQN9sNMZVgrgeGn1uaCPo3Ltcmt5WbqimOh1U5-reb-Nml2cBjgQVPUogbf-6zd-koI7x3TIe2BIKURcstQ1z_Yg)

If data usage limit of the current month is exceeded beyond limit, due to you are getting "Indexing has been stopped" error and logs are not indexing in the Database.

**How to recover**

If your current month data usage limit is extended beyond limit, you need to contact DNIF support to extend monthly license limit.

Apply the upgraded license provided by DNIF support on Datastore or A10 server, click here to [apply license](https://docs.dnif.it/admin-guide/~/drafts/-LYyzZcjQhSc-1bt9swp/primary/configuration/installing-licenses).

  


