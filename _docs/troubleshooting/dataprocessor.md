---
title: dataprocessor 
permalink: /docs/Troubleshooting/Dataprocessor/
description: dataprocessor - Indexing data to the database.
section: Troubleshooting
---

# dataprocessor

## What is `dataprocessor`

dataprocessor is a service that receive logs from redis queue and index to the database. This service runs on the Datastore \(DS\) and is also available on the All-in-One \(A10\) instances.

## Working with `dataprocessor`

Let's understand the basic operations of the `dataprocessor`.

## **Check if** **`dataprocessor`** **is running**

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Check for the `dataprocessor` service status using following command,

```text
$ supervisorctl 
agent79                          RUNNING   pid 278, uptime 0:01:08
celeryds                         RUNNING   pid 269, uptime 0:01:08
dataprocessor:01                 RUNNING   pid 272, uptime 0:01:08
dataprocessor:02                 RUNNING   pid 270, uptime 0:01:08
dataprocessor:03                 RUNNING   pid 271, uptime 0:01:08
dataprocessor:04                 RUNNING   pid 273, uptime 0:01:08
datastoreapi                     RUNNING   pid 284, uptime 0:01:08
din                              RUNNING   pid 437, uptime 0:01:05
dsinline                         RUNNING   pid 285, uptime 0:01:08
error_collector                  RUNNING   pid 266, uptime 0:01:08
ilidsapi                         RUNNING   pid 276, uptime 0:01:08
importstore                      RUNNING   pid 291, uptime 0:01:08
ingestor                         RUNNING   pid 279, uptime 0:01:08
nameserver                       RUNNING   pid 294, uptime 0:01:08
notifemail                       RUNNING   pid 292, uptime 0:01:08
sshd                             RUNNING   pid 277, uptime 0:01:08
```

## Checking the **`dataprocessor`** logs

You could check the for log events from the host systems itself. When the daemon works as designed, it will give out below informational messages.

```text
$ tail -f /dnif/{Deploy-Key}/log/DataProcessor.log
2019-02-14 13:24:03,855 INFO dataprocessor: log level set to 1
2019-02-14 13:24:03,870 INFO dataprocessor: log level set to 1
2019-02-14 13:24:03,861 INFO dataprocessor: log level set to 1
2019-02-14 13:24:03,863 INFO dataprocessor: log level set to 1
```

## **Restart** **`dataprocessor`**

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Restart the dataprocessor service status using `restart dataprocessor:*`command,

```text
$ supervisorctl 
agent79                          RUNNING   pid 278, uptime 0:01:08
celeryds                         RUNNING   pid 269, uptime 0:01:08
dataprocessor:01                 RUNNING   pid 272, uptime 0:01:08
dataprocessor:02                 RUNNING   pid 270, uptime 0:01:08
dataprocessor:03                 RUNNING   pid 271, uptime 0:01:08
dataprocessor:04                 RUNNING   pid 273, uptime 0:01:08
datastoreapi                     RUNNING   pid 284, uptime 0:01:08
din                              RUNNING   pid 437, uptime 0:01:05
dsinline                         RUNNING   pid 285, uptime 0:01:08
error_collector                  RUNNING   pid 266, uptime 0:01:08
ilidsapi                         RUNNING   pid 276, uptime 0:01:08
importstore                      RUNNING   pid 291, uptime 0:01:08
ingestor                         RUNNING   pid 279, uptime 0:01:08
nameserver                       RUNNING   pid 294, uptime 0:01:08
notifemail                       RUNNING   pid 292, uptime 0:01:08
sshd                             RUNNING   pid 277, uptime 0:01:08

supervisor> restart dataprocessor:*
dataprocessor:02: stopped
dataprocessor:03: stopped
dataprocessor:01: stopped
dataprocessor:04: stopped
dataprocessor:02: started
dataprocessor:03: started
dataprocessor:01: started
dataprocessor:04: started

supervisor> exit
```

## **How to figure if `dataprocessor` service has stopped functioning**

There are following scenarios in which `dataprocessor` service stopped functioning, we can diagnosis the issue with the help of dataprocessor logs.

* `redis-server` is not running or is in error state.
* dataprocessor unable to add item to database.

### **If redis-server is not running or is in error state**

If redis-server is not running or is in error state, `dataprocessor` reports following error in the logs,

```text
$ tail -f /dnif/{Deploy-Key}/log/DataProcessor.log
2019-02-15 11:09:16,290 ERROR dataprocessor: IUFOPM - Unable to process item from queue  <<Error 111 connecting to localhost:6379. Connection refused.>>
2019-02-15 11:09:16,291 ERROR dataprocessor: IUFOPM - Unable to process item from queue  <<Error 111 connecting to localhost:6379. Connection refused.>>
2019-02-15 11:09:21,165 ERROR dataprocessor: IUFOPM - Unable to process item from queue  <<Error 111 connecting to localhost:6379. Connection refused.>>
2019-02-15 11:09:21,165 ERROR dataprocessor: IUFOPM - Unable to process item from queue  <<Error 111 connecting to localhost:6379. Connection refused.>>
2019-02-15 11:09:21,165 ERROR dataprocessor: IUFOPM - Unable to process item from queue  <<Error 111 connecting to localhost:6379. Connection refused.>>
```

**How to recover:**

Check the [redis-server](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYfL0jRMfltEqFzCMC1/troubleshooting/sysloglistenerudp) status and recover if its not running or is in error state.  
After resolving `redis-server` issues, restart `dataprocessor` and check the `dataprocessor` logs.  


### **dataprocessor unable to add item to database**

If Database is not in running or is in error state, `dataprocessor`reports following  error in the logs,

```text
$ tail -f /dnif/{Deploy-Key}/log/DataProcessor.log
NewConnectionError: <urllib3.connection.HTTPConnection object at 0x7f6d44b86610>: Failed to establish a new connection: [Errno 111] Connection refused
2019-02-15 07:29:56,139 ERROR dataprocessor: IUFOPM - Unable to process item from queue  <<DB not responding>>
2019-02-15 07:29:56,139 ERROR dataprocessor: IUFOPM - Unable to process item from queue  <<DB not responding>>
```

**How to recover:**

Check the [database status](https://docs.dnif.it/admin-guide/troubleshooting/recover-data-index) and recover if its not running or is in RED state. After resolving database issues, restart `dataprocessor` and check the `dataprocessor` logs. 

