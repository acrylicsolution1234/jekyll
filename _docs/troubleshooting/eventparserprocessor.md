---
title:  event\_parser\_processor 
permalink: /docs/Troubleshooting/Event\_Parser\_Processor/
section: Troubleshooting
---
# event\_parser\_processor

## **What is** `event_parser_processor`

**event\_parser\_processor** service receives the logs from various DNIF listeners, the **event\_parser\_processor** service is responsible for to parse and enrich those data and ingested in the Big data store.

## Working with `event_parser_processor` 

Let's understand the basic operations of the `event_parser_processor`.

## **Check if** `event_parser_processor` **is running** 

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Check for the `event_parser_processor` service status using following command,

```text
$ supervisorctl 
adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

## **Checking the** `event_parser_processor` **logs**

You could check the for log events from the host systems itself. When the daemon works as designed, it will give out information regarding the parse/enrich/ingestion queue including data ingestion status to the Data Store.

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log
2019-02-05 13:34:44,190 INFO Parser: {'outqueue': 12, 'redisq': 234}
2019-02-05 13:34:44,198 INFO Parser: Server replied OK - tcp://172.16.10.56:8352
2019-02-05 13:34:49,190 INFO Parser: {'outqueue': 1, 'redisq': 34}
2019-02-05 13:34:49,194 INFO Parser: Server replied OK - tcp://172.16.10.56:8352
```

If the data flow is normal then output looks similar to the above-given snapshot, in the above scenarios the EventParserProcessor service is running properly.

## **Restart** `event_parser_processor` 

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Restart the SyslogListenerUDP service status using`restart event_parser_processor`command.

```text
$ supervisorctl 
adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         RUNNING   pid 389, uptime 2 days, 22:24:30
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30

supervisor> restart events_parser_processor:* 
events_parser_processor:01: stopped
events_parser_processor:02: stopped
events_parser_processor:01: started
events_parser_processor:02: started

supervisor> exit
```

## **How to figure if event\_parser\_processor service has crashed/not functioning**

There are following scenarios in which `event_parser_processor` service wont functioning, we can diagnosis the issue with the help of EventParserProcessor logs.

* `outqueue` is getting high
* `redisq` is getting high
* `redis-server` is not running or is in error state
* `Event transport` process in defunct state

### **If outqueue is getting high**

If outqueue is getting high, means the Adapter not able to ingest the logs at Datastore, following is the symptom in which the outqueue is getting high.

#### **Adapter and Datastore communication failed**

Due to Adapter and Datastore communication failed, outqueue is getting high on Adapter server and **event\_parser\_processor** has stopped processing logs to the ingestor.

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log
2019-02-06 09:49:17,216 INFO Parser: {'outqueue': 314256, 'redisq': 324}
2019-02-06 09:49:20,723 ERROR Parser: Failed to send data at tcp://172.16.10.56:8352 
2019-02-06 09:49:20,726 ERROR Parser: Failed to send data at tcp://172.16.10.56:8352 
2019-02-06 09:49:20,728 WARNING Parser: Trying for 1 time
2019-02-06 09:49:20,765 WARNING Parser: File dump created with name 1549446560_762003.json
2019-02-06 09:49:20,768 ERROR Parser: FADZC5 - DS communication failed <<1>>
2019-02-06 09:49:20,768 ERROR Parser: FADZC5 - DS communication failed <<1>>
2019-02-06 09:49:25,768 INFO Parser: {'outqueue': 314632, 'redisq': 437}
```

In above **EventParserProcessor** logs Adapter failed to send data at Datastore, outqueue data will be dumped in the json file which is created on the Adapter server.

**Resolution**:

Check for the connectivity between Adapter and Datastore, ingestor service is responsible for to receive logs from Adapter using port 8352.

Check the connectivity using following command,

```text
telnet <DS-IP> 8352
Trying <DS-IP>...
telnet: Unable to connect to remote host: Connection refused
```

If you are not able to connect the Datastore on port 8352, you need to restart ingestor service on Datastore.

```text
$ supervisorctl
supervisor> restart ingestor 
ingestor: stopped
ingestor: started
supervisor> exit
```

After restarting the ingestor service, check the **EventParserProcessor** logs using following command,

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log
2019-02-13 12:43:45,181 INFO Parser: {'outqueue': 229, 'redisq': 0}
2019-02-13 12:43:45,191 INFO Parser: Server replied OK - tcp://172.16.10.56:8352
2019-02-13 12:43:50,111 INFO Parser: {'outqueue': 244, 'redisq': 0}
2019-02-13 12:43:50,118 INFO Parser: Server replied OK - tcp://172.16.10.56:8352
2019-02-13 12:43:50,181 INFO Parser: {'outqueue': 246, 'redisq': 0}
2019-02-13 12:43:50,191 INFO Parser: Server replied OK - tcp://172.16.10.56:8352
```

In above **EventParserProcessor** logs Adapter ingest data at Datastore using port 8352.

### **If redisq is getting high**

If redisq is getting high, means _\*\*_the system not able to process the data to outqueue, following are the symptoms in which the redisq is high.

#### **EPS is high**

The function of ingestor is to ingest the data in the database, the speed of the data which is being received is high, then the redisq value increases and it will take time to ingest the queue.

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log    
2019-02-07 05:33:14,224 INFO Parser: {'outqueue': 1233, 'redisq': 232452}
2019-02-07 05:33:14,236 INFO Parser: Server replied OK - tcp://172.16.10.56:8352
2019-02-07 05:33:19,224 INFO Parser: {'outqueue': 1332, 'redisq': 424242}
2019-02-07 05:33:19,229 INFO Parser: Server replied OK - tcp://172.16.10.56:8352
2019-02-07 05:33:24,224 INFO Parser: {'outqueue': 1430, 'redisq': 544224}
2019-02-07 05:33:24,233 INFO Parser: Server replied OK - tcp://172.16.10.56:8352
```

In above **EventParserProcessor** logs redisq is getting high and speed of the data ingestion is less than to the logs are being received.

**How to recover**:

Check the log sources on the console, which is forwarded events with high EPS and suppress those events if required.

After suppressing the required log events, check the EventParserProcessor logs for queue monitoring.

#### **Parsing Error**

Due to error in the applied parser or if the applied parser is not relevant to the log source, then the outqueue becomes 0 and redisq is getting high.

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log | grep redisq    
2019-02-07 05:34:19,224 INFO Parser: {'outqueue': 0, 'redisq': 232452}
2019-02-07 05:34:24,224 INFO Parser: {'outqueue': 0, 'redisq': 424242}
2019-02-07 05:34:29,224 INFO Parser: {'outqueue': 0, 'redisq': 544224}
```

In above **EventParserProcessor** logs redisq is getting continuously high and outqueue is 0, means logs are not getting parsed.

**How to recover:**

Remove the relevant parser, which is applied on the devices from the DNIF console.

Remove the parser from devices which logs are getting parsed with PER status in large volume from the DNIF console.

After removing the relevant parsers from the devices, check the EventParserProcessor logs for queue monitoring.

### **If event transport is in defunct state**

If redisq is getting high and not process data to outqueue as per above logs or stats of the queues are not updated means there is possibility of event transport process goes into defunct state.

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log | grep redisq 
2019-02-07 05:38:14,224 INFO Parser: {'outqueue': 234534, 'redisq': 282452}
2019-02-07 05:38:19,224 INFO Parser: {'outqueue': 234534, 'redisq': 282452}
2019-02-07 05:38:24,224 INFO Parser: {'outqueue': 234534, 'redisq': 282452}
```

In above **EventParserProcessor** logs, redisq and outqueue stats are not updated, means event transport process goes into defunct state.

**How to recover:**

Check the log sources on the console, which is forwarded events with high EPS and suppress those events if required.

Check for the logs ****which are getting parsed with PER status in large volume and remove them from the DNIF console.

Check for the DNIF processes which is in defunct state, using the following command,

```text
$ ps -aux | grep defunct
root     26567 2.2  1.4 379352 30276 ?        S   Feb13 08:35 /usr/bin/python -u /usr/src/nm/rtdp-adapter-v6/EventsParserProcessor/EPP.py
root     26568 0.0  0.0      0     0 ?        Z   Feb13 08:35 [python] <defunct>
```

If any DNIF python process is in defunct state, it can effect on the event\_parser\_processor  services and its cause the data to be not ingesting in the Datastore.

Check the [redis-server](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYfL0jRMfltEqFzCMC1/troubleshooting/sysloglistenerudp) status and recover if its not running or is in error state.

Check the [syslog\_listerner\_udp](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYfL0jRMfltEqFzCMC1/troubleshooting/sysloglistenerudp) service status and recover if its in FATAL state.

After resolving `redis-server` and `syslog_listerner_udp` issues, restart `event_parser_processor` and check the EventParserProcessor logs for queue monitoring.

### **If redis-server is not running or is in error state**

If redis-server is not running or is in error state, EventParserProcessor reports following types of errors in the logs,

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log
2019-02-13 10:04:16,041 ERROR Parser: Error 111 connecting to localhost:6379. Connection refused.
2019-02-13 10:04:16,042 ERROR Parser: Error 111 connecting to localhost:6379. Connection refused.
2019-02-13 10:04:16,043 ERROR Parser: Error 111 connecting to localhost:6379. Connection refused.
```

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log
2019-02-13 10:07:18,041 WARNING Parser: process    DN3FDR - Queue size fetch failed <<Error 111 connecting to localhost:6379. Connection refused.>>
2019-02-13 10:07:18,042 INFO Parser: process    None
2019-02-13 10:07:18,043 WARNING Parser: process    YF0DOZ - Data append error <<'NoneType' object has no attribute '__getitem__'>>
```

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log
2019-02-13 10:05:12,041 WARNING Parser: process    LHNHNN - Events fetch failed <<Error 111 connecting to localhost:6379. Connection refused.>>
2019-02-13 10:05:12,042 WARNING Parser: process    LHNHNN - Events fetch failed <<Error 111 connecting to localhost:6379. Connection refused.>>
2019-02-13 10:05:12,042 WARNING Parser: process    LHNHNN - Events fetch failed <<Error 111 connecting to localhost:6379. Connection refused.>>
```

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log
2019-02-13 10:02:18,042 WARNING Parser: process    LHNHNN - Events fetch failed <<MISCONF Redis is configured to save RDB snapshots, but is currently not able to persist on disk. Commands that may modify the data set are disabled.Please check Redis logs for details about the error.>>
2019-02-13 10:02:18,042 WARNING Parser: process    LHNHNN - Events fetch failed <<MISCONF Redis is configured to save RDB snapshots, but is currently not able to persist on disk. Commands that may modify the data set are disabled.Please check Redis logs for details about the error.>>
2019-02-13 10:02:18,042 WARNING Parser: process    LHNHNN - Events fetch failed <<MISCONF Redis is configured to save RDB snapshots, but is currently not able to persist on disk. Commands that may modify the data set are disabled.Please check Redis logs for details about the error.>>
```

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log
2019-02-13 10:01:17,043 WARNING Parser: process    YF0DOZ - Data append error <<'NoneType' object has no attribute '__getitem__'>>
2019-02-13 10:01:17,342 WARNING Parser: process    LHNHNN - Events fetch failed <<MISCONF Redis is configured to save RDB snapshots, but is currently not able to persist on disk. Commands that may modify the data set are disabled.Please check Redis logs for details about the error.>>
2019-02-13 10:01:18,071 WARNING Parser: process    DN3FDR - Queue size fetch failed <<Error 99 connecting to localhost:6379. Cannot assign reduested address.>>
```

**How to recover:**

Check the log sources on the console, which is forwarded events with high EPS and suppress those events if required.

Check for the logs ****which are getting parsed with PER status in large volume and remove them from the DNIF console.

Check the [redis-server](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYfL0jRMfltEqFzCMC1/troubleshooting/sysloglistenerudp) status and recover if its not running or is in error state.

Check the [syslog\_listerner\_udp](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYfL0jRMfltEqFzCMC1/troubleshooting/sysloglistenerudp) service status and recover if its in FATAL state.

After resolving `redis-server` and `syslog_listerner_udp` issues, restart `event_parser_processor` and check the EventParserProcessor logs for queue monitoring.

### **If redisq and outqueue is 0**

If redisq and outqueue is 0 more than five minutes means logs are not forwarding on Adapter or `syslog_listerner_udp` service is not in running state or crashed.

```text
$ tail -f /dnif/{Deploy-Key}/log/EventsParserProcessor.log | grep redisq 
2019-02-07 05:43:14,224 INFO Parser: {'outqueue': 0, 'redisq': 0}
2019-02-07 05:43:19,224 INFO Parser: {'outqueue': 0, 'redisq': 0}
2019-02-07 05:43:24,224 INFO Parser: {'outqueue': 0, 'redisq': 0}
```

In above **EventParserProcessor** logs, redisq and outqueue is 0, means there is possibility of syslog\_listener\_udp service is not in running state or crashed.

**How to recover:**

Check if logs are receiving on the Adapter or A10 server from the configured log sources using `tcpdump` command.

Check the [syslog\_listerner\_udp](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYfL0jRMfltEqFzCMC1/troubleshooting/sysloglistenerudp) service status and recover if its in FATAL state.

