---
title: syslog\_listerner\_udp 
permalink: /docs/Troubleshooting/Syslog\_Listerner\_Udp/
description: 'Syslog Listener - UDP, brings syslog information from devices and hosts.'
section: Troubleshooting
---

# syslog\_listerner\_udp

## What is `syslog_listerner_udp`

`syslog_listerner_udp` is a service that receive data-gram packets from the various log sources on the `syslog` port - `udp/514`. [Syslog](https://en.wikipedia.org/wiki/Syslog) is a common standard for logging events across devices and operating systems. This service runs on the Adapter \(AD\) and is also available on the All-in-One \(A10\) instances.

## Working with `SyslogListenerUDP`

Let's understand the basic operations of the `syslog_listerner_udp`.

## **Check if `SyslogListenerUDP` is running**

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Check for the `syslog_listerner_udp` service status using following command,

```text
$ supervisorctl 
adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

All is well if you see `syslog_listerner_udp` in the `RUNNING` state, however if the daemon is in the error state or has failed you are likely to see the state set to `FATAL`. Below is an example of a failed daemon.

```text
$ supervisorctl 
adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             FATAL     Exited too quickly (process log may have details)
```

## Checking the `SyslogListenerUDP` logs

You could check the for log events from the host systems itself. When the daemon works as designed, it will give out informational messages, each message will give out the current **Events Per Second** \(EPS\) count.

```text
$ tail -f /dnif/{DEPLOY_KEY}/log/SyslogListenerUDP.log 
2019-02-06 06:07:17,676 INFO SyslogListenerUDP: EPS 2434
2019-02-06 06:07:32,676 INFO SyslogListenerUDP: EPS 1242
2019-02-06 06:07:47,677 INFO SyslogListenerUDP: EPS 823
2019-02-06 06:08:03,676 INFO SyslogListenerUDP: EPS 642
2019-02-06 06:08:20,676 INFO SyslogListenerUDP: EPS 1042
2019-02-06 06:08:33,676 INFO SyslogListenerUDP: EPS 442
2019-02-06 06:08:50,688 INFO SyslogListenerUDP: EPS 742
2019-02-06 06:09:03,676 INFO SyslogListenerUDP: EPS 1424
2019-02-06 06:09:17,677 INFO SyslogListenerUDP: EPS 1324
```

### **Restart** `syslog_listerner_udp`

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Restart the `syslog_listerner_udp` service status using `restart syslog_listerner_udp`command

```text
$ supervisorctl 
adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30

supervisor> restart syslog_listerner_udp 
syslog_listerner_udp: stopped
syslog_listerner_udp: started

supervisor> exit
```

## **Troubleshooting** `syslog_listerner_udp`

`syslog_listerner_udp` fails for the following reasons

* `redis-server` is not running or is in error state
* `event_parser_processor` ****is in the error state, which will also result in a `redis-server` failure

So essentially troubleshooting `syslog_listerner_udp` revolves around `redis-server` and if we are able to troubleshoot `redis-server` operations we should be able to solve known issues with this daemon.

### Checking `redis-server`

If `syslog_listerner_udp` ****service is gone into `FATAL` state

* Check for the `redis-server` status.

```text
$ /etc/init.d/redis-server status
 * redis-server is not running
```

### If `redis-server is not running`

* Check for the `redis` process, if `redis` process is shown in running state with a process ID, kill the `redis` service using the `kill` command, incase redis process is not running proceed to Restart the `redis-server` daemon.

```text
$ ps aux | grep redis
redis       50 19.5  0.3  37220  6712 ?        Rsl  09:51  12:45 /usr/bin/redis-server 127.0.0.1:6379
```

* Kill the `redis` process using the process ID, in this example 50 is the `pid` for `redis`.

```text
$ kill -9 50
```

* Restart the `redis-server` daemon

```text
$ /etc/init.d/redis-server start
Starting redis-server: redis-server.
```

* Check the `redis-server` logs

```text
$ tail -f /var/log/redis/redis-server.log
18420:M 05 Feb 11:03:49.445 # Server started, Redis version 3.0.6
18420:M 05 Feb 11:03:49.445 * DB loaded from disk: 0.000 seconds
18420:M 05 Feb 11:03:49.445 * The server is now ready to accept connections on port 6379
```

* You should see `Server started, Redis version`, after a few minutes you should also see `The server is now ready to accept connection on port 6379`
* You should find `redis-server is in running` when you check for status of the daemon

```text
$ /etc/init.d/redis-server status
 * redis-server is running
```

* Double check on the `redis-server` logs you should see no errors

```text
$ tail -f /var/log/redis/redis-server.log
18420:M 06 Feb 04:37:19.031 * 10 changes in 300 seconds. Saving...
18420:M 06 Feb 04:37:19.031 * Background saving started by pid 18623
18623:C 06 Feb 04:37:19.068 * DB saved on disk
18623:C 06 Feb 04:37:19.068 * RDB: 0 MB of memory used by copy-on-write
18420:M 06 Feb 04:37:19.131 * Background saving terminated with success
```

### If `redis-server` is reporting errors

Incase `redis-server` is reporting `Cannot allocate memory` it usually means the system is out of memory.

```text
$ tail -f /var/log/redis/redis-server.log
18420:M 06 Feb 04:37:19.031 * 1 changes in 900 seconds. Saving...
18420:M 06 Feb 04:37:19.031 # Can't save in Background: fork: Cannot allocate memory
18420:M 06 Feb 04:37:25.031 * 1 changes in 900 seconds. Saving...
18420:M 06 Feb 04:37:25.031 # Can't save in Background: fork: Cannot allocate memory
18420:M 06 Feb 04:37:31.031 * 1 changes in 900 seconds. Saving...
18420:M 06 Feb 04:37:31.031 # Can't save in Background: fork: Cannot allocate memory    
```

* If there is enough \(more than 40%\) memory available, open a ticket with DNIF SupportOps with all the evidences \(screenshots/ logs\) of the investigations already conducted
* If the system is out of memory you may need to check the [event\_parser\_processor](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYaAHq_cGTsNWroyNcI/troubleshooting/eventparserprocessor) ****daemon
* After resolving all `redis-server` issues, restart [syslog\_listerner\_udp](https://docs.dnif.it/admin-guide/~/edit/drafts/-LYaBdEA3nM3NJUEKD7B/troubleshooting/sysloglistenerudp)



