---
title: Recover Data Index
permalink: /docs/Troubleshooting/Recover-Data-Index/
description: Recover database health when Red
section: Troubleshooting
---

# Recover Data Index

If the database indexes get corrupted or database health is RED, then we can try to recover those indexes using below steps,

## **Basics Checks**

* Need to check Datastore server/ A10 server disk space availability, if the disk is getting full then backup or delete your back-dated data, only then to proceed to recover the database.

## **Troubleshooting**

* Login to your **Data Store** or **A10** container. Read more on [how to Login to the docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Follow the below steps __to check the database health status,

```text
$ python 
>>> from elasticsearch import Elasticsearch 
>>> es = Elasticsearch([{'port':8240}]) 
>>> es.cat.health()
u'1549265131 07:25:31 NETMON red 1 1 111 111 0 0 56 \n'
```

* If the database health status is RED, use below commands to check for which indexes are in RED state.

```text
$ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.cat.health()
u'1549265131 07:25:31 NETMON red 1 1 111 111 0 0 56 \n'
>>> m = es.cat.indices()
>>> for i in es.cat.indices().split('\n'):
...     if 'red' in i:
...           print i
... 
red open  dsdb-20190124        1 0   74046      0  29.6mb  29.6mb 
red open  dsdb-20190122        1 0   99307      0  46.7mb  46.7mb 
red open  dsdb-20190125        1 0   95539      0  43.5mb  43.5mb
```

* To recover the indexes, stop the din and ingestor services using following commands,

```text
$ supervisorctl 
supervisor> stop din ingestor 
din: stopped
ingestor: stopped
supervisor> exit
```

* Restart the database service using following commands,

```text
$ /etc/init.d/elasticsearch stop
 * Stopping Elasticsearch Server
$ /etc/init.d/elasticsearch start
 * Starting Elasticsearch Server
sysctl: setting key "vm.max_map_count": Read-only file system
                                                                                                                                               [ OK ]
```

* Check for indexes are recovered or not using the following commands, if recovered then start the din and ingestor services.

```text
$ python 
>>> from elasticsearch import Elasticsearch 
>>> es = Elasticsearch([{'port':8240}]) 
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 1 1 111 111 0 0 0 \n'
```

* If indexes are not recovered after database restarts, then again check for the indexes which are in **red** state.

```text
$ python 
>>> from elasticsearch import Elasticsearch 
>>> es = Elasticsearch([{'port':8240}]) 
>>> es.cat.health()
u'1549265131 07:25:31 NETMON red 1 1 111 111 0 0 35 \n'
>>> m = es.cat.indices()
>>> for i in es.cat.indices().split('\n'):
...     if 'red' in i:
...           print i
... 
red open  dsdb-20190125        1 0   74046      0  29.6mb  29.6mb
```

* If some of the indexes are not getting recover, then perform following steps to recover, stop database service using following commands,

```text
$ /etc/init.d/elasticsearch stop
 * Stopping Elasticsearch Server
```

* Check for the translog recovery created in the indexes, if created then move the translog recovery from the indexes, which are not recovered and still in **red** state.

```text
$ cd /dnif/{Deploy-Key}/data/{Cluster-Name}/nodes/0/indices/{ndex-name}/0/translog
$ ls
translog-15293387023094    translog-15293387023094.recovering
$ mv translog-15293387023094.recovering /var/tmp
```

Check for the translog recovery files in all indexes which are in **red** state and move them into **/var/tmp** location**.**

* Start the database service  using following command,

```text
$ /etc/init.d/elasticsearch start
* Starting Elasticsearch Server
sysctl: setting key "vm.max_map_count": Read-only file system
                                                                                                                                               [ OK ]
```

* Check for the database logs using following command,

```text
$ tail -f /dnif/{Deploy-Key}/log/elasticsearch/{cluster-name}.log
[2019-02-08T07:02:27,402][INFO ][o.e.n.Node               ] initialized
[2019-02-08T07:02:27,402][INFO ][o.e.n.Node               ] [xFeXvIY] starting ...
[2019-02-08T07:02:28,004][INFO ][o.e.t.TransportService   ] [xFeXvIY] publish_address {172.16.10.50:8340}, bound_addresses {172.16.10.50:8340}
[2019-02-08T07:02:28,051][INFO ][o.e.b.BootstrapChecks    ] [xFeXvIY] bound or publishing to a non-loopback address, enforcing bootstrap checks
[2019-02-08T07:02:31,201][INFO ][o.e.c.s.MasterService    ] [xFeXvIY] zen-disco-elected-as-master ([0] nodes joined), reason: new_master {xFeXvIY}{xFeXvIYATWmMvbAKKjEpZg}{WkvWeSquTKurBsRFabr0NA}{172.16.10.50}{172.16.10.50:8340}
[2019-02-08T07:02:31,204][INFO ][o.e.c.s.ClusterApplierService] [xFeXvIY] new_master {xFeXvIY}{xFeXvIYATWmMvbAKKjEpZg}{WkvWeSquTKurBsRFabr0NA}{172.16.10.50}{172.16.10.50:8340}, reason: apply cluster state (from master [master {xFeXvIY}{xFeXvIYATWmMvbAKKjEpZg}{WkvWeSquTKurBsRFabr0NA}{172.16.10.50}{172.16.10.50:8340} committed version [1] source [zen-disco-elected-as-master ([0] nodes joined)]])
[2019-02-08T07:02:31,308][INFO ][o.e.h.n.Netty4HttpServerTransport] [xFeXvIY] publish_address {127.0.0.1:8240}, bound_addresses {[::1]:8240}, {127.0.0.1:8240}
[2019-02-08T07:02:31,308][INFO ][o.e.n.Node               ] [xFeXvIY] started
[2019-02-08T07:02:31,730][INFO ][o.e.g.GatewayService     ] [xFeXvIY] recovered [10] indices into cluster_state
[2019-02-08T07:02:32,505][INFO ][o.e.c.r.a.AllocationService] [xFeXvIY] Cluster health status changed from [RED] to [GREEN] (reason: [shards started [[sysdata][0]] ...]).
```

In above logs, Cluster health status changed from \[RED\] to \[GREEN\].

* Check the database health status using following commands,

```text
$ python 
>>> from elasticsearch import Elasticsearch 
>>> es = Elasticsearch([{'port':8240}]) 
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 1 1 111 111 0 0 0 \n'
```

* If database health status is in green state, then start the ingestor and din services using following commands,

```text
$ supervisorctl 
supervisor> stop din ingestor 
din: started
ingestor: started
supervisor> exit
```

## **Restart database service**

* Login to your **Datastore** or **A10** container and restart database service using following command,

```text
$ /etc/init.d/elasticsearch stop
 * Stopping Elasticsearch Server
  
$ /etc/init.d/elasticsearch start
* Starting Elasticsearch Server
sysctl: setting key "vm.max_map_count": Read-only file system                                                                                                                                              [ OK ]
```

* Check for the database logs using following command,

```text
$ tail -f /dnif/{Deploy-Key}/log/elasticsearch/{cluster-name}.log
[2019-02-08T07:02:27,402][INFO ][o.e.n.Node               ] initialized
[2019-02-08T07:02:27,402][INFO ][o.e.n.Node               ] [xFeXvIY] starting ...
[2019-02-08T07:02:28,004][INFO ][o.e.t.TransportService   ] [xFeXvIY] publish_address {172.16.10.50:8340}, bound_addresses {172.16.10.50:8340}
[2019-02-08T07:02:28,051][INFO ][o.e.b.BootstrapChecks    ] [xFeXvIY] bound or publishing to a non-loopback address, enforcing bootstrap checks
[2019-02-08T07:02:31,201][INFO ][o.e.c.s.MasterService    ] [xFeXvIY] zen-disco-elected-as-master ([0] nodes joined), reason: new_master {xFeXvIY}{xFeXvIYATWmMvbAKKjEpZg}{WkvWeSquTKurBsRFabr0NA}{172.16.10.50}{172.16.10.50:8340}
[2019-02-08T07:02:31,204][INFO ][o.e.c.s.ClusterApplierService] [xFeXvIY] new_master {xFeXvIY}{xFeXvIYATWmMvbAKKjEpZg}{WkvWeSquTKurBsRFabr0NA}{172.16.10.50}{172.16.10.50:8340}, reason: apply cluster state (from master [master {xFeXvIY}{xFeXvIYATWmMvbAKKjEpZg}{WkvWeSquTKurBsRFabr0NA}{172.16.10.50}{172.16.10.50:8340} committed version [1] source [zen-disco-elected-as-master ([0] nodes joined)]])
[2019-02-08T07:02:31,308][INFO ][o.e.h.n.Netty4HttpServerTransport] [xFeXvIY] publish_address {127.0.0.1:8240}, bound_addresses {[::1]:8240}, {127.0.0.1:8240}
[2019-02-08T07:02:31,308][INFO ][o.e.n.Node               ] [xFeXvIY] started
[2019-02-08T07:02:31,730][INFO ][o.e.g.GatewayService     ] [xFeXvIY] recovered [10] indices into cluster_state
[2019-02-08T07:02:32,505][INFO ][o.e.c.r.a.AllocationService] [xFeXvIY] Cluster health status changed from [RED] to [GREEN] (reason: [shards started [[sysdata][0]] ...]).
```

