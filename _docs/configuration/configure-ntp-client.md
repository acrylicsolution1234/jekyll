---
title: Configure NTP Client 
permalink: /docs/Configuration/Configure-NTP-Client/
section: Configuration
---

# Configure NTP Client

NTP is the protocol used to sync time of machines with NTP server \(can be an appliance or another Linux machine\) over the network. It aims at keeping all machines clock in sync so that there will be no delays between any two machines in a network. 

Network time protocol runs on `UDP` port `123`. This should be open between time server and client in both directions.

To configure your DNIF system as an NTP client, you will need to install the _ntp daemon \(ntpd\)_. You can do this with the _sudo apt-get install ntp_ command:

```
$ sudo apt-get -y install ntp
```

The ntpd configuration file is located at _/etc/ntp.conf._ Open this file in a vim editor:

```text
vim /etc/ntp.conf

# /etc/ntp.conf, configuration for ntpd; see ntp.conf(5) for help
  
driftfile /var/lib/ntp/ntp.drift

# Leap seconds definition provided by tzdata
leapfile /usr/share/zoneinfo/leap-seconds.list

# Enable this if you want statistics to be logged.
#statsdir /var/log/ntpstats/

statistics loopstats peerstats clockstats
filegen loopstats file loopstats type day enable
filegen peerstats file peerstats type day enable
filegen clockstats file clockstats type day enable

# Specify one or more NTP servers.

# Use servers from the NTP Pool Project. Approved by Ubuntu Technical Board
# on 2011-02-08 (LP: #104525). See http://www.pool.ntp.org/join.html for
# more information.
pool 0.ubuntu.pool.ntp.org iburst
pool 1.ubuntu.pool.ntp.org iburst
pool 2.ubuntu.pool.ntp.org iburst
pool 3.ubuntu.pool.ntp.org iburst

# Use Ubuntu's ntp server as a fallback.
pool ntp.ubuntu.com
# Add your local or public NTP server IP address or domain.
pool tick.clara.net
```

This file contains the list of NTP servers that will be used for time synchronization. By default, Ubuntu servers will be used \(e.g. **0.ubuntu.pool.ntp.org**\). You can add your preferred servers\(local or public NTP server\).

Next, restart the NTP deamon with the _sudo service ntp reload_ command:

```text
$ service ntp reload
```

And that’s it! You system should synchronize time with NTP servers listed in the _ntp.conf_ file. You can use the _ntpq -p_ command to show a list of all the NTP servers and when they where last checked.

```text
$ ntpq -p

     remote       refid          st t  when poll reach  delay    offset  jitter
==============================================================================
 0.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 1.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 2.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 3.ubuntu.pool.n .POOL.          16 p    -   64    0    0.000    0.000   0.000
 ntp.ubuntu.com  .POOL.          16 p    -   64    0    0.000    0.000   0.000
 tick.clara.net  .POOL.          16 p    -   64    0    0.000    0.000   0.000
+139.59.15.185   139.59.50.38     3 u  197  256  377   22.344   13.440   3.546
-resolver1.uk.cl 195.66.241.2     2 u  203  256  377  146.656   -0.419  40.956
+chilipepper.can 17.253.34.125    2 u    1  256  277  126.885    3.222  53.129
*golem.canonical 140.203.204.77   2 u  160  256  377  123.273    4.578  87.361
```



