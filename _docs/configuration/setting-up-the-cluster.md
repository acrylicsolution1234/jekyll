---
title: Setting up the Cluster 
permalink: /docs/Configuration/Setting-up-the-Cluster/
section: Configuration
---
# Setting up the Cluster

## **Overview**

A Cluster consists of a group of Datastores nodes put together in order to store and manage data.

## **Scenario**

Configure cluster on three nodes Datastore.

![](../.gitbook/assets/image%20%2838%29.png)

## **Pre-Requisites**

1. Docker and DNIF DS component should be deployed on the three nodes of the DS servers. 
2. All DS nodes should communicate with each other**.**
3. SMTP configuration is mandatory on all DS servers.

## **Configuration**

* Log into all Datastore DNIF containers and stop following DNIF services on all DS servers using following command.

```text
$ supervisorctl 

supervisor> stop nameserver din ingestor ilidsapi 
nameserver: stopped
din: stopped
ingestor: stopped
ilidsapi: stopped
supervisor> exit
```

* Stop elasticsearch service on all DS servers using the following command,

```text
$ /etc/init.d/elasticsearch stop

 * Stopping Elasticsearch Server
```

* Open the elasticsearch.yml file using following command and search for “unicast” keyword in the above configuration file and uncomment the line **discovery.zen.ping.unicast.hosts: \["host1:port", "host2:port"\].**

```text
$ vim /dnif/{Depoly-Key}/config/elasticsearch.yml

discovery.zen.ping.unicast.hosts: ["192.168.86.2:8340", "192.168.86.3:8340", "192.168.86.4:8340"]
```

* If you are on DS1 server, replace “host1” as “DS1 server IP”, “host2” as “DS2 server IP” and “host3” as “DS3 server IP” with Port 8340.
* Perform the changes on DS2 and DS3 servers as below, 
  * On DS 2 Server, replace “host1” as “DS1 server IP”, “host2” as “DS2 server IP” and “host3” as “DS3 server IP” with Port 8340.
  * On DS 3 Server, replace “host1” as “DS1 server IP”, “host2” as “DS2 server IP” and “host3” as “DS3 server IP” with Port 8340.
* On DS 1 server, one more changes required in  elasticsearch.yml file to make as primary DS, uncomment or add following lines.

```text
node.master: true 
node.data: true
```

* On DS2 and DS3 servers,  uncomment following lines in the elasticsearch.yml file.

```text
node.master: false 
node.data: true
```

> **Note: If datastore servers having multiple network interfaces then changes required in the elasticsearch.yml file for the network publish host.**

* The “network.publish\_host” should be the IP Address of the same Datastore and this change should be done on all Datastores.

```text
network.publish_host: 192.168.86.2
```

* On DS2 and DS3 servers, open Pyro configuration file using the below command and change "nhost" & “host” value as IP of Datastore1.

```text
$ vim /dnif/{Deploy-Key}/csltuconfig/Pyro

{"Pyro": {"nameserver": {"nhost": "<DS1-IP>", "host": "DS1-IP", "key": "n342@duush.com"}}}
```

* On all DS servers, open **system** file using the below command and set \(“shardcount”: total count of DS\)

```
$ vim /dnif/{Depoy-Key}/csltuconfig/system

{"system": {"admin": "user@domain.com", "ScopeList": ["AV6l6ZojgSGP0Rd3MEXh"], "localip": "192.168.86.2", "systemid": "AJ6o6ZofgSGFDRd3MEXf", "shardcount": 3, "retention": 45}}
```

For Example: If three nodes of DS in the cluster then set shardcount as \(“shardcount” : 3\)

* After the above changes, start the elasticsearch service on all datastore Servers using the below command.

```text
$ /etc/init.d/elasticsearch start

* Starting Elasticsearch Server
sysctl: setting key "vm.max_map_count": Read-only file system
                                                                                                                                               [ OK ]
```

> **Note: Start elasticsearch service on every DS  servers at the same time.**

* Follow the below steps __on DS1 server to check the database health status,

```text
$ python 

>>> from elasticsearch import Elasticsearch 
>>> es = Elasticsearch([{'port':8240}]) 
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 3 3 111 111 0 0 0 \n'
```

The above commands will show the health status of database whether the status is Green, Yellow or Red.

* The cluster health API allows getting a very simple status on the health of the cluster.  
  * Green state: A Green means that all shards are allocated. 
  * Yellow state: A Yellow means that the primary shard is allocated but replicas are not. 
  * Red state: A red status indicates that the specific shard is not allocated in the cluster.
* If Database health status is in Green state, start below DNIF services on DS1 server using the following command,

```text
$ supervisorctl 

supervisor> start nameserver din ingestor ilidsapi 
nameserver: started
din: started
ingestor: started
ilidsapi: started
supervisor> exit
```

* Start below DNIF services on DS2 and DS3 server using the following command,

```text
$ supervisorctl 

supervisor> start din ingestor  
din: started
ingestor: started
supervisor> exit
```

* Login into Adapter DNIF container, open Pyro configuration file and add all DS IP's in Pyro file using the following command,

```text
$ vim /dnif/{Deploy-Key}/csltuconfig/Pyro

{"Pyro": {"nameserver": {"nhost": "<DS1-IP>", "host": "<DS1-IP>", "key": "n342@duush.com"}, "DC": ["<DS1-IP>", "<DS2-IP>", "<DS3-IP>"]}}
```

With the above configuration, we have successfully configured cluster on three nodes of the Datastores.



