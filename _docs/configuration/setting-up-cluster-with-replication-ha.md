---
title: Setting up Cluster with Replication 
permalink: /docs/Configuration/Setting-up-Cluster-with-Replication/
section: Configuration
---


# Setting up Cluster with Replication \(High Availablity\)

## **Overview**

Data Replication is the process of replicating data on more than one node. It is useful in improving the availability of data.

Replication is used in case of high availability feature.

## **Scenario**

Configure cluster with replication on two nodes of Datastore.

![](../.gitbook/assets/image%20%2855%29.png)

## **Pre-Requisites**

1. Docker and DNIF DS component should be deployed on the two nodes of the DS servers. 
2. All DS nodes should be communicated with each other**.**
3. SMTP configuration is mandatory on all Datastore servers.

## **Configuration**

* Log into all Datastore DNIF containers and stop following DNIF services on all DS servers using the following command.

```text
$ supervisorctl 

supervisor> stop nameserver din ingestor ilidsapi 
nameserver: stopped
din: stopped
ingestor: stopped
ilidsapi: stopped
supervisor> exit
```

* Stop elasticsearch service on all DS servers using the following command,

```text
$ /etc/init.d/elasticsearch stop

 * Stopping Elasticsearch Server
```

* Open the elasticsearch.yml file using following command and search for “unicast” keyword in the above configuration file and uncomment the line **discovery.zen.ping.unicast.hosts: \["host1:port", "host2:port"\].**

```text
$ vim /dnif/{Depoly-Key}/config/elasticsearch.yml

discovery.zen.ping.unicast.hosts: ["192.168.86.2:8340", "192.168.86.3:8340"]
```

* If you are on DS1 server, replace “host1” as “DS1 server IP”, “host2” as “DS2 server IP” with Port 8340.
* Perform the changes on DS2 server as below, 
  * On DS 2 Server, replace “host1” as “DS1 server IP”, “host2” as “DS2 server IP”  with Port 8340.
* On both DS servers, one more changes require in the elasticsearch.yml file to make as primary DS, uncomment following lines.

```text
node.master: true 
node.data: true
```

> **Note: If datastore servers having multiple network interfaces then changes required in the elasticsearch.yml file for the network publish host.**

* The “network.publish\_host” should be the IP Address of the same Datastore and this change should be done on both Datastores.

```text
network.publish_host: 192.168.86.2
```

* On DS2 server, open Pyro configuration file using the below command and change "nhost" & “host” value as IP of DS 1.

```text
$ vim /dnif/{Deploy-Key}/csltuconfig/Pyro

{"Pyro": {"nameserver": {"nhost": "<DS1-IP>", "host": "DS1-IP", "key": "n342@duush.com"}}}
```

* On both DS servers, open **system** file using the below command and set replication and shardcount as below,

```text
$ vim /dnif/<Depoy-Key>/csltuconfig/system

{"system": {"admin": "user@domain.com", "ScopeList": ["AV6l6ZojgSGP0Rd3MEXh"], "localip": "192.168.86.2", "systemid": "AJ6o6ZofgSGFDRd3MEXf", "shardcount": 2, "retention": 45, "replica": 1}}
```

* If two nodes of DS in the cluster then set shardcount as \(“shardcount” : 2\) and add \(“replica”: 1\) as above.
* After the above changes, start the database service on all datastore Servers using the below command.

```text
$ /etc/init.d/elasticsearch start

* Starting Elasticsearch Server
sysctl: setting key "vm.max_map_count": Read-only file system
                                                                                                                                               [ OK ]
```

> **Note: Start elasticsearch service on both DS servers at the same time.**

* Follow the below steps __on DS __1 server to check the database health status,

```text
$ python 

>>> from elasticsearch import Elasticsearch 
>>> es = Elasticsearch([{'port':8240}]) 
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 2 2 111 111 0 0 0 \n'
```

The above commands will show the health status of database whether the status is Green, Yellow or Red.

* The cluster health API allows getting a very simple status on the health of the cluster. 
  * Green state: A Green means that all shards are allocated. 
  * Yellow state: A Yellow means that the primary shard is allocated but replicas are not. 
  * Red state: A red status indicates that the specific shard is not allocated in the cluster.
* If Database health status is in Green state, enable the replication on system indices\(ustats, ctdp\_conf, sysdata, logging and current date index\) using below commands,

```text
$ python 

>>> from elasticsearch import Elasticsearch 
>>> es = Elasticsearch([{'port':8240}]) 
>>> replica = {"settings":{'number_of_replicas':1}} 
>>> es.indices.put_settings(index="{index_name}",body=replica) 
```

**For Example**: 

```text
es.indices.put_settings(index="ctdp_conf", body=replica)
```

* After enabling replication on system indices along with current date index, you can see the value of replica on the indices as 1.

```text
green open  dsdb-20190124        2 1   74046      0  29.6mb  29.6mb 
green open  ctdp_conf            2 1   18780    704 107.6mb 107.6mb 
green open  dsdb-20190123        2 1   70188      0  28.7mb  28.7mb 
green open  dsdb-20190122        2 1   99307      0  46.7mb  46.7mb 
green open  dsdb-20190125        2 1   95539      0  43.5mb  43.5mb 
```

> **Note: Whenever the disk space will get filled above 85% then replication will get stopped and you are not able to see logs on DNIF console.**

* Start below DNIF services on DS1 server using the following command,

```text
$ supervisorctl 

supervisor> start nameserver din ingestor ilidsapi 
nameserver: started
din: started
ingestor: started
ilidsapi: started
supervisor> exit
```

* Start below DNIF services on DS2 server using the following command,

```text
$ supervisorctl 

supervisor> start din ingestor ilidsapi 
din: started
ingestor: started
ilidsapi: started
supervisor> exit
```

* Log into adapter DNIF container, open Pyro configuration file and add both DS IP's in Pyro file using the following command,

```text
$ vim /dnif/{Deploy-Key}/csltuconfig/Pyro

{"Pyro": {"nameserver": {"nhost": "<DS1-IP>", "host": "<DS1-IP>", "key": "n342@duush.com"}, "DC": ["<DS1-IP>", "<DS2-IP>"]}}
```

With the above configuration, we have successfully configured cluster with replication on two nodes of the Datastores.

