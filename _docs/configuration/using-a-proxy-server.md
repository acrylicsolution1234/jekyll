---
title: Using a Proxy Server 
permalink: /docs/Configuration/Using-a-Proxy-Server/
description: When you want to use a proxy server
section: Configuration
---

# Using a Proxy Server

When you don't have unrestricted access to the Internet and you need to use a proxy server to reach system on the Internet, this guide can be used to configure proxy settings on DNIF components to get started. There are some considerations while using a proxy server -

* Each component needs access to `https://api2.netmonastery.com` without which these systems will not have the requisite data required to function.
* All DNIF components within an environment need to have direct access to each other.

## **Configure proxy on linux**

### **Pre-Requisites:**

* Proxy details like proxy server IP address, port, username and password.
* Username and password is required if defined, otherwise you can configure the proxy using IP address and port.

### **Configuration:**

#### **In order to configure proxy at the OS level:**

* Set environment variable using the following command:

```text
$ export http_proxy="http://USERNAME:PASSWORD@{Proxy-Server-IP/Domain}:{port}"
```

* **Examples:**

```text
$ export http_proxy="http://user01:password@10.10.10.10:8080"
$ export https_proxy="https://user01:password@10.10.10.10:8080"
$ export no_proxy="localhost,127.0.0.1,{DNIF-Servers-IP}"
```

* To check whether it is set, use the following command:

```text
$ env

HOME=/root
SUDO_USER=netmon
https_proxy=https://user01:password@10.10.10.10:8080
http_proxy=http://user01:password@10.10.10.10:8080
no_proxy=localhost,127.0.0.1,172.16.10.0/24
SUDO_UID=1000
MAIL=/var/mail/root
SHELL=/bin/bash
```

* Configure the same in the /etc/environment file to make the changes persistent using the following command:
* Note that if you use init.d or the services tool, you must add the export statement. Alternatively, add both variants in the environment file.

```text
$ vim /etc/environment

HTTP_PROXY="http://user01:password@10.10.10.10:8080"
HTTPS_PROXY="https://user01:password@10.10.10.10:8080"
NO_PROXY="localhost,127.0.0.1,172.16.10.0/24"

export http_proxy="http://user01:password@10.10.10.10:8080"
export https_proxy="https://user01:password@10.10.10.10:8080"
export no_proxy="localhost,127.0.0.1,172.16.10.0/24"
```

**A proxy is required when the server running Docker does not have direct access to the Internet.**

Configure the Docker daemon to use a proxy server to access images stored on the official Docker Hub Registry or 3rd-party registries.

There are 2 ways to configure the proxy for docker 

#### **Configuring proxy variables in the /etc/sysconfig/docker file:**

* Add the HTTPS\_PROXY / HTTP\_PROXY/ NO\_PROXY environment variable to the docker sysconfig file.
* Note that if you use init.d or the services tool, you must add the export statement. Alternatively, add both variants in the sysconfig file of docker.

```text
$ vim /etc/sysconfig/docker

HTTP_PROXY="http://user01:password@10.10.10.10:8080"
HTTPS_PROXY="https://user01:password@10.10.10.10:8080"
NO_PROXY="localhost,127.0.0.1,172.16.10.0/24"

export http_proxy="http://user01:password@10.10.10.10:8080"
export https_proxy="https://user01:password@10.10.10.10:8080"
export no_proxy="localhost,127.0.0.1,172.16.10.0/24"
```

* Restart the Docker daemon after setting up the proxy.

```text
$ service docker restart
```

#### **Configuring environment variables:**

* Create a drop-in

```text
$ mkdir /etc/systemd/system/docker.service.d/
```

* Create a file with name /etc/systemd/system/docker.service.d/docker-proxy.conf that adds the proxy environment variable:

```text
$ vim /etc/systemd/system/docker.service.d/docker-proxy.conf

[Service]
Environment="HTTP_PROXY=http://user01:password@10.10.10.10:8080"
Environment="HTTPS_PROXY=https://user01:password@10.10.10.10:8080"
Environment="NO_PROXY=localhost,127.0.0.1,172.16.10.0/24"
```

* After making the above configuration, to apply the changes you need to reload the docker daemon using below command.

```text
$ sudo systemctl daemon-reload
```

* Verify that the configuration has been loaded:

```text
$ systemctl show docker --property Environment

Environment=GOTRACEBACK=crash HTTP_PROXY=http://user01:password@10.10.10.10:8080 HTTPS_PROXY=https://user01:password@10.10.10.10:8080 NO_PROXY=localhost,127.0.0.1,172.16.10.0/24
```

* Once you complete the proxy configuration on the host machine, before starting the docker using docker-compose.yml file you need to add proxy details in the environment of docker-compose.yml file.

```text
$ vim docker-compose.yml

version: '2.0'
services:
 comp-ship-a10:
   image: "dnif/ship-a10:7.3.3"
   network_mode: "host"
   cap_add:
    - NET_ADMIN
   ports:
    - "127.0.0.1:826:22/tcp"
    - "443:443/tcp"
    - "8350:8350/tcp"
    - "8351:8351/tcp"
    - "8355:8355/tcp"
    - "8340:8340/tcp"
    - "9234:9234/tcp"
    - "9236:9236/tcp"
    - "514:514/udp"
    - "514:514/tcp"
    - "8362:8362/tcp"
   volumes:
    - /dnif:/dnif
   environment:
    - "DKEY=XXXXXXXXXXXXXXX"
    - "CRIP={CR_IP_ADDRESS}"
    - "HTTP_PROXY=http://user01:password@10.10.10.10:8080"
    - "HTTPS_PROXY=https://user01:password@10.10.10.10:8080"
    - "NO_PROXY=localhost,127.0.0.1,172.16.10.0/24"
    - "bootstrap_memory_lock=true"
   ulimits:
    memlock:
     soft: -1
     hard: -1
   container_name: comp-dnif-a10
```

* Start the docker container using following command

```text
$ docker-compose up -d
```

* To set appropriate proxy environment variables within the container, login to your **DNIF** container. Read more on [how to Login to the docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Add the proxy variable in /etc/environment file using following command.

```text
$ vim /etc/environment

HTTP_PROXY="http://user01:password@10.10.10.10:8080"
HTTPS_PROXY="https://user01:password@10.10.10.10:8080"
NO_PROXY="localhost,127.0.0.1,172.16.10.0/24"
```

> **Note: If DNIF container gets down, you need to reset proxy variables in the /etc/environment file.**



