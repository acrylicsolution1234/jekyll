---
title: Setting up the Console 
permalink: /docs/Configuration/Setting-up-the-Console/
section: Configuration
---

# Setting up the Console

1. Once you have successfully installed DNIF, you can [log into the DNIF web console](https://docs.dnif.it/admin-guide/quick-start-guide/using-the-web-console) and set it up for further usage.
2. Once you are logged in, you can establish a [connection to your DNIF datastore](https://docs.dnif.it/admin-guide/configuration/connecting-to-the-datastore).
3. Once you successfully established the connection with your Datastore, here is the [list of publicly available devices](https://docs.dnif.it/admin-guide/ingesting-data/available-connectors) and their versions that you can integrate with DNIF, for more information check out: [how to add devices to DNIF console](https://docs.dnif.it/admin-guide/ingesting-data/add-devices-to-dnif).
4. After adding the required devices, you need to apply their respective parsers, here is a guide to help you with this process:  How to [apply parsers](https://docs.dnif.it/admin-guide/ingesting-data/loading-parsers).
5. Now, the integrated devices need to be configured to forward log data to DNIF. Here is a comprehensive guide to help you with the same: I[ntegrate log sources with DNIF](https://docs.dnif.it/integration-manual/).
6. Once the above process is complete, check if the logs from [integrated devices](https://docs.dnif.it/admin-guide/ingesting-data/integrating-devices) are being parsed correctly. Here is a quick guide to help you with the same:  [Check parsing status](https://docs.dnif.it/admin-guide/ingesting-data/integrating-devices).
7. You can now browse the `DNIF Cloud` repository and [SYNC only those packages that are created by DNIF HQ user](https://docs.dnif.it/operators-guide/create-and-view-packages) \(a package generally comprises of ready-made dashboards, reports, templates, modules and workbooks\) related to your device list.





