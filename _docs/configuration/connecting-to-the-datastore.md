---
title: Connecting to the Datastore 
permalink: /docs/Configuration/Connecting-to-the-Datastore/
description: Fetching your data from the datastore
section: Configuration
---

# Connecting to the Datastore

Before we discuss the connectivity details it is important to understand the architecture required for this federated system to work.

In order to fetch data from the datastore and bring it to the web, the console requires direct access from the analyst’s workstation to the datastore over HTTPS. The workstation then directly communicates with the datastore and retrieves results over an SSL encrypted tunnel.

![](../.gitbook/assets/image%20%2843%29.png)

The above diagram shows an example of a complex configuration where data hunters or security analysts are centrally located and can access any number of datastores over an HTTPS tunnel. However, the hunters could also be local to the datastore and could access data directly over the local network.

### **ACCESS OPTIONS**

Here are some of the most common options for accessing data from the datastore:

* Expose the datastore to a public IP and access it over the Internet \(Not Recommended\)
* Access the datastore over a private network or a Local Area Network \(LAN\)
* Access the datastore over a Virtual Private Network \(VPN\)

> **Note: IP of the datastore, w.r.t the workstation in use, must be recorded and configured in the web console, In distributed deployments where the Adapters, Datastores and Correlators are deployed individually, you need to connect your workstation with the datastore.**

### **Establish the connection with DNIF**

From the MANAGEMENT menu, click CONNECTIONS. A **CONNECTIONS** page is displayed.

In the **Source Address** field, enter the IP address of the DNIF server.

From the **Action** tab, click **Save**.The connection is saved.

> **Note: Saving the connection will not guarantee workstation and A10 server connection. You need to refresh and link the saved connection. The steps for the same are given below.**

Click ![Refresh icon](https://dnif.it/images/installation/Refresh.png) to see whether your saved connection setting is displayed or not.

To link your workstation and the A10 server, click ![connect icon](https://dnif.it/images/installation/Red_Link.png)

A new window with a **hello** message is displayed and the link icon changes it color from red to blue, this implies that the connection is successful between your workstation and A10 server - the connection is encrypted using a self signed SSL certificate.  


