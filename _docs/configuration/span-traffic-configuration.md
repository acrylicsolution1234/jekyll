---
title: Span Traffic Configuration 
permalink: /docs/Configuration/Span-Traffic-Configuration/
section: Configuration
---

# Span Traffic Configuration

## Port Spanning 

Port spanning has been requested for Detector component of DNIF for Threat Management.

![](../.gitbook/assets/image%20%2811%29.png)

SPAN or mirroring can be done using manageable network devices. eg. Manageable Switch, UTM box, Firewalls, etc. 

These devices have inbuilt functionality to probe one or more ports as SPAN input and retrieve their mirrored output from another vacant port. This mirrored output port needs to be connected with Detector's SPAN NIC. 

It is suggested to deploy mirror tapping before firewall \[in Public zone\] to get more meaningful intelligence from Threat Management box.

Detector component will have two NIC cards to it.

One NIC card would be in promiscuous mode to probe the traffic coming towards the firewall from the internet.

The 2nd NIC card is a management port which will be used for the log communication between the Datastore and the detector.

DNIF Detector component monitors network traffic by reading a copy of all network packets at high speeds from the mirror port.

Data packets are analysed with high accuracy in real time and if any anomalies or suspicious behaviour is found they are sent as alerts to the team responsible for mitigation subsequently.



