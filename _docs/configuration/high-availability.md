---
title: High Availability 
permalink: /docs/Configuration/High-Availability/
description: Setting up your adapter in a failover configuration.
section: Configuration
---

# High Availability

## **Overview**

The Service Keepalived is used for IP failover between two servers. Its facilities for load balancing and high-availability to Linux-based infrastructures. It worked on VRRP \(Virtual Router Redundancy Protocol\) protocol.

## **Scenario**

Configuration of two adapters in high availability.

```text
LB1 Server: 192.168.10.111 (eth0)
LB2 Server: 192.168.10.112 (eth0)
Virtual IP: 192.168.10.113
```

![](../.gitbook/assets/image%20%2835%29.png)

## **Pre-requisites**

1. Virtual IP\(VRRP\) require for to configure HA, logs will be initiated on the VRRP.
2. SMTP configuration required on both adapters to send out alerts.
3. At least 3 available IP addresses \(1 for each of at least 2 peer Adapter servers, and 1 virtual IP shared amongst them\).
4. Log will be initiate on VRRP\(virtual IP\).
5. Two AD containers in the same network

## **Configuration**

Log into both Adapter DNIF containers and create/edit Keepalived configuration keepalived.conf file on AD1 Server and add the following configuration.

```text
$ vim /etc/keepalived/keepalived.conf

! Configuration File for keepalived
global_defs {
    notification_email {
        dnif_soc@mydomain.uk
    }
    notification_email_from dnif@mydomain.uk   
    smtp_server 192.168.10.2
    smtp_connect_timeout 30
}
vrrp_instance VI_1 {
    state MASTER
    interface eth0
    virtual_router_id 101
    unicast_src_ip 192.168.10.111    # IP address of Master Server
    unicast_peer {
    192.168.10.112                   # IP address of Backup Server
}   
    priority 101
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        192.168.10.113
    }
}
```

Fields from the configuration file are explained below:

<table>
  <thead>
    <tr>
      <th style="text-align:left">Field Name</th>
      <th style="text-align:left">Description</th>
      <th style="text-align:left">Example</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">notification_email</td>
      <td style="text-align:left">Will send a notification email to the email address mentioned in the field</td>
      <td
      style="text-align:left">
        <p>notification_email { dnif_soc@mydomain.uk</p>
        <p>}</p>
        </td>
    </tr>
    <tr>
      <td style="text-align:left">notification_email_from</td>
      <td style="text-align:left">Alerts will be sent via the email address mentioned in the from field</td>
      <td
      style="text-align:left">
        <p>notification_email_from</p>
        <p>dnif@mydomain.uk</p>
        </td>
    </tr>
    <tr>
      <td style="text-align:left">smtp_server</td>
      <td style="text-align:left">Contains the IP address or domain-name of your SMTP server</td>
      <td style="text-align:left">smtp_server 192.168.10.2</td>
    </tr>
    <tr>
      <td style="text-align:left">interface</td>
      <td style="text-align:left">Interface for inside_network, bound by vrrp</td>
      <td style="text-align:left">interface eth0</td>
    </tr>
    <tr>
      <td style="text-align:left">priority</td>
      <td style="text-align:left">The <code>priority</code> must be highest on the server you want to be the
        master/primary.</td>
      <td style="text-align:left">priority 101</td>
    </tr>
    <tr>
      <td style="text-align:left">virtual_ipaddress</td>
      <td style="text-align:left">the <code>virtual_ipaddress</code> option specifies the interface virtual
        IP address.</td>
      <td style="text-align:left">
        <p>virtual_ipaddress { 192.168.10.113</p>
        <p>}</p>
      </td>
    </tr>
  </tbody>
</table>**Setup KeepAlived on AD2 Server:**

Create or edit Keepalived configuration keepalived.conf file on AD2 Server and add the following configuration.

While making changes in AD2 server configuration file, make sure to set priority values to lower than AD1 server. 

For example below configuration is showing 51 priority value than AD1 has it 101.

```text
$ vim /etc/keepalived/keepalived.conf

! Configuration File for keepalived
global_defs {
    notification_email {
        dnif_soc@mydomain.uk
    }
    notification_email_from dnif@mydomain.uk   
    smtp_server 192.168.10.2
    smtp_connect_timeout 30
}
vrrp_instance VI_1 {
    state BACKUP
    interface eth0
    virtual_router_id 101
    unicast_src_ip 192.168.10.112    # IP address of Backup Server
    unicast_peer {
    192.168.10.111                   # IP address of Master Server
}   
    priority 51
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        192.168.10.113
    }
}
```

> **Note: virtual\_router\_id should be same on both Adapter servers.**

**Start KeepAlived Deamon:**

Start KeepAlived service using the following command on both AD servers and also configure to autostart on system boot.

```text
$ systemctl enable keepalived
$ sudo service keepalived start
```

**Check Virtual IP's**

By default virtual IP will be assigned to master server\(AD1\), In the case of master gets down, it will automatically assign to the backup server\(AD2\). 

Use the following command to show assigned virtual IP on the interface.

```text
$ ip addr show eth0

eth0:  mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:b9:b0:de brd ff:ff:ff:ff:ff:ff
    inet 192.168.10.111/24 brd 192.168.1.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet 192.168.10.113/32 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::11ab:eb3b:dbce:a119/64 scope link
       valid_lft forever preferred_lft forever
```

**Verify IP Fail-over Setup**

Shutdown master server \(AD1 Server\) and check if ips are automatically assigned to backup server \(AD2\).

```text
$ ip addr show eth0
```

Now start AD1 server , IPs will automatically assigned to master server.

Watch log files to insure its working

```text
$ tailf /var/log/messages

Mar 19 17:30:24 localhost Keepalived_vrrp[6958]: VRRP_Instance(VI_1) Transition to MASTER STATE
Mar 19 17:30:25 localhost Keepalived_vrrp[6958]: VRRP_Instance(VI_1) Entering MASTER STATE
Mar 19 17:30:25 localhost Keepalived_vrrp[6958]: VRRP_Instance(VI_1) setting protocol VIPs.
Mar 19 17:30:25 localhost Keepalived_healthcheckers[6957]: Netlink reflector reports IP 192.168.10.113 added
Mar 19 17:30:25 localhost avahi-daemon[1407]: Registering new address record for 192.168.10.113 on eth0.IPv4.
Mar 19 17:30:25 localhost Keepalived_vrrp[6958]: VRRP_Instance(VI_1) Sending gratuitous ARPs on eth0 for
```

**Adapter container snapshot**

Commit the both containers to make the configuration persistent using following command,

```text
$ docker commit -m "<Message>" -a "<Author_name>" -p <Container_ID> <new_image_name>:version
```

Edit the configuration of **docker-compose.yml** and change/replace the **image name** with commited one. 

