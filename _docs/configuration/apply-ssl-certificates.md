---
title: SSL Certificates 
permalink: /docs/Configuration/SSL-Certificates/
description: Adding trusted root certificates to the server
section: Configuration
---

# Apply SSL Certificates

Apply SSL Certificates on DNIF Servers

If you want to send or receive messages signed by root authorities and these authorities are not installed on the server, you must add a trusted root certificate manually.

## For Ubuntu/Debian

#### Use the following steps to add trusted root certificates to/from a server.

Install the ca-certificates package

```text
$ apt-get install ca-certificates
```

Copy your CA to directory /usr/local/share/ca-certificates/ using following command,

```text
$ sudo cp foo.crt /usr/local/share/ca-certificates/
```

Where foo.crt is a trusted root certificate name.

Update the CA store using following command,

```text
$ sudo update-ca-certificates
```

You can test it with following command,

```text
$ wget https://thewebsite.org
```

#### Use the following steps to remove trusted root certificates to/from a server.

Move to directory /usr/local/share/ca-certificates/ and remove your CA,

```text
$ cd /usr/local/share/ca-certificates/
$ rm foo.crt
```

Update the CA store using following command,

```text
$ sudo update-ca-certificates --fresh
```

## For CentOS/RHEL

#### Use the following steps to add trusted root certificates to/from a server.

Install the ca-certificates package

```text
$ yum -y install ca-certificates
```

Enable the dynamic CA configuration feature using following command,

```text
$ update-ca-trust force-enable
```

Copy your CA to dir /etc/pki/ca-trust/source/anchors/ using following command,

```text
$ sudo cp foo.crt /etc/pki/ca-trust/source/anchors/
```

Where foo.crt is a trusted root certificate name.

Update the CA store using following command,

```text
$ update-ca-trust extract
```

You can test it with following command,

```text
$ wget https://thewebsite.org
```

#### Use the following steps to remove trusted root certificates to/from a server.

Move to directory /usr/local/share/ca-certificates/ and remove your CA

```text
$ cd /etc/pki/ca-trust/source/anchors/
$ rm foo.crt
```

Update the CA store using following command,

```text
$ update-ca-trust extract
```



