---
title: Setting up a Query Node 
permalink: /docs/Configuration/Setting-up-a-Query-Node/
section: Configuration
---

# Setting up a Query Node

##  **Overview**

A Cluster consists of a group of Datastores put together in order to store and manage data.

There are 3 types DS which can be included in the cluster:

1. Data Node: Stores all data and response to all the requests. 
2. Master Node: Manages the data and requests, for Data Node. 
3. Client Node/ Query Node: Handles requests and distributes the requests on the corresponding data nodes.\(Does not require much disk space\)

## **Scenario**

Configure cluster on three data nodes and one query node of Datastore.

![](../.gitbook/assets/image%20%2832%29.png)

## **Pre-requisites**

1. Docker and DNIF Services should be deployed on the three data nodes and query node of the DS servers. 
2. All DS nodes should be communicated with each other 
3. Add Query node IP address as DS IP in the docker-compose.yml file on all adapter and correlator servers. 
4. SMTP configuration is mandatory on Query Node server and optional for rest of the data node servers.

## **Configuration**

* Login to all DNIF Datastore containers and stop following DNIF services on all Datastore servers using the following command.

```text
$ supervisorctl 

supervisor> stop nameserver din ingestor ilidsapi 
nameserver: stopped
din: stopped
ingestor: stopped
ilidsapi: stopped
supervisor> exit
```

* Stop elasticsearch service on all DS servers using the following command

```text
$ /etc/init.d/elasticsearch stop

 * Stopping Elasticsearch Server
```

* Open the elasticsearch.yml file using following command and search for “unicast” keyword in the above configuration file and uncomment the line **discovery.zen.ping.unicast.hosts: \["host1:port", "host2:port"\].**

```text
$ vim /dnif/{Depoly-Key}/config/elasticsearch.yml

discovery.zen.ping.unicast.hosts: ["192.168.86.2:8340", "192.168.86.3:8340", "192.168.86.4:8340", "192.168.86.5:8340"]
```

* If you are on DS1 server, replace “host1” as “Query server IP”, “host2” as “DS1 server IP” ,  “host3” as “DS2 server IP” and “host4” as “DS3 server IP” with Port 8340.
* Perform the following changes on Query node, DS2 and DS3 servers as below, 

  * On Query Node Server, replace “host1” as “Query server IP”, “host2” as “DS1 server IP” ,  “host3” as “DS2 server IP” and “host4” as “DS3 server IP” with Port 8340.
  * On DataStore 2 Server, replace “host1” as “Query server IP”, “host2” as “DS1 server IP” ,  “host3” as “DS2 server IP” and “host4” as “DS3 server IP” with Port 8340.
  * On DataStore 3 Server, replace “host1” as “Query server IP”, “host2” as “DS1 server IP” ,  “host3” as “DS2 server IP” and “host4” as “DS3 server IP” with Port 8340.

* On all DS1 server, two more changes are require in the elasticsearch.yml file, uncomment or add following lines.

```text
node.master: true 
node.data: true
```

* Uncomment following line in the elasticsearch.yml file on DS1 server and add minimum master node count as 1,

```text
discovery.zen.minimum_master_nodes: 1
```

* On DS2 and DS3 servers,  uncomment following lines in the elasticsearch.yml file.

  ```text
  node.master: false 
  node.data: true
  ```

* On Query node server, uncomment following lines in the elasticsearch.yml file.

```text
node.master: false
node.data: false
```

> **Note: If Query and data node servers having multiple network interfaces then changes required in the elasticsearch.yml file for the network publish host.**

* The “network.publish\_host” should be the IP address of the same Query/Data node and this change should be done on all Datastores.

```text
network.publish_host: 192.168.86.2
```

* Open Pyro configuration file using below command and change "nhost" & “host” value as IP of Query node server on all data node datastore servers.

```text
$ vim /dnif/{Deploy-Key}/csltuconfig/Pyro

{"Pyro": {"nameserver": {"nhost": "<Query-IP>", "host": "<Query-IP>", "key": "n342@duush.com"}}}
```

* On all data and query node DS servers, open **system** file using below command and set \(“shardcount”: total count of DS\)

```text
$ vim /dnif/{Depoy-Key}/csltuconfig/system

{"system": {"admin": "user@domain.com", "ScopeList": ["AV6l6ZojgSGP0Rd3MEXh"], "localip": "192.168.86.2", "systemid": "AJ6o6ZofgSGFDRd3MEXf", "shardcount": 3, "retention": 45}}
```

For Example: If 3 data node and 1 query node in the cluster then make shardcount count as \(“shardcount” : 3\)

* After the above changes, start the elasticsearch service on all datastore servers using the below command.

```text
$ /etc/init.d/elasticsearch start

* Starting Elasticsearch Server
sysctl: setting key "vm.max_map_count": Read-only file system
                                                                                                                                               [ OK ]
```

> **Note: Start elasticsearch service on every DS  servers at the same time.**

* Follow the below steps __on Query node server to check the database health status,

```text
$ python 

>>> from elasticsearch import Elasticsearch 
>>> es = Elasticsearch([{'port':8240}]) 
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 4 3 111 111 0 0 0 \n'
```

The above commands will show the health status of database whether the status is Green, Yellow or Red.

* The cluster health API helps to get status on health of the cluster. 
  * Green state: It means all shards are allocated. 
  * Yellow state: It means the primary shard is allocated but replicas are not. 
  * Red state: It means the specific shard is not allocated in the cluster.
* If Database health status is in Green state, start below DNIF services on Query node server using the following command,

```text
$ supervisorctl 

supervisor> start nameserver din ingestor ilidsapi 
nameserver: started
din: started
ingestor: started
ilidsapi: started
supervisor> exit
```

* Start below DNIF services on all data node servers using the following command,

```text
$ supervisorctl 

supervisor> start din ingestor ilidsapi 
din: started
ingestor: started
ilidsapi: started
supervisor> exit
```

* Log into adapter DNIF container, open Pyro configuration file and add all data node IP's in Pyro file using the following command,

```text
$ vim /dnif/<Deploy-Key>/csltuconfig/Pyro

{"Pyro": {"nameserver": {"nhost": "<Query_node-IP>", "host": "<Query_node-IP>", "key": "n342@duush.com"}, "DC": ["<DS1-IP>", "<DS2-IP>", "<DS3-IP>"]}}
```

With the above configuration, we have successfully configured cluster on three data nodes of the datastores along with Query node.

