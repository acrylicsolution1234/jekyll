---
title: Installing Licenses 
permalink: /docs/Configuration/Installing-Licenses/
section: Configuration
---

# Installing Licenses

## **How to apply License**

* Open the email with subject **DNIF-Getting Started** and download the **license** and **signature.bin** files.
* Copy the license files download by you on the DNIF server at any temporary location like /var/tmp.
* Login into DNIF server and move these files to the **LICENSE** folder.

```text
$ mv /var/tmp/license /dnif/{Deploy-Key}/LICENSE
$ mv /var/tmp/signature.bin /dnif/{Deploy-Key}/LICENSE
$ cd /dnif/{Deploy-Key}/LICENSE
$ ls

license    signature.bin
```

> **Note: LICENSE folder can be found within the your\_deployment\_key folder of your installation directory, as defined in the docker-compose.yml file. The your\_deployment\_key folder is basically deployment key you entered while editing the configuration file.**

* Login into your Datastore or A10 container and restart the ingestor and din services to apply license using following commands,

```text
$ supervisorctl 

supervisor> restart din ingestor 
din: stopped
ingestor: stopped
din: started
ingestor: started
supervisor> exit
```

> **Note: If there are multiple Datastore servers in a cluster, then you need to apply provided license\(by DNIF team\) on all Datastore server nodes.**

## **Configure alerts for licence expiry updates**

* We will configure alerts for the license expiry,  auto alerts are sent out after the usage is crossed above 50, 80, 85, 90, 95, 98, 100.
* Login to your **Datastore** or **A10** container. Read more on [how to Login to the docker container](https://dnif.it/docs/guides/tutorials/access-dnif-container-via-ssh.html).
* Open **system** configuration file and set the alert using following command,

```text
$ vim /dnif/{DEPLOY_KEY}/csltuconfig/system

{"system": {"admin":"admin@dnif.it", "localip": "172.16.10.78", "retention": 45, "systemid": "AV6IOeNhgJGP0Rd3JTtv", "ScopeList": ["AV6IOeNhgJGP0Rd3JTtv"], "shardcount": 1}}
```

In above configuration, we have added **"admin":"admin@dnif.it"** field in the system configuration file, add valid email id to receive license expiry alerts.

* Restart the agent79 service after adding the above configuration.

```text
$ supervisorctl

supervisor> restart agent79 
agent79: stopped
agent79: started
supervisor> exit
```

> **Note: If there are multiple Datastore servers in a cluster, then we need to add this configuration only on the primary/query node Datastore server.**

