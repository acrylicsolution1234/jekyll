---
title: Load Balancing 
permalink: /docs/Configuration/Load-Balancing/
description: Configuring your adapters for load balancing
section: Configuration
---

# Load Balancing

## **Overview**

Load Balancing refers to efficiently distributing network traffic across multiple backend servers.

![](../.gitbook/assets/image%20%2869%29.png)

## **Pre-requisites** 

1. Latest open source NGINX built with the --with-stream configuration flag, or latest NGINX Plus \(no extra build steps required\).
2. An application service that communicates over TCP or UDP.
3. Upstream servers, each running the same instance of the application or service.
4. LB server should be communicated with all adapters.
5. Logs will be initiated on LB server from the log sources.

## **Configuration**

Installing Nginx Latest Version from Official Nginx Repository

Nginx is included in Ubuntu software repository but the version is out-of-date. To install the latest version of Nginx, we will need to add the official Nginx repository.

Login into LB server and open up sources.list file with a text editor such as vim and add ngnix repository.

```text
$ vim /etc/apt/sources.list

deb http://nginx.org/packages/mainline/ubuntu/ xenial nginx 
deb-src http://nginx.org/packages/mainline/ubuntu/ xenial nginx
```

In order to verify integrity of packages downloaded from this repo, we need to import Nginx public key using the commands below.

`$ wget http://nginx.org/keys/nginx_signing.key`

`$ sudo apt-key add nginx_signing.key`

Once the repository is added to your Ubuntu 16.04 system, run the following command to update repository info and install Nginx.

```text
$ sudo apt-get update
$ sudo apt-get install nginx
```

If you have installed Nginx from Ubuntu software repository, you need to remove it before executing the above two commands.

```text
$ sudo apt-get remove nginx nginx-common nginx-full nginx-core
```

Also you may want to back up the main Nginx config file /etc/nginx/nginx.conf because it will be replaced with a new nginx.conf file.

```text
$ sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
```

Your existing server block files will be intact.

After Nginx is installed, we need to unmask nginx.service file using the command below.

```text
$ sudo systemctl unmask nginx
```

Then start Nginx.

```text
$ sudo systemctl start nginx
```

If nginx.service isn’t unmasked and you try to start Nginx, you will see the following error.

`failed to start nginx.service: unit nginx.service is masked.`

To enable Nginx to auto start at boot time, run

```text
$ sudo systemctl enable nginx
```

To check the status of Nginx, run

```text
$ systemctl status nginx

● nginx.service - LSB: Stop/start nginx
   Loaded: loaded (/etc/init.d/nginx; bad; vendor preset: enabled)
   Active: active (running) since Sat 2017-01-07 02:58:50 UTC; 3min 16s ago
     Docs: man:systemd-sysv-generator(8)
 Main PID: 1708 (code=exited, status=0/SUCCESS)
   CGroup: /system.slice/nginx.service
           ├─3429 nginx: master process /usr/sbin/nginx -c /etc/nginx/nginx.con
           └─3430 nginx: worker process
```

To check Nginx version, use this command:

```text
$ nginx -v

nginx version: nginx/1.15.3
```

Set the soft and hard limit in limits.conf file.

```text
$ vim /etc/security/limits.conf

web soft nofile 65535
web hard nofile 65535
```

Set the ULIMIT in nginx file.

```text
$ vim /etc/default/nginx

ULIMIT="-n 65535"
```

Open configuration file using text editor you prefer, for example with vim and add following lines:

```text
$ vim /etc/nginx/nginx.conf

user root root;

# One worker process per CPU core.
worker_processes 8;

worker_rlimit_nofile 65535;

pid /var/run/nginx.pid;

events {
    #
    # Determines how many clients will be served by each worker process.
    # (Max clients = worker_connections * worker_processes)
    # Should be equal to `ulimit -n`
    #
      worker_connections 65535;

    #
    # Let each process accept multiple connections.
    # Accept as many connections as possible, after nginx gets notification
    # about a new connection.
    # May flood worker_connections, if that option is set too low.
    #
      multi_accept on;

    #
    # Preferred connection method for newer linux versions.
    # Essential for linux, optmized to serve many clients with each thread.
    #
      use epoll;
}

stream {

       # Add ‘server’ directives inside each of the ‘upstream’ block. The protocol is defined for the
       # entire upstream group when you give the parameter in ‘listen’ directive i.e. if you define ‘udp’ in
       # the listen directive for ‘xyz’ upstream ,then it is defined for the entire upstream group. 
       # The balancing method used here the default Round-Robin Method.
       # We can also change the amount of time a server is marked as unavailable, by including the fail_timeout option to the server directive in the  	     # upstream group. With the following setting, NGINX Plus marks failed upstream servers as unavailable for 60 seconds.
	 upstream dnif_adapter {
                server <AD1 IP>:514 fail_timeout=60s;
                server <AD2 IP>:514 fail_timeout=60s;
        }

        server {
        # For UDP traffic , add ‘udp’ parameter in the ‘listen’ directive. For TCP traffic , no
        # need to enter any parmeter, because tcp is the default protocol.
                listen 514 udp;
        # Add "proxy_pass" directive to define the proxied server or an upstream group to
        # which the server forwards traffic        
		proxy_pass dnif_adapter;
	# after the proxy_pass line to make nginx a transparent proxy and passing on the original log source IP address. 
                proxy_bind $remote_addr transparent;
	# how many responses NGINX expects for each proxied UDP request. In our case, after receiving a single response NGINX immediately stops 	      # waiting for further responses, which frees up the memory and socket used for that session.
                proxy_responses 1;
	# determines how long NGINX waits for a response from the server (here we’re reducing the default 10 minutes to 1 second). If NGINX receives 	      # no response within this period, it tries the next server in the upstream group and marks the unresponsive upstream server as unavailable for 	    # a defined period (10 seconds by default) so that no other clients suffer a timeout‑induced delay during that time.
                proxy_timeout 1s;
        }
}

```

Then save the file and exit the editor.

Then use the following command to reflect changes,

```text
$ sudo service nginx restart
```

Check that nginx starts successfully. 

> **Note: If the restart fails, take a look at the /etc/nginx/nginx.conf you just created to make sure there are no mistypes or missing semicolons.**





