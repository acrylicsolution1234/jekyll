---
title: Configuring SMTP 
permalink: /docs/Configuration/Configuring-SMTP/
description: Configuring SMTP
section: Configuration
---

# SMTP Configuration

In order to enable email alerts and reports, you need to configure your SMTP server details on the DNIF containers.

The configuration can be done manually by editing a **YAML** template file that comes with the **Datastore**, **Correlator**, and **A10** installations.

#### NETWORK PREREQUISITES <a id="network-prerequisites"></a>

The **Datastore**, **Correlator**, and **A10** containers will connect to your email server on the SMTP port from their network address. 

For example, if your email server is configured for SMTP ports 25, 465, 587, or any other, make sure that these ports are reachable from the DNIF Container.

**STEPS TO CONFIGURE SMTP IN DNIF CONTAINER**

Login to your [Data Store, Correlator, and A10 containers](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh) and move to the `/dnif/{Deploy-Key}/tool` folder path.

```text
$ cd /dnif/{Deploy-Key}/tool/
```

Here, you will find smtp.yaml template file and configsmtp.py python script.

```text
$ cd /dnif/{Deploy-Key}/tool/
$ ls
configsmtp.py smtp.yaml
```

Open the **smtp.yaml** template file and edit the required fields, while commenting/uncommenting as needed, with the correct values.

```text
# Example config for internal smtp (without authentication)
- internal:
domain: 192.168.10.32
- from: autonotif@mydomain.com
- target_email: myemail@domain.com

# Example config for external smtp (with authentication)
# - external:
# domain: 192.168.10.32
# username: myusername
# password: mypassword
# port: 465
# tls: 1
# - from: autonotif@mydomain.com
# - target_email: myemail@domain.com
```

This YAML template is divided in two sections:

* Without Authentication \[Internal SMTP\]
* With Authentication \[External SMTP\]

You will comment on the required section, as per your SMTP server requirements, and uncomment on the not required ones.

> **Note: YAML file is sensitive to white spaces and indentation. Hence, it is strongly recommended to make a copy of the template file before editing it.**

Fields from the YAML template are explained below:

| Field Name | Description | Example |
| :--- | :--- | :--- |
| domain | domain field contains the IP address or domain-name for your SMTP server | domain: postmaster@mydomain.uk or domain: 126.112.xx.yy |
| port | SMTP port on your email-server \[e.g. 25, 465, 587, etc.\] | port: 465 |
| from | DNIF alerts and reports shall be sent via the email address mentioned in the from field | from: dnif@mydomain.uk |
| target\_email | During SMTP configuration configsmtp.py script will send a test email to the email address mentioned in the target\_email field | target\_email: dnif\_soc@mydomain.uk |
| username | Authorized SMTP username for sending emails | username: smtp\_user |
| password | SMTP authentication password for the username above | password: smtp\_password |
| tls | If your SMTP server is using a secure TLS, then set tls to 1, else to 0 | tls: 1 or tls: 0 |

**An example for the smtp.yaml:**

```text
# Example config for internal smtp (without authentication)
#- internal:
#	domain: 192.168.10.32
#- from: autonotif@mydomain.com
#- target_email: myemail@domain.com

# Example config for external smtp (with authentication)
- external:
domain: smtp.dnif.it
username: postmaster@dnif.it
password: S4**************!J
port: 587
tls : 1
- from: autonotif@dnif.it
- target_email: support@dnif.it
```

Save changes and close the **smtp.yaml** file.

Now, execute the **configsmtp.py** script to apply the **smtp.yaml** configurations on the DNIF containers.

```text
$ python configsmtp.py

Configuration of SMTP tested and applied successfully
```

If the configuration is done correctly, you’ll see a ‘success’ message after running **configsmtp.py**

Besides, you will receive an email on **target\_email** with the subject line **Test Email** with the sender’s name defined in the **from** field within **smtp.yaml**.

> **Note:** **After successful configuration of SMTP, in case there is a scenario of accidental or planned restart of your docker instance, the changes which were made in “smtp.yaml” will be set to a default configuration, however, the SMTP configuration details will be automatically loaded from the file, `/dnif/{Deploy-Key}/csltuconfig/agent29` which can be found in your DNIF docker instance. To put it simply you don’t have to edit or redo the configuration again.**

