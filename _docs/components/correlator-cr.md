---
title: Correlator 
permalink: /docs/Components/Correlator/

description: >-
  The Correlator is an analytics engine, that is responsible for all your
  computing and correlation needs.
section: Components
---

# Correlator \(CR\)

## Overview <a id="overview"></a>

The correlator powered by the DQL engine works in tandem with:

* Generating data for custom queries
* Powering-up dashboards with relevant visualizations
* Raising modules internal to DNIF or triggering external systems to take relevant action using correlated events

The DQL console is your command centre to hunt down threats that impact your organization.

## Functionality <a id="functionality"></a>

| Functionality | Description |
| :--- | :--- |
| **Correlate** | Data is baselined to a common standard and pre-existing rules are run on the data for primary correlation. |
| **Visualize & Hunt** | Use the DQL console to analyse data, proactive threat hunting or to define new rules to set up reactive triggers. Get them displayed on dashboards using widgets. |
| **Respond & Orchestrate** | Trigger automated response systems that are integrated with external services or devices. |

​‌

## Assets <a id="assets"></a>

![Correlator \(CR\)](../.gitbook/assets/cr.png)

