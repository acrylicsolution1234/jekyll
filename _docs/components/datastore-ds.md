---
title: Datastore
permalink: /docs/Components/Datastore/

description: >-
  The Datastore is responsible for indexing all your data and get it amped up
  for being queried.
section: Components
---

# Datastore \(DS\)

## Overview

The Datastore \(DS\) is responsible for storing and indexing all the data that is received from the Adapter. Some of the key functions that the DS performs are as follows - 

* _Receives_ events from the Adapter.
* _Indexes_ all the events that include the raw event, the parsed meta data and the enriched data in a Big Data Index based on Apache Lucene.
* _Maintains concurrency_ in a distributed environment.
* _Accounts_ for data received at source and reports.
* _Manages licenses_ for DNIF and enforces the preset quota.
* _Manages high availability_ to recover from failure.
* _Aggregates_ results from across the cluster.
* _Store_ events on hot, warm and cold indexes spread across multiple disk types.
* _Maintains authenticated sessions_ for the console users.
* _Manages indexes_ using backups, and data pruning.

## Features

Listed are the technical features and capability of the Datastore \(DS\)

| Function | Features |
| :--- | :--- |
| Collection | Indexes parsed and enriched data from the Adapter |
| Scale | Can be load balanced, can also auto-scale |
| Compression | Indexes are compressed to upto 50% of the data value |
| Partitioning | Multi-tenancy capabilities for holding multi department data |
| Management | Can automatically backup and restore indexes |

## Functionality



| Functionality | Description |
| :--- | :--- |
| **Horizontal Scaling** | With the help of clustering, DS are designed to scale up and down seamlessly without breaking operations. Add/remove additional storage. Compute without any "rip and replace" process or redeployment. |
| **Fault Tolerance** | Clustered setup also provides the required resiliency and redundancy for high availability features. In case one of the nodes in the DS cluster goes down, there will be no data loss as the respective copy of the data would be present on other active node\(s\). |



## Assets

![DNIF Datastore \(DS\)](../.gitbook/assets/ds.png)

