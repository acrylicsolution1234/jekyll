---
title: Adapter 
permalink: /docs/Components/Adapter/
redirect_from: /docs/Components/index.html
description: The Adapter is responsible for the data ingestion process.
section: Components
---

# Adapter \(AD\)

## Overview

The Adapter is the connection between the data sources and DNIF. Furthermore, it -

* _collects_ events over different channels like syslog, http\(s\), netflow etc.
* _parses_ these events using relevant parsers.
* _annotates_ events in order to better identify events from different devices.
* _enriches_ these events to add relevant context, geo-data, and threat intelligence.

## Features

Listed are the technical features and capabilities of the Adapter \(AD\):

| Stage | Function | Features |
| :--- | :--- | :--- |
| Collection | Collect | Collectors for file, syslog, netflow, http\(s\), database, etc |
|  |  | Agentless event collection |
|  |  | Can be load balanced, can also auto-scale |
|  |  | Adds UTC Time-stamp for better triage across geographic deployments |
| Data Preparation | Parsing | Compact parsing language |
|  |  | High speed line parsers |
|  |  | Multi-pass engine for accurate extraction |
|  |  | Master regex for enhanced speed |
|  |  | Substitution ability for common context |
|  |  | Single, inline engine |
|  |  | High speed multi-pass annotation |
|   |  | Multi-processed for speed optimisation |
| Data Preparation | Enrichment | Pluggable framework for integrations |
|  |  | Read and cache engine |
|  |  | Lazy cache engine |
|  |  | Zip scan/ stack and update capability |

## Functionality

| Functionality | Description |
| :--- | :--- |
| **High Availability \(HA\)** | The AD can be setup in an active/ passive configuration where if the primary AD fails, the secondary AD is available on standby and will take over the workload. This configuration uses \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_ to create a virtual IP which is mapped in each integrated device \(the IP that is used to receive the events\). In a best case operation, the primary AD will be mapped in with the virtual IP. However when the primary AD fails, the secondary AD which is on standby with the same configurations will be mapped to the virtual IP address. |
| **Load Balancing** | If the input feed is larger than the max capacity of a specific AD, there will be a need to load balance multiple ADs to share the workload. This load balancing could be done using a commercially available product or using NGNIX that is open source, to setup this configuration. Load Balancing will also provide resiliency or high availability features. |

## Assets

![DNIF Adapter \(AD\)](../.gitbook/assets/ad.png)

