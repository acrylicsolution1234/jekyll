---
title: How to create User 
permalink: /docs/User-Management/How-to-create User/
description: User Creation
section: User-Management
---

# How to create User

Admin User has the privileges to create a new User.

**How to promote your DNIF Account as Admin:**

Follow the below steps to promote your account as admin account,

Initially you have to contact DNIF Support to promote your account as admin account for your Scope/DNIF Setup. After that, you are able to Create New User.

As your request, DNIF will promote your account to admin privilege. After getting admin access two new buttons are getting enable in **MANAGEMENT** tab i.e **USER** and **SCOPE**.

![](https://lh3.googleusercontent.com/ou65eCOxls3B9TD1lsLvRFYoxrQHaac36nS4R-6m_qmK724rPFzc4ABsX07DDx_U_9y2dZeaFaN29U2HbyiOBKZ9YHPNhSxxeVWHGaysMCK9XWVxOo8DO7aOOCjGCbfRhgvq0oi7)

Click on **USER** button, after clicking on the USER button you will get the list of Users those has access to that specific repository.

![](https://lh6.googleusercontent.com/Ibcfah08uuFEahr5MUGZmx7w5mOk27a_QhPpU0jyoBiVAuetIv-7_AJPObRZagYzPjace0TnWozUAzE-aer9uolmmGed8Ovq7Sqyc9mqWc-jZM_rmkOyukjQD3dX78HQAJLeAi0f)

For to create new user, click on **+**  button, Admin has to fill all required information related to new User, also Admin User can define the role of the new User like General User/Admin User and Admin is able to assign scope to that User and restrict the access of User for views.

![](https://lh5.googleusercontent.com/knkYhSMmg7k3LuO5cMOlaDix6e0zbhSNIKszcCDKMFjnYO9zdc28w0Hu1fYGT2A6DvrY6UVXqC8pzjHFD02J8sbrb5ELS4PWC1bdPAV-AjjRMbMbnxHfXMvPnIPDM3Wxk2ndMWEg)

Fields from ADD NEW USER page are explained below,

| Field Name | Description |
| :--- | :--- |
| First name | You can enter your First name. |
| Last name | Enter your Last name |
| Mobile number | Here, add your mobile no, its a mandatory field |
| Home phone no | Enter your Home phone no, its not a mandatory. |
| Office phone no | Enter your Office phone no, its not a mandatory. |
| Username | Enter your username, username should be your email id. |
| Role | User can assigned user role as GU\(General User\) or AU\(Admin User\). |
| Scope | Select the Scope from the Scope list which you want to give access. |
| Views | Select the Views for the User, here you can restrict the access of DNIF pages. |

Click on save button, new user will be created for requested scope and user credentials are displayed on the console.

![](../.gitbook/assets/image%20%285%29.png)

> **Note:  Admin User can create new Admin User and remove the access of the mapped user.**

