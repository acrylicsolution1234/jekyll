---
title: Change User Privileges 
permalink: /docs/User-Management/Change-User-Privileges/
section: User-Management
---


# Change User Privileges

Admin User has the privileges to change user permissions and he can remove the access of the mapped user.

**Edit User Privilege Using DNIF Admin Account**

For existing users, if you want to add access of new Scope or remove the access of scope or you want to change user permissions,  click on **MANAGEMENT &gt; USER** button.

After clicking on the **USER** button, you will get the list of Users those has access to that specific scope, click on the **EDIT** button for to change user permissions.

![](https://lh5.googleusercontent.com/LVEqd9bzTIuw0stTGGmxP36cZvpXUSOMT7TmP9zg-OKD6m35Ttl6iCTPfw4eWJXlxT3E95fEJPgmJFN6Cs4aXGl-K5COqghlj6f07xGfn-qRsDyN-_2KfVSVl1ZSDaVkxUaVxFzN)

You can add the scope in their account else you can be removed it. Also, you can restrict the access of user for views.

![](https://lh5.googleusercontent.com/Z_Sy8GfLmM8wZDvaeLs5GBKOIxcNCbyJ1zPksWjDNUn5RGIu7RFdhrKWa8eW0fZTfgQQ9180vcLc-GG64ebWhMr5LRYkXni7z9LDgh3nzDHjrJb3RbncxvXvw3-8xb8LP395x6-O)

After completed necessary changes, click on **save** button to make changes persistent.

