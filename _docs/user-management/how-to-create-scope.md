---
title: How to create Scope 
permalink: /docs/User-Management/How-to-create-Scope/
section: User-Management
---
# How to create Scope

Admin User has the privileges to create a new Scope \(for sharing same systemid of current scope\).

> **Note: This feature is available for shared setup only.**

**Create a new scope using DNIF Admin account**

You can create a new scope using DNIF admin credentials, click on **MANAGEMENT** &gt; **SCOPE** button.

![](https://lh3.googleusercontent.com/EPby8FmSpyieWtl4qCQ14QC9fuoMJV04PZ7PQ31f7Ij98HnmZMFJmg5dOU8Hjs_bAK6uQHnWPj6Uxix44-xB0Mmanv0fNPj2bcqSbjKU0j4kCbzcaJh-QdExg7WarkxzGRQIua-K)

After clicking on **SCOPE** button, you will get the list of all scope, those have the admin privileges access.

By using **+** button,  Admin user can create a new scope as per requirement.

![](https://lh4.googleusercontent.com/FvqP9e3SUakpjJKfQpZyz3eBJA81yK7NVTuWtXNtV780q5SJ4NQL6_3g4hk53iW0cbAlyfkAM8EhbxrZQwEqYpL_0sgCSaAApxmjkeVUvOrNANqbEbJXHhInc_IBkioZoQ2DkPgD)

After clicking on **+** button, Admin has to fill all required information.

![](https://lh4.googleusercontent.com/r5TFf_Cxtf6zQdWd0tNHibQMqrTYybh907XMrIE9uSaiLI7Qz-k_rgW0Z3cKapeaSfmElIOvRHq4ooj2mq2DEEBc0phDDtrpCWtdjFq2SiuP2HUYlG0SsKgLIyi8UCKrxmhuHIBY)

Fields from ADD NEW SCOPE page are explained below,

| Field Name | Description |
| :--- | :--- |
| Scope name | Enter the scope name, which you want to create. |
| System name | System name is your Datastore name. |
| Scope description | Add the description as per Scope role. |

Click on save button, new Scope will be created and you can verify it from your scope list.

