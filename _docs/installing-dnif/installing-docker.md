---
title: Installing docker 
permalink: /docs/Installing-DNIF/Installing-Docker/
section: Installing-DNIF
---
# Installing docker

All DNIF Components come in the form of [docker](https://www.docker.com/) containers \(more on [why docker](https://www.docker.com/why-docker)\). Oh, we did docker just so that it's easy for you to install and maintain. In this guide we are going to install docker on a standard [Ubuntu Server 16.04 LTS](http://releases.ubuntu.com/16.04/ubuntu-16.04.5-server-amd64.iso) version installed with OpenSSH.

## OS requirements

To install Docker CE, you need the 64-bit version and `x86_64` \(or `amd64`\) architecture, of one of these Ubuntu versions,

* Bionic 18.04 \(LTS\)
* Xenial 16.04 \(LTS\)

Installing Ubuntu is out the scope of this document, however for this guide, we will allocate all available space to the `/` \(root\) partition.

To get started with Docker CE on Ubuntu, make sure you [meet the prerequisites](https://docs.docker.com/install/linux/docker-ce/ubuntu/#prerequisites), then [install Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce).

## Install Docker CE

Setting up docker on the host can be customised. You can lookup [docker instructions](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce) for installing the software, or just follow us through this page to get started.

You can install Docker CE in different ways, depending on your needs:

* Most users [set up Docker’s repositories](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository) and install from them for ease of installation and to upgrade tasks. This is the recommended approach.

### Install using the repository

Before you install Docker CE for the first time on a new host machine, you need to set up the Docker repository. Afterwards, you can install and update Docker from the repository.

#### **Set up the repository**

* Updating your operating system using`apt-get update` will download all the latest content in the package repositories and get you loaded up. You might be prompted for your password.

```text
$ sudo apt-get update
```

* Install packages to allow `apt` to use a repository over HTTPS:

```text
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
```

* Add docker's official GPG key to your repository using the command:

```text
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

* Validate your key, ensure the outcome is the same as below:

```text
$ sudo apt-key fingerprint 0EBFCD88
pub   4096R/0EBFCD88 2017-02-22
      Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid                  Docker Release (CE deb) <docker@docker.com>
sub   4096R/F273FCD8 2017-02-22
```

* Add the docker stable repository to your host:

```text
$ sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
```

* Update the `apt` package index using following command:

```text
$ sudo apt-get update
```

* Install the latest version of Docker CE and containerd, or go to the next step to install a specific version:

```text
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

* To install a _specific version_ of Docker CE, list the available versions in the repo, and then select and install:

        List the versions available in your repo using following command:

```text
$ apt-cache madison docker-ce
docker-ce | 5:18.09.1~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
docker-ce | 5:18.09.0~3-0~ubuntu-xenial | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
docker-ce | 18.06.1~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
docker-ce | 18.06.0~ce~3-0~ubuntu       | https://download.docker.com/linux/ubuntu  xenial/stable amd64 Packages
```

* Install a specific version using the version string from the second column, for example, `docker-ce | 18.06.1~ce~3-0~ubuntu`

```text
$ sudo apt-get install docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io
```

* At this point you have successfully installed docker on your host.
* Next you should try the customary `hello world` and test if all is working at the docker end. This command downloads a test image and runs it in a container. When the container runs, it prints an informational message and exits.

```text
$ sudo docker run hello-world
```



