---
title: Standalone\(A10\) Deployment 
permalink: /docs/Installing-DNIF/Standalone\(A10\)-Deployment/
section: Installing-DNIF
---

# Standalone\(A10\) Deployment

If you are experimenting with / testing DNIF, and your data volume is _below 1.5 TB per month_, then the standalone deployment is for you. The free [forever plan](https://dnif.it/signup.html?plan=freeforever) \(free tier\) is available only in the standalone deployment model.

![](../.gitbook/assets/image%20%2828%29.png)

A10 stands for All-in-One which includes all three \([adapter](../components/adapter-ad.md), [datastore](../components/datastore-ds.md) and [correlator](../components/correlator-cr.md)\) components required by the platform, integrated into a single instance.

The A10 allows you to have a retention policy, but it does not have the capability to provide any in product redundancy or high availability.

All upgrades from the standalone deployment to the standard or enterprise deployment will require a full reinstall and migration, while the other models can easily be upgraded or extended.

## Installing DNIF

* All DNIF Components come in the form of [docker](https://www.docker.com/) containers \(more on [why docker](https://www.docker.com/why-docker)\), you can lookup [docker instructions ](https://docs.dnif.it/admin-guide/quick-start-guide/installing-dnif/installing-docker)for installing the software on ubuntu.
* After successfully installing docker, check and allow the [network pre-requisites](https://docs.dnif.it/admin-guide/preparing-to-install/network-pre-requisites) before diving into the A10 installation.
* Let's begin by setting up a home folder, this is where all the data and configurations are going to be stored. This folder will hold everything and will be mapped with the docker A10 container.

```text
$ sudo mkdir /dnif
$ cd /dnif
```

* Just to make things easy, we built out a `docker-compose` file that can help you configure the image. But first, we install `docker-compose` and for doing that, we install `python-pip` and then straighten our `locales` settings.

```text
$ sudo apt-get install -y python-pip
```

* Install `docker-compose`

```text
$ sudo pip install docker-compose
```

* Create a A10 container image configuration or the docker-compose file using `vim` editor

```text
$ cd /dnif
$ sudo vim docker-compose.yml
```

* Contents of `docker-compose.yml` should be:

```text
version: '2.0'
services:
 comp-ship-a10:
   image: "dnif/ship-a10-km:latest"
   network_mode: "host"
   cap_add:
    - NET_ADMIN
   ports:
    - "127.0.0.1:826:22/tcp"
    - "443:443/tcp"
    - "8350:8350/tcp"
    - "8351:8351/tcp"
    - "8355:8355/tcp"
    - "8340:8340/tcp"
    - "9234:9234/tcp"
    - "9236:9236/tcp"
    - "514:514/udp"
    - "514:514/tcp"
    - "8362:8362/tcp"
   volumes:
    - /path/to/volume:/dnif
   environment:
    - "DKEY=DEPLOY_KEY"
    - "CRIP=127.0.0.1"
    - "bootstrap_memory_lock=true"
   ulimits:
    memlock:
     soft: -1
     hard: -1
   container_name: comp-dnif-a10
```

Edit the configuration of **docker-compose.yml** as shown:

| Field Name | Change to |
| :--- | :--- |
| image: "dnif/ship-a10:latest" | image: "dnif/ship-a10:&lt;To install a _specific version_ of DNIF or you can use `latest` for latest version&gt;" |
| /path/to/volume:/dnif | /dnif:/dnif |
| DKEY=KEY | DKEY=’Enter the deployment A10 key which you received on your DNIF Getting Started email’ |

#### Increase the vm.max\_map\_count parameter

* If you are using a VM, the default operating system limits on mmap counts is likely to be too low, which may result in out of memory exceptions. You may need to increase the `vm.max_map_count`parameter.
* You can increase the limits by running the following command as `root`

```text
$ sysctl -w vm.max_map_count=262144
```

* To set this value permanently, update the `vm.max_map_count` setting in `/etc/sysctl.conf`

```text
$ vim /etc/sysctl.conf
vm.max_map_count=262144
```

* Reload the config as root

```text
$ sysctl -p
```

* Check the new value using following command:

```text
$ cat /proc/sys/vm/max_map_count
```

* After increasing the `vm.max_map_count` parameter, move into the folder where you have created `docker-compose.yml` file and run `docker-compose` to download and start the image.

```text
$ cd /dnif
$ sudo docker-compose up -d
```

* The time taken by the command above to fetch the DNIF image from the online repository will depend on the bandwidth of your network connection. 

  Once the installation is complete, you will see an installation complete message within the terminal or an ASCII art of DNIF. 

  The command above successfully performs a “pull” operation, which gets the latest code from the online repository and runs the same.

* Finally, ensure our container has started up. The following command should give you a list of all the running containers and you should see your container in the `running` state.

```text
$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS               NAMES
d682sfgh078f        dnif/ship-a10:latest   "/bin/bash /usr/sr..."   5 minutes ago       Up 1 minute                             comp-dnif-a10
```

* Click to install and apply [license](https://docs.dnif.it/admin-guide/configuration/installing-licenses) on your A10 server.
* Click here to [integrate log sources](https://docs.dnif.it/admin-guide/ingesting-data/available-connectors) with DNIF.

 

