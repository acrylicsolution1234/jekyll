---
title: Distributed Deployment 
permalink: /docs/Installing-DNIF/Distributed-Deployment/Distributed-Deployment/
section: Installing-DNIF
sub-section: Distributed-Deployment
---

# Distributed Deployment

You have tried the product and you are ready to go mainstream. The distributed deployment is tiered and allows you to grow based on your needs.

![Standard Deployment](../../.gitbook/assets/image%20%2857%29.png)

The distributed installation includes a single instance of each component making the deployment tiered and distributed. This deployment model can be scaled out by adding additional components to the cluster and growing the overall capacity of the installation.

This model also allows you to distribute the components across locations to achieve shared or aggregated topologies. Allows you to scale effectively in any enterprise requirement.

The distributed deployment does not provide in product redundancy or high availability.

The Datastore can be scaled by adding new Datastore components and build a cluster, this change does not require downtime but just reconfiguration.



