---
title: Datastore Deployment
permalink: /docs/Installing-DNIF/Distributed-Deployment/Installing-Datastore/
section: Installing-DNIF
sub-section: Distributed-Deployment
---
# Datastore Deployment

All DNIF Components come in the form of [docker](https://www.docker.com/) containers \(more on [why docker](https://www.docker.com/why-docker)\), you can lookup [docker instructions ](https://docs.dnif.it/admin-guide/quick-start-guide/installing-dnif/installing-docker)for installing the software on Ubuntu.

After successfully installing docker, checks and allow the [network pre-requisites](https://docs.dnif.it/admin-guide/preparing-to-install/network-pre-requisites) before diving into Datastore installation.

## Installing Datastore

To know more about [Datastore](https://docs.dnif.it/admin-guide/components/datastore-ds) and how it works.

* Let's begin by setting up a home folder, this is where all the data and configurations are going to be stored. This folder will hold everything and will be mapped with the docker Datastore container.

```text
$ sudo mkdir /dnif
$ cd /dnif
```

* Just trying to make things easy, we built out a `docker-compose` file that can help you configure the image, but first we will install `docker-compose` and for doing that that we install `python-pip` and then straighten our `locales` settings.

```text
$ sudo apt-get install -y python-pip
$ export LC_ALL="en_US.UTF-8"
$ export LC_CTYPE="en_US.UTF-8"
$ sudo dpkg-reconfigure locales
```

* Install `docker-compose`

```text
$ sudo pip install docker-compose
```

* Create a Datastore container image configuration or the docker-compose file using `vim` editor.

```text
$ cd /dnif
$ sudo vim docker-compose.yml
```

* Contents of `docker-compose.yml` should be,

```text
version: '2.0'
services:
 comp-ship-ds:
   image: "dnif/ship-ds-km:latest"
   network_mode: "host"
   cap_add:
    - NET_ADMIN
   ports:
    - "127.0.0.1:826:22/tcp"
    - "443:443/tcp"
    - "8350:8350/tcp"
    - "8351:8351/tcp"
    - "8355:8355/tcp"
    - "8340:8340/tcp"
   volumes:
    - /path/to/volume:/dnif
   environment:
    - "DKEY=DEPLOY_KEY"
    - "CRIP=IP Address"
    - "bootstrap_memory_lock=true"
   ulimits:
    memlock:
     soft: -1
     hard: -1
   container_name: comp-dnif-ds
```

Edit the configuration of **docker-compose.yml** as shown,

| Field Name | Change to |
| :--- | :--- |
| image: "dnif/ship-ds:latest" | image: "dnif/ship-ds:&lt;To install a _specific version_ of DNIF or you can use `latest` for latest version&gt;" |
| /path/to/volume:/dnif | /dnif:/dnif |
| DKEY=KEY | DKEY=’Enter the deployment Datastore key which you received from DNIF’ |
| IP Address | CRIP-Enter the IP address of Correlator |

#### Increase the vm.max\_map\_count parameter

* If you are using VM, the default operating system limits on mmap counts is likely to be too low, which may result in out of memory exceptions.you may need to increase the `vm.max_map_count`parameter.
* you can increase the limits by running the following command as `root`

```text
$ sysctl -w vm.max_map_count=262144
```

* To set this value permanently, update the `vm.max_map_count` setting in `/etc/sysctl.conf`

```text
$ vim /etc/sysctl.conf
vm.max_map_count=262144
```

* Reload the config as root,

```text
$ sysctl -p
```

* Check the new value using following command,

```text
$ cat /proc/sys/vm/max_map_count
```

* After increasing the `vm.max_map_count` parameter, move into the folder where you have created `docker-compose.yml` file and run `docker-compose` to download and start the image.

```text
$ cd /dnif
$ sudo docker-compose up -d
```

* The time taken by the above command to fetch the DNIF image from the online repository will depend on the bandwidth of your network connection.

  Once the installation is complete, you will see an installation complete message within the terminal or an ASCII art of DNIF.

  The above command successfully performs a “pull” operation , which gets the latest code from the online repository and runs the same.

* Finally, ensure our container has started up, the following command should give you a list of all the running containers and you should see your container in the `running` state.

```text
$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS               NAMES
b682fdbk058f        dnif/ship-ds:latest   "/bin/bash /usr/sr..."   5 minutes ago       Up 1 minute                             comp-dnif-ds
```

* Click to install and apply [license](https://docs.dnif.it/admin-guide/configuration/installing-licenses) on Datastore.
* Click to configure [Cluster](https://docs.dnif.it/admin-guide/configuration/setting-up-the-cluster), [Cluster with Query Node](https://docs.dnif.it/admin-guide/configuration/setting-up-a-query-node) and [Cluster with replication\(HA\)](https://docs.dnif.it/admin-guide/configuration/setting-up-cluster-with-replication-ha) on Datastore.

