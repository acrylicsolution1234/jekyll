---
title: Correlator Deployment
permalink: /docs/Installing-DNIF/Distributed-Deployment/Installing-Correlator/
section: Installing-DNIF
sub-section: Distributed-Deployment
---
# Correlator Deployment

All DNIF Components come in the form of [docker](https://www.docker.com/) containers \(more on [why docker](https://www.docker.com/why-docker)\), you can lookup [docker instructions ](https://docs.dnif.it/admin-guide/quick-start-guide/installing-dnif/installing-docker)for installing the software on Ubuntu.

After successfully installing docker, checks and allow the [network pre-requisites](https://docs.dnif.it/admin-guide/preparing-to-install/network-pre-requisites) before diving into Correlator installation.

## Installing Correlator <a id="installing-datastore"></a>

To know more about [Correlator](https://docs.dnif.it/admin-guide/components/correlator-cr) and how it works.

* Let's begin by setting up a home folder, this is where all the configurations are going to be stored. This folder will hold everything and will be mapped with the docker Correlator container.

```text
$ sudo mkdir /dnif
$ cd /dnif
```

* Just trying to make things easy, we built out a `docker-compose` file that can help you configure the image, but first we will install `docker-compose` and for doing that that we install `python-pip` and then straighten our `locales` settings.

```text
$ sudo apt-get install -y python-pip
$ export LC_ALL="en_US.UTF-8"
$ export LC_CTYPE="en_US.UTF-8"
$ sudo dpkg-reconfigure locales
```

* Install `docker-compose`

```text
$ sudo pip install docker-compose
```

* Create a Datastore container image configuration or the docker-compose file using `vim` editor.

```text
$ cd /dnif
$ sudo vim docker-compose.yml
```

* Contents of `docker-compose.yml` should be,

```text
version: '2.0'
services:
 comp-ship-cr:
   image: "dnif/ship-cr-km:latest"
   network_mode: "host"
   cap_add:
    - NET_ADMIN
   ports:
    - "127.0.0.1:826:826/tcp"
    - "8362:8362/tcp"
   volumes:
    - /path/to/volume:/dnif
   environment:
    - "DKEY=DEPLOY_KEY"
    - "DS=10.192.5.49"
   container_name: comp-dnif-cr
```

Edit the configuration of **docker-compose.yml** as shown,

| Field Name | Change to |
| :--- | :--- |
| image: "dnif/ship-cr:latest" | image: "dnif/ship-cr:&lt;To install a _specific version_ of DNIF or you can use `latest` for latest version&gt;" |
| /path/to/volume:/dnif | /dnif:/dnif |
| DKEY=KEY | DKEY=’Enter the deployment Correlator key which you received from DNIF’ |

#### Increase the vm.max\_map\_count parameter

* If you are using VM, the default operating system limits on mmap counts is likely to be too low, which may result in out of memory exceptions.you may need to increase the `vm.max_map_count`parameter.
* you can increase the limits by running the following command as `root`

```text
$ sysctl -w vm.max_map_count=262144
```

* To set this value permanently, update the `vm.max_map_count` setting in `/etc/sysctl.conf`

```text
$ vim /etc/sysctl.conf
vm.max_map_count=262144
```

* Reload the config as root,

```text
$ sysctl -p
```

* Check the new value using following command,

```text
$ cat /proc/sys/vm/max_map_count
```

* After increasing the `vm.max_map_count` parameter, move into the folder where you have created `docker-compose.yml` file and run `docker-compose` to download and start the image.

```text
$ cd /dnif
$ sudo docker-compose up -d
```

* The time taken by the above command to fetch the DNIF image from the online repository will depend on the bandwidth of your network connection.

  Once the installation is complete, you will see an installation complete message within the terminal or an ASCII art of DNIF.

  The above command successfully performs a “pull” operation , which gets the latest code from the online repository and runs the same.

* Finally, ensure our container has started up, the following command should give you a list of all the running containers and you should see your container in the `running` state.

```text
$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED             STATUS              PORTS               NAMES
m896fhgk089f        dnif/ship-cr:latest   "/bin/bash /usr/sr..."   5 minutes ago       Up 1 minute                             comp-dnif-cr
```



