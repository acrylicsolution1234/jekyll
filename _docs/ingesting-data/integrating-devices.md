---
title: Integrating Devices 
permalink: /docs/Ingesting-Data/Integrating-Devices/
section: Ingesting-Data
---
# Integrating Devices

## How to check if devices are being parsed correctly

DNIF has the capability of automatically detecting whether an event was parsed correctly and if not, identifying the exact reason of failure. 

This reason for failure \(or success\) is stored in a field called `$PStatus`.

Following are the different values for the `$PStatus` field :

* PAD - Parsed As Desired
* NLF - No Log Found
* NEF - No Event Found
* PER - Parser Error
* DNF - Device Not Found

### PAD \(PARSED AS DESIRED\)

PAD is set on the event when the parser worked well and all fields were extracted correctly from the event log.

![](../.gitbook/assets/pad.png)

### NLF \(NO LOG FOUND\)

NLF is set on the event when the device was not assigned to a specific log type. When a device is added to the DNIF platform we are required to define the parsers that will be applied to the device. Each device could be assigned one or more parsers and if the primary format of these parsers do not match the log event, then that event will be marked as NLF.

![](../.gitbook/assets/nlf.png)

As an example, you could have Linux system that has an installed Apache2 webserver and a MySQL database. You will need to add both Apache2 and MySQL to the device for DNIF to parse events correctly. You could go back and edit the device and add the missing log type to the device in order to rectify the issue.

### NEF \(NO EVENT FOUND\)

NEF is set on the event when DNIF could identify the log type but could not find the specific event that was received. Continuing with the previous example, MySQL can send multiple types of logs - some events are related to authentication, while some are related to creation or deletion of tables. These events are different and have different event formats. DNIF has a parser written for each event log and if the parser is not able to find the specific event then the event will be marked as NEF.

![](../.gitbook/assets/nef.png)

You will need to add this specific entry to the parser or report the issue to the partner who built the parser. In most cases, this will be the integration team at DNIF. In that case, you could submit a new case to get this worked on.

### PER \(PARSER ERROR\)

PER is set on events that trigger a parser error, meaning that the match is not complete. You will have to rework the event parser or report the issue to the partner who built the parser. In most cases, the integration team at DNIF will be responsible for setting this up. You could submit a new case to get this fixed.

![](../.gitbook/assets/per.png)

### DNF \(DEVICE NOT FOUND\)

DNF is set on events that are received from hosts that have not been been integrated yet, but are already sending data to the DNIF platform. In this case, all the events sent by the host will be received and indexed but these events will not be parsed. You could click on DNF in the `$PStatus` field and the context menu will give you an option to add the device to DNIF.

![](../.gitbook/assets/dnf.png)





