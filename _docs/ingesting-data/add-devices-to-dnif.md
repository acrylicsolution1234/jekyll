---
title: Add Devices to DNIF 
permalink: /docs/Ingesting-Data/Add-Devices-to-DNIF/
section: Ingesting-Data
---
# Add Devices to DNIF

You can add the devices to the DNIF console by using ****different methods. This document consists three different ways of adding devices to the DNIF console.

### **Add device using DEVICES option**

Click on the **MANAGEMENT** tab at the top right corner and select the option **DEVICES**.

![](https://lh5.googleusercontent.com/JtzUlvTj03ZI1AqJ49rILHh1GHYndz9KF7ciohYk_1RSaUyWMnl-OHjMayFPBxw0HxJo2ZCTToGs7tpIMRnuaRFpbKd3EvMYQAYKINsM4HCOZN15nN16LnVEPOtndhRfrxSV0yyZ)

Here you will see the list of devices which are already added to the DNIF console. To add a new device, click on the **+** button at the top right corner.

![](https://lh5.googleusercontent.com/KOpQ-h3uJp2yw6rdVaXXn8Z7eRx7Xnaz2hkC8MpBmr9pU1hiTqbOn4OZMv2u1_5ZjyYXgcj06ooejwMkLPkwJLDscjFEAGRYE6u0UJnwkwSYgpEITgPUlFT-N_7BJacPINkeI6Yv)

You will be prompted with an device template which will contain several fields as shown in below,

![](https://lh4.googleusercontent.com/kzSTihwTjbOuumrvFJfivSkqERpGNoqaAXcQc-7RM8TWnNlnrN5B_wFoMkff5Sv4tkVD8Py-jhPDnwYLLdRfezTvjd0Fpor9QR1bm9JGI7eyh7SEdp9MkB3RCkT8ZUZk0NTSW7TP)

Fields from ADD NEW DEVICE page are explained below,

| Field Name | Description |
| :--- | :--- |
| Name | Name for the New Device you want to add |
| Timezone | Which timezone the device follows |
| Owner | email id of the owner/department who owns the device |
| Tags | Keywords to help with search/filter |
| Device IP Address | IP address for the device |
| Priority | Select priority from 1-5, 1 being lowest & 5 being the highest |
| Additional Information | Information in JSON format |
| Log types | Add the parser to the device with respect to the device type |

> **Note: You will need to sync respective package from the DNIF cloud to your local repository before adding the parser to the device.**

Fill all the necessary details and save it by clicking on Save button, your device will get added to the DNIF console.

### **Add device using SEARCH option**

If you have forwarded logs on DNIF console before adding device to the console**,** you can add the device using search option to the console.

Check the logs on the DNIF console which parsing status is DNF by executing the below query.

```text
fetch * from event where $PStatus=DNF limit 100
```

![](https://lh3.googleusercontent.com/WQJN4NFjitw7hku21y3YGXBSD6ih_Ucxw0OPXY3B849-Dg2FAvIKZBI5KZieXdtQOsv2VUhhzmXA-shyueGRWeUfwZH62kxwvZW3AxdIfXU2d13xW-UMEntC76spT4iAd4g4pYda)

Here you can see the parsing status DNF as above, click on the DNF and select an option ADD NEW DEVICE as below,

![](https://lh4.googleusercontent.com/SULC9agytSdE8e8kEUfq6odabboQCSIjPJf6y0SCenAXxzjCRjHqvIclJZock_qWx19nCEBROH701sVPR1tSZOsB0WR3qhUx7ZnALubnV54V0f5FVGsL4b_hWMB2YH4BNC7cUJ0J)

You will be prompted with an device template which will contain several fields as shown in below and it will automatically takes the IP address of the device in `Device IP Address` field.

![](https://lh3.googleusercontent.com/zZRxE7wzSCsYe6gtyelUXyeehI29vggs5MT9BSwwpYu4oR88sY9NeG2VssxdXmU7IbB_xujqiMIqUTyThrtlQqg-UL0Yw4CTz6e4igfBtRweLZwrggFYbMsZ6YsLlm0fLS-J6LiW)

Fields from ADD NEW DEVICE page are explained below,

| Field Name | Description |
| :--- | :--- |
| Name | Name for the New Device you want to add |
| Timezone | Which timezone the device follows |
| Owner | email id of the owner/department who owns the device |
| Tags | Keywords to help with search/filter |
| Device IP Address | IP address for the device |
| Priority | Select priority from 1-5, 1 being lowest & 5 being the highest |
| Additional Information | Information in JSON format |
| Log types | Add the parser to the device with respect to the device type |

> **Note: You will need to sync respective package from the DNIF cloud to your local repository before adding the parser to the device.**

Fill all the necessary details and save it by clicking on Save button, your device will get added to the DNIF console.

### **Add multiple devices using upload a file**

Click on the **MANAGEMENT** tab at the top right corner and select the option **DEVICES**.

![](https://lh5.googleusercontent.com/JtzUlvTj03ZI1AqJ49rILHh1GHYndz9KF7ciohYk_1RSaUyWMnl-OHjMayFPBxw0HxJo2ZCTToGs7tpIMRnuaRFpbKd3EvMYQAYKINsM4HCOZN15nN16LnVEPOtndhRfrxSV0yyZ)

Here you will see the list of devices which are already added to the DNIF console. To add a multiple devices at a time, click on the **UPLOAD** button at the top right corner.

![](https://lh3.googleusercontent.com/aaDorPBsYlwJNiQ3qQgB1NdK-3PWxU1pDUywg5BqDPH_Q3_t6T6fYGceRWASwyQMFKDRcsstSWLKFEiyoIpYtc0yCq4xu2yu0RrvvRZbIfX_wftQEPdlkvMA-AEXIyZdNm4Nhk_Y)

You will be prompted option as **Upload excel** on the console.

![Device-upload](https://lh5.googleusercontent.com/sZnI5WNkCkhcYZAqat-fSqcmpi17LRI2bsdTKnPMsHZLdOdWGLx3z4byf_h8emROxMToKWR5Y4NZ8brLwyivD53DNlFvAYHaso999EgXlBvCHAa_hFS3H7xAi0xVwtAWj1F-cA3u)

Before uploading a exel file, make sure that the format of a file fields must be same as given below,

![](../.gitbook/assets/image%20%2818%29.png)

Fields from above add device template are explained below,

| Field Name | Description |
| :--- | :--- |
| DeviceName | Name for the New Device you want to add |
| ComplianceTags | Tagging for compliance   |
| AddressScheme | IP Address scheme of the device |
| DeviceAddress | IP address for the device |
| TimeZone | Which timezone the device follows |
| PrimaryOwner | email id of the owner/department who owns the device |
| Tags | Keywords to help with search/filter |
| AgentLess | If you are integrating Windows Devices with WMI\(AgentLess\), you required the Username and Password details. If you are integrating log sources using third party agents, you do not require the Username and Password details. |
| Username | Username of the device |
| Password | Password of the device |
| LogType | Log types of the log source |
| LogName | Log name of the log source |
| Weightage | Select Weightage from 1-5, 1 being lowest & 5 being the highest |

After choosing the file, click on the Upload button as shown in the below,

![](https://lh5.googleusercontent.com/ApWlRsAanvXzDE0nvqYpcRjiajuLTiMUQQXEyDAW4kB-bNsxycG20dc4cu9e1so-RKniFYt7tLCI_SuiBUWUBbobe8sRASTNB06MJ4YJ2PcgR6gqIVEmlZYlZWllKvG8u33hXYzx)

After successful file upload, you will receive the message as below,

![](https://lh4.googleusercontent.com/Vq5DJNdgIrn8hC5X5r_2Is-GFgKvSJaXWzEe4HPncR0IYzRluKZS2jjxTk1y6Rnftg0z_tSm3eMAxiwuJqZzy5L3IkNbtDqX2RNa99YsUW-8mPngTIAr0tBfFkQcytbp2HpYbG-4)

Check for the devices are added or not to the existing list, go to the DEVICES and check for the devices which you have uploaded.

  


