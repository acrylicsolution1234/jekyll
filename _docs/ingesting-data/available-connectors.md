---
title: Available Connectors 
permalink: /docs/Ingesting-Data/Available-Connectors/
description: List of publicly available devices and their versions that are integrated.
section: Ingesting-Data
---

# Available Connectors

| Log Source | Log Type | Log Name | Version/Model Supported |
| :--- | :--- | :--- | :--- |
| Apache Web Server | WEBSERVER | APACHE2 | All |
| Apache Tomcat | WEBSERVER | TOMCAT | All |
| Amazon VPC | FLOW | AWS-VPC | All |
| Amazon Cloudtrail | CLOUD | AWS-CLOUDTRAIL | All |
| Amazon S3 Bucket | CLOUD | AWS-S3 | All |
| Amazon EC2 Cloudwatch | CLOUD | AWS-NIX | All |
| ADAudit Plus | OS | WINDOWS-AD-AUDIT-PLUS | All |
| Arbor DDOS | DDOS | ARBOR | All |
| Array Loadbalancer | LOADBALANCER | ARRAY-OS | 8.5 |
| Arista Switch | SWITCH | ARISTA | All |
| Accops Hysecure VPN | VPN | ACCOPS | All |
| Azure NSG | CLOUD | AZURE-NSG | All |
| Azure Activity Log | CLOUD | AZURE-ACTIVITY | All |
| Bluecoat | WEBFILTER | BLUECOAT | All |
| Brocade Compact Ethernet Switch | SWITCH | BROCADE-CES | All |
| Brocade Switch | SWITCH | BROCADE | All |
| Brocade Vyatta Router | ROUTER | BROCADE-VYATTA | All |
| Cyberoam | FIREWALL | CYBEROAM | All |
| Cisco IOS Switch | SWITCH | CISCO-IOS | All |
| Cisco IOS Router | ROUTER | CISCO-IOS | All |
| Cisco Ironport | EWSA | CISCO-IRONPORT | All |
| Cisco ASA | FIREWALL | CISCO-ASA | 5500-X Series |
| Cisco Firepower | IPS | CISCO-FIREPOWER | All |
| Cisco FCOE | SWITCH | FCOE-SWITCH | All |
| Cisco ACE | ACE | CISCO-ACE | 4710 Series |
| Cisco ISE | ISE | CISCO-ISE | 3595 Series |
| Cisco TAC ACS | ACS | TACACS | 5.7 |
| Cisco Wireless Controllers | WLC | CISCO-AIR-CT5508-K9 | 8.3 |
| CheckPoint FW1 R77.30 / LEA | FIREWALL | CHECKPOINT-V80.10 | R70.33 |
| CheckPoint FW1 R80.10 / LEA | FIREWALL | CHECKPOINT | R80.10 |
| Carbon Black Endpoint Detection and Response | EDR | CARBON-BLACK | 6.1 |
| Cowrie Honeypot Decoy | DECOY | COWRIE-HONEYPOT | All |
| Citrix Netscaler | WAF | NETSCALAR-WAF | All |
| Citrix Netscaler Application Firewall | WAF | NETSCALAR-OS | All |
| Database Activity Monitoring | DAM | MCAFEE-DAM | 5.x |
| Darktrace | IPS | DARKTRACE | DCIP-X2 |
| Dell EMC | STORAGE | EMC-LSILON | All |
| Dell Switch | SWITCH | DELL-SWITCH | 9.11 |
| Dell Cylance Endpoint | ENDPOINT | DELL-CYLANCE | 3.8 |
| DMZ Gateway | GATEWAY | DMZ | All |
| EfficientIP DDI | DDI | EFFICIENT-IP | All |
| Fortinet Fortigate | FIREWALL | FORTIGATE | 5.2.x, 5.4.x, 5.6.x |
| Fortinet Fortiweb  | WAF | FORTIWEB | All |
| F5 Big-IP  | WAF | F5BIGIP | All |
| F5 Loadbalancer | LOADBALANCER | F5LB | All |
| FireEye APT  | APT | FIREEYE | All |
| FileZilla - FTP Solution | FTP | FILEZILLA | All |
| Forcepoint Websense | WEBFILTER | WEBSENSE | All |
| Forcepoint DLP | WEBFILTER | WEBSENSE | All |
| HP-UX | OS | NIX | All |
| HP Switch | SWITCH | HP-SWITCH | 5.2 |
| HPE OfficeConnect Switch  | SWITCH | HP-OFFICE-CONNECT | PD.01.05 |
| IBM AIX | OS | NIX | All |
| IBM AIX Audit | OS | AUDITD | All |
| IBM WebSphere AS | WEBSERVER | WEBSPHERE | All |
| IBM Security Siteprotector System | IDS | SITEPROTECTOR | All |
| IBM DataPower Gateway | GATEWAY | IBM-DATAPOWER | All |
| IBM ISS | NIPS | IBM-ISS | GX5108-4.6.2 |
| IBM DB2 | DATABASE | DB2 | All |
| IBM HTTP Server | WEBSERVER | APACHE2 | All |
| ISC DNS | DNS | NAMED | All |
| Imperva SecureSphere Web Gateway | WAF | IMPERVA | All |
| Infoblox DDI | DDI | INFOBLOX | All |
| Juniper SRX | FIREWALL | JUNIPER-SRX | All |
| Juniper EX Series | SWITCH | JUNIPER | All |
| Juniper Netscreen SSG  | SSG | JUNIPER-NETSCREEN | All |
| Microsoft Windows Event Logs | OS | WINDOWS-NXLOG-AUDIT, WINDOWS-NXLOG-SERVICE, WINDOWS-NXLOG-APPLICATION | All |
| Microsoft Windows Event Logs - WMI | OS | WINDOWS-WMI | All |
| Microsoft Windows Active Directory | OS | WINDOWS-AD | All |
| Microsoft Windows SQL | DATABASE | MSSQL | All |
| Microsoft Windows SQL using Nxlog | DATABASE | MSSQL\_WINEVT | All |
| Microsoft Windows SQL using ODBC | DATABASE | MSSQL | All |
| Microsoft Windows IIS | WEBSERVER | IIS | All |
| Microsoft Windows Sysmon | OS | WINDOWS-SYSMON | All |
| Microsoft Windows DNS | DNS | WIN-DNS | All |
| Microsoft Windows DHCP | DHCP | WIN-DHCP | All |
| Microsoft Windows Exchange Server | MAILSERVER | MS-EXCHANGE | All |
| Microsoft Windows Sharepoint CMS | CMS | SHAREPOINT | All |
| McAfee ePolicy Orchestrator | ANTIVIRUS | MCAFEE-EPO | All |
| McAfee Web Gateway | WEBFILTER | MCAFEE-WG | All |
| McAfee NIPS | NIPS | MCAFEE | 8.3 |
| McAfee ATD | ATD | MCAFEE-ATD | 3.8 |
| MySQL | DATABASE | MYSQL | All |
| Mimecast Email and Web Security | EWSA | MIMECAST | All |
| NGINX | WEBSERVER | NGINX | All |
| Network Management System | NMS | NMS-CPE | All |
| OPUS | WEBSERVER | OPUS | All |
| Oracle Solaries | OS | NIX | All |
| Oracle Audit Logs using Syslog | DATABASE | ORACLE | All |
| Oracle Audit Logs using Nxlog | DATABASE | ORACLE\_WINEVT | All |
| Oracle Audit Logs using JDBC | DATABASE | ORACLE | All |
| Oracle Weblogic | WEBSERVER | WEBLOGIC | All |
| Pfsense Firewall | FIREWALL | PFSENSE | All |
| PostgreSQL | DATABASE | PGSQL | All |
| Postfix | MTA | POSTFIX | All |
| Palo Alto Networks | FIREWALL | PALOALTO | All |
| Redhat/CentOS | OS | NIX | All |
| Redhat/CentOS Audit | OS | AUDITD | All |
| Radware WAF | WAF | RADWARE | All |
| Radware DDOS | DDOS | RADWARE | All |
| Radware Loadbalancer | LOADBALANCER | RADWARE | All |
| Symantec Endpoint Protection | ANTIVIRUS | SYMANTEC-ENDPOINT | 12.0.0 |
| Symantec ATP | ENDPOINT | SYMANTEC\_ATP | 12.0.0 |
| SafeSquid | WEBFILTER | SAFESQID | All |
| SmokeScreen | DECOY | SMOKESCREEN | All |
| SonicWall  | UTM | SONICWALL | All |
| Snort | IDS | SNORT | All |
| SNMP Traps | SNMP | SNMP | All |
| SDMS | WEBSERVER | SDMS | All |
| Trend Micro Control Manager | ANTIVIRUS | TREND-MICRO | All |
| Trend Micro Deep Security Manager | ANTIVIRUS | TREND-MICRO | All |
| Trend Micro Deep Discovery Inspector | ANTIVIRUS | TREND-MICRO | All |
| Trend Micro Deep Security Agent | ANTIVIRUS | TREND-MICRO | All |
| TippingPoint | IPS | TIPPINGPOINT | All |
| Ubuntu | OS | NIX | All |
| VMware ESXi | OS | VMWARE | All |
| VMware VCenter | OS | VMWARE | All |
| WildFly JBoss AS | WEBSERVER | JBOSS | All |
| Zimbra Mail Server | MTA | ZIMBRA | All |
| zScaler DNS | DNS | ZSCALER | All |
| zScaler Firewall | FIREWALL | ZSCALER | All |

