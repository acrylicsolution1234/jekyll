---
title: Loading Parsers 
permalink: /docs/Ingesting-Data/Loading-Parsers/
description: Add the Parser to the Devices
section: Ingesting-Data
---

# Loading Parsers

### Sync new parser on the newly integrated devices.

Whenever you integrate a new device with DNIF, sync the parser of that device from the DNIF cloud to your local repository. 

Following steps will guide you for syncing the parser from the DNIF cloud to the local repository.

Login into DNIF console and switch to the DNIF cloud repository by clicking on the toggle button.

![](../.gitbook/assets/image%20%2813%29.png)

Search for the package in which the relevant parser is present \(For e.g. Linux\) with the owner named as DNIF HQ and sync the package from DNIF cloud repository to your local repository by clicking the sync button.

![](../.gitbook/assets/image%20%2845%29.png)

Select your scope name any click on the Sync button.

![](../.gitbook/assets/image%20%2817%29.png)

Open the package which is synced into your local repository and check for the parser is get synced or not. To check the parser, click on the entity option and you will see the drop-down menu.

![](../.gitbook/assets/image%20%2822%29.png)

Select the option Parsers from the list.

![](../.gitbook/assets/image%20%2850%29.png)

You will see the parser in the package of your local repository.

![](../.gitbook/assets/image%20%2847%29.png)

### Sync the updated parser into the local repository.

Whenever changes happen in the parser as per client requirement,  we need to replace existing parser with the updated parser. 

Below are the steps for syncing the updated parser from DNIF cloud to the local repository.

Provide the admin rights to the package owner into your local repository package by clicking on the +Add button in front of the Admin section on edit package page.

![](../.gitbook/assets/image%20%2814%29.png)

For example, if you synced the package which owner is DNIF HQ, then you need to provide admin rights to the user support@dnif.it. 

Remove the existing parser from the local repository package to replace updated one.

![](../.gitbook/assets/image.png)

Switch to the DNIF cloud repository using toggle button which is present on the left hand side.

![](../.gitbook/assets/image%20%284%29.png)

Search for the package in which the relevant parser is present \(For e.g. Linux\) with the owner named as DNIF HQ and sync the package from DNIF cloud repository to your local repository by clicking the sync button.

![](../.gitbook/assets/image%20%2845%29.png)

While syncing the package you will be prompted with the two options as below,

Add missing entities and Overwrite the existing package entities.

Select the option Add missing entities and click on the Sync button.

![](../.gitbook/assets/image%20%2837%29.png)

Check whether the parser is get synced to the local repository, click on the entity option and you will see the drop-down menu.

Select the option Parsers from the list.

![](../.gitbook/assets/image%20%2850%29.png)

You will see the parser in the package of your local repository.

![](../.gitbook/assets/image%20%2847%29.png)

> **Note: It will take 15-20 minutes for the parser updates on the device, after 15-20 minutes the logs will get parsed with updated parser.**

### Add parser to the devices which are already added to the DNIF.

Select the device from the device list and click on edit button of the device.

![](../.gitbook/assets/image%20%2868%29.png)

Click on the **+** Add button which is in the front of the LogType section, select the parser according to the device log type.

![](../.gitbook/assets/image%20%287%29.png)

Save the device by clicking on the Save button after the parser get added to it.

![](../.gitbook/assets/image%20%2830%29.png)

It will take 15-20 minutes for the parser updates on the device, after 15-20 minutes the logs will get parsed with added parser.

> **Note: Parser should be present into the local repository before adding it to the devices.**

