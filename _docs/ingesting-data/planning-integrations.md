---
title: Planning Integrations 
permalink: /docs/Ingesting-Data/Planning-Integrations/
section: Ingesting-Data
---
# Planning Integrations

Types of Ingestors we have,

## List of DNIF Ingestors

Ingestor is work as a receiver, its receives logs from various log sources, this listerners runs on the Adapter \(AD\) and is also available on the All-in-One \(A10\) instances.

1. syslog\_listerner\_udp
2. syslog\_listerner\_tcp
3. cef\_listerner\_udp
4. bluecoat\_listerner
5. httplisterner
6. httpslisterner
7. netflowv5
8. snmp\_app

### syslog\_listerner\_udp

`syslog_listerner_udp` is a service that receive data-gram packets from the various log sources on the `syslog` port - `udp/514`. [Syslog](https://en.wikipedia.org/wiki/Syslog) is a common standard for logging events across devices and operating systems. Its can transmit a huge amount of data within the network with minimal packet loss.

**Check if `syslog_listerner_udp` is running**

* Login to your **Adapter** or **A10** container, connect to your [docker container.](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh)
* Check for the `syslog_listerner_udp` service status using following command,

```text
$ supervisorctl 

adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

### syslog\_listerner\_tcp

`syslog_listerner_tcp` is a service provides the ability to receive connection-oriented packets from various log sources on the `syslog` port 514 - `tcp/514`.  Encryption is natively provided.

Transmission of syslog data over TCP is reliable ordered data transmission between networks and provides re-transmission in the event of network failure.

**Check if `syslog_listerner_tcp` is running**

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the `syslog_listerner_tcp` service status using following command,

```text
$ supervisorctl 

adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

### cef\_listerner\_udp

`cef_listerner_udp` is a service provides the ability to receive `cef` format logs  from various log sources on the `port` - `udp/5514`.

**Check if `cef_listerner_udp` is running**

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the `cef_listerner_udp` service status using following command,

```text
$ supervisorctl 

adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

### bluecoat\_listerner

`bluecoat_listerner`is a service provides the ability to receive logs from blue-coat proxy server on the `port` - `2325/tcp`.

**Check if `bluecoat_listerner` is running**

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the `bluecoat_listerner` service status using following command,

```text
$ supervisorctl 

adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
bluecoat_listerner               RUNNING   pid 905, uptime 2 days, 22:19:40
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

### httplisterner and httpslisterner

`httplisterner` and `httpslisterner`is a services provides the ability to receive logs in a JSON format on the port - `9234/TCP(http)` and `9236/TCP(https)`. This service is also used to receive data from the DNIF Detector component.

**Check if `httplisterner` and `httpslisterner` is running**

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the `httplisterner` and `httpslisterner`services status using following command,

```text
$ supervisorctl 

adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
bluecoat_listerner               RUNNING   pid 905, uptime 2 days, 22:19:40
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

### netflowv5

`netflowv5` is a service provides the ability to receive logs from the CISCO networking devices on the `port` - `udp/2055`

**Check if `netflowv5` is running**

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the `netflowv5`service status using following command,

```text
$ supervisorctl 

adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
bluecoat_listerner               RUNNING   pid 905, uptime 2 days, 22:19:40
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

### snmp\_app

`snmp_app`is a service provides the ability to receive SNMP traps from the Swift application server on the `port` - `udp/161,162`.

**Check if `snmp_app` is running**

* Login to your **Adapter** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the`snmp_app`service status using following command,

```text
$ supervisorctl 

adinline                         RUNNING   pid 316, uptime 2 days, 22:24:30
cef_listerner_udp                RUNNING   pid 303, uptime 2 days, 22:24:30
config_reporter                  RUNNING   pid 317, uptime 2 days, 22:24:30
events_parser_processor:01       RUNNING   pid 310, uptime 2 days, 22:24:30
events_parser_processor:02       RUNNING   pid 309, uptime 2 days, 22:24:30
bluecoat_listerner               RUNNING   pid 905, uptime 2 days, 22:19:40
httplistener                     RUNNING   pid 901, uptime 2 days, 22:19:40
httpslistener                    RUNNING   pid 906, uptime 2 days, 22:19:39
netflowv5                        RUNNING   pid 894, uptime 2 days, 22:19:42
snmp_app                         RUNNING   pid 896, uptime 2 days, 22:19:43
sshd                             RUNNING   pid 305, uptime 2 days, 22:24:30
syncDump                         STOPPED   Feb 01 09:43 AM
syslog_listerner_tcp             RUNNING   pid 311, uptime 2 days, 22:24:30
syslog_listerner_udp             RUNNING   pid 302, uptime 2 days, 22:24:30
```

> **Note: If any above listerner is not available in the Adapter services, then contact DNIF Support to deploy additional listerner on Adapter.**

