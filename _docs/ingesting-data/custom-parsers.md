---
title: Custom Parsers 
permalink: /docs/Ingesting-Data/Custom-Parsers/
section: Ingesting-Data
---
# Custom Parsers

DNIF has formulated parsers for more than 220+ standard industry devices. Apart from readily available library of connectors and parsers, customers can get custom ones built within a 7-day SLA\(additional costs might be incurred\) or you can [create one on your own from the DNIF web console](https://docs.dnif.it/operators-guide/create-a-parser-using-console).

If you would like to request parser creation from DNIF team, make sure you share the required details as follows:

| Device Details | Description |
| :--- | :--- |
| Device Name | Name of the device. |
| Device Type | Type of device, e.g., Firewall, Web Server, etc. |
| Device Version | Specific version of the device. |
| OS Version | Specific version of the inherent OS of the device. |
| Log Format String | **Example**:  date=2019-04-22 time=11:44:13 devname=_devicename_ devid=_deviceid_ logid=_logid_ type=_type_ subtype=_subtype_ level=_loglevel_ vd=root logdesc="_description_" action=_Actiontaken_ status=_status_ reason="_reason_" msg="_message_" |
| Log Type | Type of logs, e.g, traffic, system, access, audit, etc. |
| Reference Document | Reference document of the device logs . |
| Unique Log Samples | Unique logs sample of integrated device. |



