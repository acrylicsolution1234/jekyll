---
title: Managing Indexes 
permalink: /docs/Managing-Your-Deployment/Managing-Indexes/
section: Managing-Your-Deployment
---

# Managing Indexes

## **Location of indexes**

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* The location of data indexes available under **/dnif/{Deploy-Key}/data/{Cluster-Name}/nodes/0/indices** folder on Datastore and A10 container.

```text
$ cd /dnif/{Deploy-Key}/data/{Cluster-Name}/nodes/0/indices
$ ls
322aYbUoT6eRwotWOyvE4A O8o7QS2AREKELr8y01QjCw VG5t6knuTN2akCZUQ3sZfQ lFVh0DjWQdK6VO-b78PciQ vcyqkLl5TIW2aT-_PDbEqQ ycDmKEs1R-OOjxpy1X5e0A 
7S3YMW6DQ3WsULJXwtq-ew QkEW6wuVTEeQD6iwpzyEEg VRL7HRM1TgCpw44WVjsXRA lHNL8mVfTTWDEFgypiIrpA w6knHcX9TbaStvuSKGA-hQ z1_ihBtZTRKHwVNSnSD2VA
```

## **Checking indexes**

You can check open and close indexes using below steps,

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the **open** indexes using following commands,

```text
$ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 1 1 111 111 0 0 0 \n'
>>> m = es.cat.indices()
>>> for i in es.cat.indices().split('\n'):
...     if 'open' in i:
...           print i
... 
green open  dsdb-20190124        1 0   74046      0  29.6mb  29.6mb 
green open  ctdp_conf            1 0   18780    704 107.6mb 107.6mb 
green open  dsdb-20190123        1 0   70188      0  28.7mb  28.7mb 
green open  dsdb-20190122        1 0   99307      0  46.7mb  46.7mb 
green open  dsdb-20190125        1 0   95539      0  43.5mb  43.5mb 
```

* Check for the **close** indexes using following commands,

```text
$ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.cat.health() 
u'1549265131 07:25:31 NETMON green 1 1 111 111 0 0 0 \n'
>>> m = es.cat.indices()
>>> for i in es.cat.indices().split('\n'):
...     if 'close' in i:
...           print i
...
      close dsdb-20181216                                           
      close dsdb-20181218                                           
      close dsdb-20181220                                           
      close dsdb-20181217                                           
      close dsdb-20181219                                              
```

## **Monitoring health of indexes**

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Follow the below steps __to check the database health status,

  ```text
  $ python 
  >>> from elasticsearch import Elasticsearch 
  >>> es = Elasticsearch([{'port':8240}]) 
  >>> es.cat.health()
  u'1549265131 07:25:31 NETMON green 1 1 111 111 0 0 0 \n'
  ```

  The above commands will show the health status of database whether the status is Green, Yellow or Red.

  The cluster health API allows getting a very simple status on the health of the cluster.  

  * **Green state**: A Green means that all shards are allocated. 
  * **Yellow state**: A Yellow means that the primary shard is allocated but replicas are not. 
  * **Red state**: A red status indicates that the specific shard is not allocated in the cluster.

## **Opening/ Closing indexes**

### For open to close indexes

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the **close** indexes using above commands and open them as per your requirement using following commands,

```text
$ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.indices.open("index_name")
{u'acknowledged': True}
```

### For close to open indexes

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Check for the **open** indexes using above commands and close them as per your requirement using following commands,

```text
$ python
>> from elasticsearch import Elasticsearch
>> es = Elasticsearch([{'port':8240}])
>> es.indices.close("index_name")
{u'acknowledged': True}
```

## **Auto index creation** 

Auto database index creating on daily basis at 12:00 AM\(UTC\).

* Login into Datastore or A10 container and check for the today’s index is created or not in the database using following commands:

```text
$ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 1 1 111 111 0 0 0 \n'
>>> m = es.cat.indices()
>>> for i in es.cat.indices().split('\n'):
...             print i
... 
green open ustats        O8o7QS2AREKELr8y01QjCw 1 0      80 0 36.1kb 36.1kb
green open sysdata       lHNL8mVfTTWDEFgypiIrpA 1 0   16781 0  2.5mb  2.5mb
green open dsdb-20190214 jTxMqVXhSue9bqbiiPAFGw 1 0       0 0   262b   262b
green open ctdp_conf     n3zmyiesTRGvZxeNHf9_RA 1 0       1 0  4.1kb  4.1kb
green open logging       yJfkcyw3T9SXDYz7BYqFZw 1 0       0 0   262b   262b
green open dsdb-20190227 ycDmKEs1R-OOjxpy1X5e0A 1 0       0 0   261b   261b
green open dsdb-20190215 z1_ihBtZTRKHwVNSnSD2VA 1 0  414694 0 18.3mb 18.3mb
green open dsdb-20190216 FC2VzadbTO2X6SyICFkVDQ 1 0       0 0   262b   262b
green open dsdb-20190219 w6knHcX9TbaStvuSKGA-hQ 1 0       0 0   261b   261b
green open intel         penDos3qQWyYfT-5GU7kjQ 1 0       0 0   230b   230b
green open dsdb-20190218 zUxcZFqAT4WogfVHwlgR1g 1 0 1020471 0 44.8mb 44.8mb
```

* This above commands will listed all indices which is available in the database with current date index.

Below reasons due to daily auto index creation getting failed,

* Disk space is utilised above 90% on Datastore or A10 server.
* Docker container is not in running state.
* Database service is not running state.

## **Manually index creation**

* Login into Datastore or A10 container and check for the today’s index is created or not in the database using following commands:

```text
$ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 1 1 111 111 0 0 0 \n'
>>> m = es.cat.indices()
>>> for i in es.cat.indices().split('\n'):
...             print i
... 
green open ustats        O8o7QS2AREKELr8y01QjCw 1 0      80 0 36.1kb 36.1kb
green open sysdata       lHNL8mVfTTWDEFgypiIrpA 1 0   16781 0  2.5mb  2.5mb
green open dsdb-20190214 jTxMqVXhSue9bqbiiPAFGw 1 0       0 0   262b   262b
green open ctdp_conf     n3zmyiesTRGvZxeNHf9_RA 1 0       1 0  4.1kb  4.1kb
green open logging       yJfkcyw3T9SXDYz7BYqFZw 1 0       0 0   262b   262b
green open dsdb-20190227 ycDmKEs1R-OOjxpy1X5e0A 1 0       0 0   261b   261b
green open dsdb-20190215 z1_ihBtZTRKHwVNSnSD2VA 1 0  414694 0 18.3mb 18.3mb
green open dsdb-20190216 FC2VzadbTO2X6SyICFkVDQ 1 0       0 0   262b   262b
green open dsdb-20190219 w6knHcX9TbaStvuSKGA-hQ 1 0       0 0   261b   261b
green open intel         penDos3qQWyYfT-5GU7kjQ 1 0       0 0   230b   230b
green open dsdb-20190218 zUxcZFqAT4WogfVHwlgR1g 1 0 1020471 0 44.8mb 44.8mb
```

This above commands will listed all indices which is available in the database with current date index.

* If auto current index is not created, stop din and ingestor services and manually create current date index.
* Stop din and ingestor services using following commands, 

```text
$ supervisorctl
supervisor> stop din ingestor 
din: stopped
ingestor: stopped
supervisor> exit
```

* Manually create the current date index using following commands,

```text
$ crontab -l
30 23 * * * /usr/bin/python /usr/src/nm/rtdp-datastore-v6/ecurator/ecurator.py
*/15 * * * * /usr/bin/python /usr/src/nm/rtdp-datastore-v6/iliteralDS/create_field_list.py
*/5 * * * * /usr/bin/python /usr/src/nm/rtdp-datastore-v6/ServiceDaemon/alerts.py
```

*  Execute the ecurator.py script for index creation using following command,

```text
$ /usr/bin/python /usr/src/nm/rtdp-datastore-v6/ecurator/ecurator.py
```

* Check for the current date index which is available under **/dnif/{Deploy-Key}/data/{Cluster-Name}/nodes/0/indices** folder.
* If current date index is available in database folder, start din and ingestor services using following commands.

```text
$ supervisorctl
supervisor> start din ingestor 
din: started
ingestor: started
supervisor> exit
```

