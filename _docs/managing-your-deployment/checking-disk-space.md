---
title: Checking Disk Space
permalink: /docs/Managing-Your-Deployment/Checking-Disk-Space/
section: Managing-Your-Deployment
---

# Checking Disk Space

**df command** shows the amount of disk space used and available on Linux file systems.

The disk-space utilization should not be more than 90%.

```
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
none             58G   40G   19G  69% /
tmpfs           7.9G     0  7.9G   0% /dev
tmpfs           7.9G     0  7.9G   0% /sys/fs/cgroup
/dev/vda1        58G   40G   19G  69% /dnif
shm              64M   24K   64M   1% /dev/shm
tmpfs           7.9G     0  7.9G   0% /sys/firmware
```

If the disk-space utilization is up to 90% then the database stopped to ingest data, cluster and replication won't be working and database health status automatically goes down to red state.

