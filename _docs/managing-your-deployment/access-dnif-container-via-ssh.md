---
title: Access DNIF Container via ssh 
permalink: /docs/Managing-Your-Deployment/Access-DNIF-Container-Via-SSH/
section: Managing-Your-Deployment
---

# Access DNIF Container via ssh

Similar to any LINUX server, the DNIF container also allows a user to login using SSH on a custom ssh \(tcp/826\) port. However, this is only possible from the host machine.

### Login to your DNIF container

Open a terminal window on your host machine and ssh using the root user to the mapped ssh port i.e. tcp/826.

```text
$ ssh root@localhost -p 826
```

Next, you shall get a prompt to enter you password where you have to enter the password as : `tSV67BZ7bJKnme4`

A new bash prompt should open up for you. This signifies that you now have access within the container. 

### Understanding the connection between host machine and docker container

Docker container uses port 826 of the host machine. The host machine in turn connects over default SSH port 22 with the docker container via ssh port 826.

![](../.gitbook/assets/image%20%2834%29.png)

The recommended method to connect to your docker container is:

* First connect to the host machine via ssh port 22
* Then connect to the container via the host machine to ssh port 826 \(see diagram above\)

### Change the login password of DNIF container

You can also change the password for the container, but it will only be effective till the container is reset/restarted. Once the container is reset or restarted, then the container login password shall change back to the default password `tSV67BZ7bJKnme4` as per the original configuration.

Change your DNIF container password using following command,

```text
$ passwd root

Enter new UNIX password: 
Retype new UNIX password: 
passwd: password updated successfully
```

### Change default ssh port of DNIF container

We can’t change the Docker container SSH port, as port 826 is exposed during compilation of the container. If anyone changes SSH port from the _**sshd\_config**_ file, the port shall change but the user will be unable to login, as the port is unexposed.

### Securing DNIF container

![](../.gitbook/assets/image%20%2849%29.png)

For a secured connection it is recommended to set UFW \( Unix Firewall \) rules to only allow authorised IP addresses to connect to Docker port 826 on host machine. This rule shall deny un-authorized accesses to the Docker. 

You can execute the commands below,

```text
$ ufw default allow incoming

Default incoming policy changed to 'allow'
(be sure to update your rules accordingly)
```

```text
$ ufw deny in on eth0 to any port 826 proto tcp

Rules updated
Rules updated (v6)
```

> **Note : Where ‘eth0’ is the name of your NIC card.**

```text
$ ufw enable

Command may disrupt existing ssh connections. Proceed with operation (y|n)? Y
Firewall is active and enabled on system startup
```

```text
$ ufw status

Status: active

To                     Action     From
--                     ------     ----
826/tcp on enp0s3      DENY       Anywhere 
826/tcp (v6) on eth0   DENY       Anywhere (v6)
```



