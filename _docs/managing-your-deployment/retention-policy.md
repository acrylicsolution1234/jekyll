---
title: Retention Policy 
permalink: /docs/Managing-Your-Deployment/Retention-Policy/
section: Managing-Your-Deployment
---
# Retention Policy

## **About Retention Policy**

By default, the retention policy to make data available in the DNIF web console is set to **45 days**. This duration is called **retention**, which defines the time for which the information shall be maintained or “retained” online, irrespective of the format.

Any data that is older than that prescribed in the retention period will be available offline, but won’t be visible or available for query within the **web console**.

![](../.gitbook/assets/image%20%2861%29.png)

## **Find the Current Retention Policy**

* Login to your **Data Store** or **A10** container. Read more on [how to Login to the docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* The retention policy configuration is defined under **/dnif/{Deploy-Key}/csltuconfig/system** on Datastore and A10 container.

```text
$ vim /dnif/{Deploy-Key}/csltuconfig/system
{"system": {"admin": "user@domain.com", "ScopeList": ["AK8o6XoigPNP1Rd3OYDh"], "localip": "172.16.12.35", "systemid": "AV6o6ZofgSGP0Rd3MEXf", "shardcount": 1, "retention": 45}}
```

Where, **"retention": 45** indicates the number of live data retention period in days.

## **Change Retention Policy**

* Login to your **Data Store** or **A10** container. Read more on [how to Login to the docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* Open system configuration file located under **/dnif/{Deploy-Key}/csltuconfig/** folder and set required retention value for “retention”: key.

```text
$ vim /dnif/{Deploy-Key}/csltuconfig/system
{"system": {"admin": "user@domain.com", "ScopeList": ["AK8o6XoigPNP1Rd3OYDh"], "localip": "172.16.12.35", "systemid": "AV6o6ZofgSGP0Rd3MEXf", "shardcount": 1, "retention": 60}}
```

With the above configuration, we have changed retention policy from 45 days to 60 days.

> **Note: Increasing retention value will require additional storage space on disk to keep live-data.**

The changes shall be effective from midnight onwards, once the file have been saved. Secondly the retention period wont automatically change the closed index. It will be applicable from the current date applied for future indexing. 

\*\*\*\*

