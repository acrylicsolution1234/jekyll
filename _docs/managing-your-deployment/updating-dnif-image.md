---
title: Updating DNIF Image 
permalink: /docs/Managing-Your-Deployment/Updating-DNIF-Image/
section: Managing-Your-Deployment
---
# Updating DNIF Image

We periodically release an upgrade for DNIF components on the [Docker Hub](https://hub.docker.com/r/dnif/) repository.

You can easily upgrade your DNIF images by following these steps,

## Check your existing version

Before you start your upgrade process, you can check for the existing version of DNIF installed on your system by executing this command,

```text
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
dnif/ship-a10      7.8.0               76240932e221        1 months ago        1.54GB
hello-world        latest              05a3bd381fc2        17 months ago       1.84kB
```

## Stopping your DNIF container

Before you start your upgrade process, you need to stop the running instance of your DNIF container by following these steps,

If you know where the **docker-compose.yml** file is present within your system, you can traverse to the folder containing the file and execute the following command,

```text
$ docker-compose down
```

## Upgrading your DNIF image

Next, to upgrade the existing docker-image, you can use the `docker pull` command. This command downloads the latest docker-image from the Docker-Hub repository into your system.

```text
$ docker pull image-name<:TAG>
```

The text `<:TAG>` in the above command, needs to be replaced by a specific tag number for available releases.

As per your installed component, you can checkout and select the specific version for your component from the below links and can replace the text`<:TAG>` with that tag number.

## **Available release versions as of now**

You can check for available release tags on these links ****as per your architecture.

| DNIF container | Available release |
| :--- | :--- |
| dnif/ship-ad | [https://hub.docker.com/r/dnif/ship-ad/tags/](%20https://hub.docker.com/r/dnif/ship-ad/tags/) |
| dnif/ship-ds | [https://hub.docker.com/r/dnif/ship-ds/tags/](%20https://hub.docker.com/r/dnif/ship-ds/tags/) |
| dnif/ship-cr | [https://hub.docker.com/r/dnif/ship-cr/tags/](%20https://hub.docker.com/r/dnif/ship-cr/tags/) |
| dnif/ship-a10 | [https://hub.docker.com/r/dnif/ship-a10/tags/](https://hub.docker.com/r/dnif/ship-a10/tags/) |

**For Example:**

To upgrade to the latest release of dnif/ship-a10 container.

```text
$ docker pull dnif/ship-a10:7.8.3
```

> **Note - In the above example, `7.8.3` is the version TAG of docker-image used to fetch or upgrade to a specific version of docker-image. This type of command is used occasionally for the scenarios, where there is a special need for specific dnif-image.**

Open ****a docker-compose file using `vim` editor and edit the configuration of **docker-compose.yml** as shown:

| Field Name | Change to |
| :--- | :--- |
| image: "dnif/ship-a10:latest" | image: "dnif/ship-a10:&lt;To install a _specific version_ of DNIF or you can use `latest` for latest version&gt;" |

Contents of `docker-compose.yml` should be:

```text
version: '2.0'
services:
 comp-ship-a10:
   image: "dnif/ship-a10:latest"
   network_mode: "host"
   cap_add:
    - NET_ADMIN
   ports:
    - "127.0.0.1:826:22/tcp"
    - "443:443/tcp"
    - "8350:8350/tcp"
    - "8351:8351/tcp"
    - "8355:8355/tcp"
    - "8340:8340/tcp"
    - "9234:9234/tcp"
    - "9236:9236/tcp"
    - "514:514/udp"
    - "514:514/tcp"
    - "8362:8362/tcp"
   volumes:
    - /path/to/volume:/dnif
   environment:
    - "DKEY=DEPLOY_KEY"
    - "CRIP=127.0.0.1"
    - "bootstrap_memory_lock=true"
   ulimits:
    memlock:
     soft: -1
     hard: -1
   container_name: comp-dnif-a10
```

## Starting your DNIF container

We are ready to start the running instance of your DNIF container by following these steps,

If you know where the **docker-compose.yml** file is present within your system, you can traverse to the folder containing the same and execute the following command,

```text
$ docker-compose up -d
```



