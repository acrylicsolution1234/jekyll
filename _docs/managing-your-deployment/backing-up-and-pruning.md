---
title: Backing up and Pruning 
permalink: /docs/Managing-Your-Deployment/Backing-up-and-Pruning/
section: Managing-Your-Deployment
---
# Backing up and Pruning

### Creating a script for backup and integrity checks

> **This feature is available only for DNIF version &gt;= 7.9.x.**

> **Note: To configure backup on local system for DNIF versions 7.9.x to 8.1.0, contact DNIF Support for the backup script requirement.**
>
> **Backup feature is inbuilt in all DNIF versions above 8.4.x.**

#### Configure a folder where all the data and configuration will be copied.

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* The configuration can be done manually by creating or editing a **YML** template file that comes with the **Datastore** or **A10** installations.

```text
$ cd /dnif/{Depoly-Key}/tool/
$ vim sync.yml

SYNC:     
   FREQUENCY: 1
   DATE: 20190201
   EMAIL_RECIPIENT : admin@domain.com
```

Fields from the sync.yml template are explained below,

| Field Name | Description |
| :--- | :--- |
| FREQUENCY | No. of days prior to day of execution. \(If executing the script on Jan 10 and frequency is 1, backup will be executed for Jan 9\) |
| DATE | For a particular date of backup. |
| EMAIL\_RECIPIENT | To receive backup notification of either success or failure, to receive alerts you need to [Configure SMTP](https://docs.dnif.it/admin-guide/configuration/smtp-configuration) on datastore or A10 container. |

> **Note: `DATE`will always override frequency and if you want to configure daily backup of indexes, comment out `DATE`parameter from the sync.yml file and set the`FREQUENCY`as 1.**

* Copy a `local_backup.py` script for backup and integrity checks in the `tool` folder.
* Create below new two folders in the deploy key folder using following command,

```text
$ mkdir -p /dnif/{Deploy-Key}/backup
$ mkdir -p /dnif/{Deploy-Key}/snapshot
```

* Set `elasticsearch`group and user ownership of the`snapshot`folder to save snapshot of indices to be backed up .

```text
$ cd /dnif/{Deploy-Key}
$ chown -R elasticsearch:elasticsearch snapshot
```

* Configure snapshot repo in elasticsearch by modifying elasticsearch.yml to add `path.repo: ['/dnif/{Deploy-Key}/snapshot']` below the `path.data` parameter.

```text
$ vim /dnif/{Deploy-Key}/config/elasticsearch.yml

path.repo: ['/dnif/{Depoly-Key}/snapshot']
```

* Restart elasticsearch service using following command,

```text
$ /etc/init.d/elasticsearch stop

 * Stopping Elasticsearch Server
  
$ /etc/init.d/elasticsearch start
* Starting Elasticsearch Server
sysctl: setting key "vm.max_map_count": Read-only file system                                                                                                                                              [ OK ]
```

### Setting up a backup strategy 

Using above script, data will be backed up in the `backup` folder which is available under `/dnif/{Deploy-Key}`directory.

#### Schedule daily data backup

* You can schedule daily data backup in the backup folder using to setup a cron.
* Setup a cron using following command as per your requirement,

```text
$ crontab -e

30 1 * * * /usr/bin/python /dnif/{Deploy-Key}/tool/local_backup.py
```

* Save the changes and verify the added cron using following command,

```text
crontab -l

WORKDIR=/dnif/{Deploy-Key}
30 1 * * * /usr/bin/python /dnif/{Deploy-Key}/tool/local_backup.py
```

* Set the `FREQUENCY` of indexes you want to schedule backup from days previous to day of execution and comment out the `DATE` parameter from the sync.yml file.

```text
$ cd /dnif/{Depoly-Key}/tool/

$ vim sync.yml
SYNC:     
   FREQUENCY: 1
 #  DATE: 20190201
   EMAIL_RECIPIENT : admin@dnif.it
```

> **Note - To receive backup notification of either success or failure, to receive alerts you need to** [**Configure SMTP**](https://docs.dnif.it/admin-guide/configuration/smtp-configuration) **on datastore or A10 container.**

#### Manually data backup

* You can take manually backup of particular date index using setup a `DATE` parameter and comment out `FREQUENCY` parameter from the sync.yml file. 

```text
$ cd /dnif/{Depoly-Key}/tool/
$ vim sync.yml
SYNC:     
 #  FREQUENCY: 2
   DATE: 20190205
   EMAIL_RECIPIENT : admin@dnif.it
```

* Execute backup script for to take manually backup of a particular date using following command,

```text
$ cd /dnif/{Depoly-Key}/tool/
$ python local_backup.py
tar created at /dnif/{Deploy-Key}/backup/20190205 /dnif/{Deploy-Key}/snapshot/
```

### Verify the backed up indexes

As mentioned above, data will be backed up with integrity checks in the backup folder which is available under `/dnif/{Deploy-Key}`directory.

```text
$ cd /dnif/{Depoly-Key}/backup
$ ls
20190205.tar.gz  20190205.txt
```

You can see tar file with the name as date of backup, eg for above config it will be `20190205.tar.gz` and it also contain md5 hash of that tar file in a text file of name `20190205.txt`.

### **Location of the** backup utility **logs**

You receive backup success or failure notification over a designated email address which is configured by you in sync.yml file, if backup gets failed you can check the exact cause with the help of `backup_file.log` file.

```text
$ tail -f /dnif/{Deploy-Key}/log/backup_file.log
2019-02-27 10:21:57,534 WARNING ----------STARTING BACKUP----------
2019-02-27 10:21:57,541 INFO HEAD http://localhost:8240/dsdb-20190205 [status:200 request:0.004s]
2019-02-27 10:21:57,544 INFO GET http://localhost:8240/dsdb-20190205 [status:200 request:0.002s]
2019-02-27 10:21:57,544 WARNING Directory to copy dsdb-20190205
2019-02-27 10:21:57,548 INFO GET http://localhost:8240/_cluster/state [status:200 request:0.003s]
2019-02-27 10:21:57,566 INFO GET http://localhost:8240/_cat/indices/dsdb-20190205 [status:200 request:0.018s]
2019-02-27 10:21:57,567 WARNING green open dsdb-20190205 QmrD2l-6Q0q5tbKrZnF-LQ 1 0 0 0 262b 262b

2019-02-27 10:21:57,567 INFO index----dsdb-20190205---uuid--QmrD2l-6Q0q5tbKrZnF-LQ---exists--True--state--open
2019-02-27 10:21:57,572 INFO GET http://localhost:8240/ [status:200 request:0.005s]
2019-02-27 10:21:57,612 INFO PUT http://localhost:8240/_snapshot/snaps [status:200 request:0.015s]
2019-02-27 10:21:57,719 INFO PUT http://localhost:8240/_snapshot/snaps/dsdb-20190205?wait_for_completion=true [status:200 request:0.107s]
2019-02-27 10:21:57,741 INFO DELETE http://localhost:8240/_snapshot/snaps/dsdb-20190205 [status:200 request:0.019s]
2019-02-27 10:21:57,748 INFO DELETE http://localhost:8240/_snapshot/snaps [status:200 request:0.007s]
2019-02-27 10:21:57,748 WARNING sending email to designated addresses
```

If backup gets failed due to low disk space, output looks similar as below,

```text
$ tail -f /dnif/{Deploy-Key}/log/backup_file.log
2019-02-27 11:36:47,872 ERROR Error in backup at 2019-02-05 11:36:47.872225 for yml{'SYNC': {'DATE': 20190205, 'FREQUENCY': 7, 'EMAIL_RECIPIENT': 'admin@dnif.it'}} 
Traceback (most recent call last):
 File "backup.py", line 235, in initiate
   check_space(file_src,bkfolder)
 File "backup.py", line 132, in check_space
   '''.format(format_size(dest_size)))
Exception: Insufficient space at destination  - 67.62 MB,
       destination free space should be greater than 500GB and index size should be less than 80% of free disk space
```

### How regularly you need to backup

You need to take regularly backup of backed up indexes from `/dnif/{Depoly-Key}/backup` folder using a third party backup agent or any backup utility.

> **Note: Disk free space should be greater than 500GB and index size should be less than 80% of free disk space. otherwise your schedule data backup will getting failed.**

### Deleting indexes from database

> **Note: To configure data deletion scheduler, contact DNIF Support for the data deletion script requirement.**

There are two methods to delete data indexes from the database.

#### Manually delete indexes from database

Follow below steps for to delete indexes from DNIF Datastore or A10 container,

> **Note: If there are multiple Datastores in a Cluster/HA environment, then perform the steps only on one of the datastore server. Also use the command’s carefully, once the index is deleted it cannot be rolled back.**

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh) and perform following commands,

```text
$ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.cat.health()
u'1549265131 07:25:31 NETMON green 1 1 111 111 0 0 0 \n'
>>> m = es.cat.indices()
>>> for i in es.cat.indices().split('\n'):
...             print i
... 
green open ustats        O8o7QS2AREKELr8y01QjCw 1 0      80 0 36.1kb 36.1kb
green open sysdata       lHNL8mVfTTWDEFgypiIrpA 1 0   16781 0  2.5mb  2.5mb
green open dsdb-20190214 jTxMqVXhSue9bqbiiPAFGw 1 0       0 0   262b   262b
green open ctdp_conf     n3zmyiesTRGvZxeNHf9_RA 1 0       1 0  4.1kb  4.1kb
green open logging       yJfkcyw3T9SXDYz7BYqFZw 1 0       0 0   262b   262b
green open dsdb-20190227 ycDmKEs1R-OOjxpy1X5e0A 1 0       0 0   261b   261b
green open dsdb-20190215 z1_ihBtZTRKHwVNSnSD2VA 1 0  414694 0 18.3mb 18.3mb
green open dsdb-20190216 FC2VzadbTO2X6SyICFkVDQ 1 0       0 0   262b   262b
green open dsdb-20190219 w6knHcX9TbaStvuSKGA-hQ 1 0       0 0   261b   261b
green open intel         penDos3qQWyYfT-5GU7kjQ 1 0       0 0   230b   230b
green open dsdb-20190218 zUxcZFqAT4WogfVHwlgR1g 1 0 1020471 0 44.8mb 44.8mb
>>> es.indices.delete("{index_name}")
{u'acknowledged': True}
```

Data indexes starting with`dsdb-date` format, replace `index_name` with data index, which you want to delete from the database.

#### Create and schedule daily data index deletion 

You can schedule daily auto data index deletion activity after retention period.

* Login to your **Datastore** or **A10** container, connect to your [docker container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh).
* The configuration can be done manually by creating or editing a **YML** template file that comes with the **Datastore** or **A10** installations.

```text
$ cd /dnif/{Depoly-Key}/tool/
$ vim del.yml

PURGE:
  FREQUENCY: 46
  EMAIL_RECIPIENT : admin@domain.com
```

Fields from the del.yml template are explained below,

| Filed Name | Description |
| :--- | :--- |
| FREQUENCY | Total no of retention period + 1 day, if your retention period is 45 days then you can set `FREQUENCY` for 46 days. |
| EMAIL\_RECEPIENT | To receive backup notification of either success or failure, to receive alerts you need to [Configure smtp](https://docs.dnif.it/admin-guide/configuration/smtp-configuration) on datastore or A10 container. |

* Copy a `esdb_delete.py` script in the tool folder.
* You can schedule daily auto data index deletion activity using to setup a cron.
* Set the cron using following command as per your requirement,

```text
$ crontab -e

30 2 * * * /usr/bin/python /dnif/{Deploy-Key}/tool/esdb_delete.py
```

* Save the changes and verify the added cron using following command,

```text
crontab -l

WORKDIR=/dnif/{Deploy-Key}
30 1 * * * /usr/bin/python /dnif/{Deploy-Key}/tool/esdb_delete.py
```

* You receive indexing deletion success or failure notification over a designated email address which is configured by you in del.yml file, if activity gets failed you can check the exact cause with the help of `deleteindex.log` file.

```text
$ tail -f /dnif/{Deploy-Key}/log/deleteindex.log
```

