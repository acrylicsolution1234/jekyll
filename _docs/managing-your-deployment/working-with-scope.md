---
title: Working with Scope 
permalink: /docs/Managing-Your-Deployment/Working-with-Scope/
description: Creating logical partitions for your data and users
section: Managing-Your-Deployment
---

# Working with Scope

Each datastore has the ability to be logically partitioned into multiple scopes, each scope can hold data that needs to be segregated. As an example an organisation might be working with data that is originating from different device types or departments or application groups and have different user base. 

As a Managed Security Service Provider \(MSSP\) you could use different scope names to partition data from different customers, this data is stored in logically different containers and provides a segregated Role Based Access Control \(RBAC\).

It is not mandatory to create multiple scopes within your datastore, you could choose to bring all data into a single scope container, which would mean all users will have the same access level but can have different access to console features.

Below are the core functionalities 

* Partition data within the same datastore
* Retain data in separate containers, without any mangling
* Have different retention periods for different scopes
* RBAC for each scope, users can have different permission levels
* Users can have segregated view of console functions
* Archival and recovery could be scope based
* Correlation rules / analytics could be segregated at the scope level
* Dashboards / reports can be customised by scope and user
* Scope administrators can change access / correlation / analytic policies

Creating a scope is a decision that is required to be taken at the design phase, this is an irreversible decision after data has been indexed. Reindexing data is usually not a possibility for large data volumes.

