---
title: Adapter
permalink: /docs/Managing-Your-Deployment/DCC/Adapter/
section: Managing-Your-Deployment
sub-section: DCC
---
# Adapter

For to download DCC is available centrally at [http://dnif.it/downloads/tools/dcc-latest.tar.gz](http://dnif.it/downloads/tools/dcc-latest.tar.gz).

Copy the downloaded`dcc-latest.tar.gz`file on your DNIF Adapter server in the `/dnif/{Deploy-Key}/`folder.

**Getting started with DCC**

To execute DCC commands, login into [Adapter container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh), extract the`dcc-latest.tar.gz` into `{Depoy-Key}` folder and traverse to `/dnif/{Depoy-Key}/dcc` folder.

Execute below command to enable virtual environment,

```text
$ cd /dnif/{Depoy-Key}/dcc
$ source init.sh

++ echo 'Installing python3-dev'
Installing python3-dev
++ sudo dpkg -i ./Packages/libpython3.5.deb
(Reading database ... 37647 files and directories currently installed.)
Preparing to unpack ./Packages/libpython3.5.deb ...
Unpacking libpython3.5:amd64 (3.5.2-2ubuntu0~16.04.4) over (3.5.2-2ubuntu0~16.04.4) ...
Setting up libpython3.5:amd64 (3.5.2-2ubuntu0~16.04.4) ...
Processing triggers for libc-bin (2.23-0ubuntu9) ...
++ sudo dpkg -i ./Packages/libpython3.5-dev.deb
Selecting previously unselected package libpython3.5-dev:amd64.
(Reading database ... 37647 files and directories currently installed.)
Preparing to unpack .../Packages/libpython3.5-dev.deb ...
Unpacking libpython3.5-dev:amd64 (3.5.2-2ubuntu0~16.04.4) ...
Setting up libpython3.5-dev:amd64 (3.5.2-2ubuntu0~16.04.4) ...
++ sudo dpkg -i ./Packages/python3.5-dev.deb
Selecting previously unselected package python3.5-dev.
(Reading database ... 37771 files and directories currently installed.)
Preparing to unpack ./Packages/python3.5-dev.deb ...
Unpacking python3.5-dev (3.5.2-2ubuntu0~16.04.4) ...
Setting up python3.5-dev (3.5.2-2ubuntu0~16.04.4) ...
++ echo 'Installing virtualenv'
Installing virtualenv
++ pip install ./Packages/virtualenv-16.4.3-py2.py3-none-any.whl
Processing ./Packages/virtualenv-16.4.3-py2.py3-none-any.whl
Installing collected packages: virtualenv
Successfully installed virtualenv-16.4.3
```

Execute below command to deactivate virtual environment.

```text
$ deactivate
```

After enabling the virtual environment on Adapter, execute below command to start with DCC,

```text
$ dcc 

Usage: dcc [OPTIONS] COMMAND [ARGS]...

Options:
  --version  Show the version and exit.
  --help     Show this message and exit.

Commands:
  checks   DCC basic checks recipes.
  diag     diagnostics functions
  service  service stats
  setup    Creates backup and restore of configuarations
  system   DNIF system stats
```

DCC is a command-line utility and it helps you perform basic checks, diagnostics,  check service status, system information, create and restore backup.

**checks**

It provides a list of commands to perform basic checks on Adapter, Datastore, Correlator and A10 components.

```text
$ dcc checks

Usage: dcc checks [OPTIONS] COMMAND [ARGS]...

  DCC basic checks recipes.

Options:
  --help  Show this message and exit.

Commands:
  a10  return A10 component status
  ad   return AD component status
  cr   return CR component status
  ds   return DS component status
```

* **ad**

  This command helps you view crucial stats for Adapter, like server information \(deployment key, IP address, system type, system ID, system name\), related services status, Adapter to Datastore connectivity, CPU and memory utilisation, NS connectivity status, disk utilisation, DNIF version details, Ingestion queue, incoming queue, EPS status, Queue server status and UNET connectivity status.

```text
$ dcc checks ad

---
AD:
- CPU Utilization:
    cpu_percent: 100.0
  DISK Utilization:
    free: 1.12 GB
    percent: 85.4
    total: 7.69 GB
    used: 6.56 GB
  MEMORY Utilization:
    memory:
      swap_memory:
        free: 0.0 Byte
        percent: 0
        sin: 0.0 Byte
        sout: 0.0 Byte
        total: 0.0 Byte
        used: 0.0 Byte
      virtual_memory:
        active: 1.42 GB
        available: 3.02 GB
        buffers: 299.88 MB
        cached: 2.45 GB
        free: 600.51 MB
        inactive: 1.32 GB
        percent: 21.8
        shared: 40.77 MB
        slab: 502.92 MB
        total: 3.86 GB
        used: 537.95 MB
- EPS status:
    15Minute:
      '2019-06-26 13:00:00': 5.0
    1Hour:
      '2019-06-26 13:00:00': 5.0
    1Minute:
      '2019-06-26 13:09:00': 4.5
    30Minute:
      '2019-06-26 13:00:00': 5.0
- InQueue value:
    status:
      ping: true
- AD services status:
    enrich-domain-tools:
      pid: '151'
      status: RUNNING
      uptime: 3 days, 21:21:31
    listener-bluecoat-udp:
      pid: '156'
      status: RUNNING
      uptime: 3 days, 21:21:31
    listener-cef:
      pid: '159'
      status: RUNNING
      uptime: 3 days, 21:21:31
    listener-http:
      pid: ''
      status: STOPPED
      uptime: Not started
    listener-https:
      pid: ''
      status: STOPPED
      uptime: Not started
    listener-netflow-v5:
      pid: ''
      status: STOPPED
      uptime: Not started
    listener-syslog-tcp:
      pid: '155'
      status: RUNNING
      uptime: 3 days, 21:21:31
    listener-syslog-udp:
      pid: '158'
      status: RUNNING
      uptime: 3 days, 21:21:31
    service-etl:01:
      pid: '154'
      status: RUNNING
      uptime: 3 days, 21:21:31
    service-etl:02:
      pid: '153'
      status: RUNNING
      uptime: 3 days, 21:21:31
    service-ohw:
      pid: '161'
      status: RUNNING
      uptime: 3 days, 21:21:31
    service-overwatch:
      pid: '160'
      status: RUNNING
      uptime: 3 days, 21:21:31
    service-sshd:
      pid: '157'
      status: RUNNING
      uptime: 3 days, 21:21:31
    system-refresh:
      pid: '152'
      status: RUNNING
      uptime: 3 days, 21:21:31
- InQueue value:
    length:
      inqueue: 0
- EPP inqueue and outqueue value:
    service-etl:
    - '2019-06-25 09:25:07,282 INFO Parser: {''outqueue'': 0, ''redisq'': 0}'
    - '2019-06-25 09:25:12,289 INFO Parser: {''outqueue'': 0, ''redisq'': 0}'
    - '2019-06-25 09:25:17,302 INFO Parser: {''outqueue'': 0, ''redisq'': 0}'
    - '2019-06-25 09:25:22,311 INFO Parser: {''outqueue'': 0, ''redisq'': 0}'
    - '2019-06-25 09:25:27,321 INFO Parser: {''outqueue'': 0, ''redisq'': 0}'
- AD server information:
    dnif_file:
      deployKey: YAJRDR
      localIPv4Address: 172.31.30.39
      systemClassType: AD
      systemID: AVYHCVGZrcGbt3iNksi9
      systemName: GLOBAL-SCOPE
  DNIF version:
    version: 8.1.1
- AD to DS connectivity:
    status:
      DS_8350: Port 8350 for DS  is reachable from AD.
      DS_8351: Port 8351 for DS  is reachable from AD.
      DS_8352: Port 8352 for DS  is reachable from AD.
      DS_8355: Port 8355 for DS  is reachable from AD.
      DS_8370: Port 8370 for DS  is not reachable from AD.
      NS: AD and DS connection is working.
- Connectivity status:
    ns_connectivity:
      CR:service-api: 172.31.30.126:8362
      DS:service-nameserver: 172.31.17.48:8350
      DS:service-rpc: 172.31.17.48:8351
      DS:service-rpc-api: 172.31.17.48:8355
- UNET connectivity status:
    status: Connected
```

**diag**

It lists down connectivity status of Adapter with Datastore, Adapter queue status and worker process stats.

```text
$ dcc diag

Usage: dcc diag [OPTIONS] COMMAND [ARGS]...

  diagnostics functions

Options:
  --help  Show this message and exit.

Commands:
  connectivity  DNIF connectivity stats
  datacluster   DNIF DS (datastore) stats
  queues        DNIF queues stats
  worker        DNIF worker stats
```

* **connectivity**

This command lists out options like connectivity checks between Adapter to Datastore, it also checks UNET and NS connectivity status.

```text
$ dcc diag connectivity

Usage: dcc diag connectivity [OPTIONS] COMMAND [ARGS]...

  DNIF connectivity stats

Options:
  --help  Show this message and exit.

Commands:
  ad-ds   AD to DS
  cr-ds   CR to DS
  status  status
  unet    unet
```

* **ad-ds**

  This command checks ingestion and NS connectivity on Adapter.

```text
$ dcc diag connectivity ad-ds

---
status:
  DS_8350: Port 8350 for DS  is reachable from AD.
  DS_8351: Port 8351 for DS  is reachable from AD.
  DS_8352: Port 8352 for DS  is reachable from AD.
  DS_8355: Port 8355 for DS  is reachable from AD.
  DS_8370: Port 8370 for DS  is not reachable from AD.
  NS: AD and DS connection is working.
```

* **status**

  This command will return the status of processes within NS services.

```text
$ dcc diag connectivity status

---
ns_connectivity:
  CR:service-api: 172.31.30.126:8362
  DS:service-api: 172.31.17.48:8355
  DS:service-nameserver: 172.31.17.48:8350
  DS:service-rpc: 172.31.17.48:8351
```

* **unet**

  This command checks the connectivity with UNET \(api2.netmonastery.com\) from Adapter.

```text
$ dcc diag connectivity unet

---
status: Connected
```

* **queues**

This command lists inward queue count,  queue keys, queue server status.

```text
$ dcc diag queues

Usage: dcc diag queues [OPTIONS] COMMAND [ARGS]...

  DNIF queues stats

Options:
  --help  Show this message and exit.

Commands:
  show-ingestor-queue      return dataqueue length
  show-inward-queue        return evtmem length
  show-keys                return queue keys
  show-notification-queue  return emailer length
  show-queue-status        return queue status
  show-report-queue        return reportqueue length
```

* **show-inward-queue**

  This command will return incoming queue count.

```text
$ dcc diag queues show-inward-queue

---
length:
  inqueue: 1230
```

* **show-queue-status**

  This command will return queue server status.

```text
$ dcc diag queues show-queue-status

---
status:
  ping: true
```

* **show-keys**

  This command will return the _keys_ information of the queue server.

```text
$ dcc diag queues show-keys

---
keys:
- ingest_din
- ingest_usage
- ucall-20190624
- ucall-20190621
- SYSL
- ucall-20190622
- ucall-20190625
- ucall-20190623
- watchdog
```

**service**

This command provides a list of crucial information like EPS of all active listeners, Adapter services logs and options for Adapter services management.

```text
$ dcc service

Usage: dcc service [OPTIONS] COMMAND [ARGS]...

  service stats

Options:
  --help  Show this message and exit.

Commands:
  list            used to retrive list of allowed services
  restart         used to restart service
  set-log-level   used to set log level
  show-eps-count  used to tail all running listener services log
  start           used to start service
  status          returns service status
  stop            used to stop service
  tail            used to tail service log
```

* **show-eps-count**

  This command will return the EPS count of listener Syslog UDP .

```text
$ dcc service show-eps-count

---
15Minute:
  '2019-06-26 13:00:00': 5.0
1Hour:
  '2019-06-26 13:00:00': 5.0
1Minute:
  '2019-06-26 13:08:00': 5.0
30Minute:
  '2019-06-26 13:00:00': 5.0
```

* **list**

  This command retrieves the list of all Adapter services .

```text
$ dcc service list

---
services:
- service-ohw
- listener-bluecoat-udp
- listener-cef
- system-refresh
- enrich-domain-tools
- service-monitor
- service-etl:01
- service-etl:02
- listener-http
- listener-https
- listener-netflow-v5
- service-sshd
- listener-syslog-tcp
- listener-syslog-udp
```

* **status**

  This command is used to check status of Adapter services.

  * Syntax:

    `$ dcc service status {service_name}`

```text
$ dcc service status listener-syslog-udp

---
listener-syslog-udp:
  pid: '158'
  status: RUNNING
  uptime: 3 days, 19:49:50
```

* **start**

  This command is used to start Adapter services.

  * Syntax

          `$ dcc service start {service_name}`

```text
$ dcc service start listener-syslog-udp

---
listener-syslog-udp:
  Status: started
```

* **stop**

  This command is used to stop Adapter services.

  * Syntax

    `$ dcc service stop {service_name}`

```text
$ dcc service stop listener-syslog-udp

---
listener-syslog-udp:
  Status: stopped
```

* **restart**

  This command is used to restart Adapter services.

  * Syntax

    `$ dcc service restart {service_name}`

```text
$ dcc service restart listener-syslog-udp

---
listener-syslog-udp:
  status:
  - stopped
  - started
```

* **tail**

  This command ****prints the last 10 lines of log of the given service.

  * Syntax

    `$ dcc service tail {service_name}`

```text
$ dcc service tail listener-syslog-udp

---
listener-syslog-udp:
- '2019-06-25 09:22:07,923 INFO SyslogListenerUDP: EPS 120'
- '2019-06-25 09:22:08,117 INFO SyslogListenerUDP: EPS 130'
- '2019-06-25 09:22:09,118 INFO SyslogListenerUDP: EPS 203'
- '2019-06-25 09:22:10,359 INFO SyslogListenerUDP: EPS 240'
- '2019-06-25 09:22:11,923 INFO SyslogListenerUDP: EPS 140'
- '2019-06-25 09:22:12,117 INFO SyslogListenerUDP: EPS 340'
- '2019-06-25 09:22:13,118 INFO SyslogListenerUDP: EPS 430'
- '2019-06-25 09:22:14,359 INFO SyslogListenerUDP: EPS 506'
- '2019-06-25 09:22:15,379 INFO SyslogListenerUDP: EPS 403'
- '2019-06-25 09:22:16,403 INFO SyslogListenerUDP: EPS 205'
```

* **set-log-level**

  This command is used to set log level as per your requirement.

  * Syntax

    `$ dcc service set-log-level {service_name} {level_name}`

```text
$ dcc service set-log-level listener-syslog-udp info

---
listener-syslog-udp: set loglevel 1
```

| level name | level number |
| :--- | :--- |
| debug | 0 |
| info | 1 |
| warning | 4 |
| error | 5 |
| critical | 6 |

**setup**

This command provides a list of options like backup and restore management, functionality of change port and password of dcc admin and reset Adapter configuration.

```text
$ dcc setup

Usage: dcc setup [OPTIONS] COMMAND [ARGS]...

  Creates backup and restore of configuarations

Options:
  --help  Show this message and exit.

Commands:
  backup                     Creates backup
  backup-list                Returns backup list
  change-dcc-admin-password  Change DCC Admin Password
  change-dcc-port            Change Flask API Port
  reset-statics              Resets the system to default configs
  restore                    Restores backup file based on date
```

* **backup**

  This command takes the backup of Adapter configuration files and stores it into _config\_backup_ folder which is available under deploy key folder.

```text
$ dcc setup backup

File backup comepleted. Config direcotory does not exist! (AD and CR server doesn't have a config direcotory.)
 Csltuconfig backed up successfully.
```

* **backup-list**

  This command will return the list of backups which were created as a result of backup command. 

```text
$ dcc setup backup-list 

---
result:
- - csltuconfig_D2019-06-25T09:47:56
```

* **restore**

  This command is used to restore the configuration files created during backup.

```text
$ dcc setup restore

Write foldername after doing backup-list.: csltuconfig_D2019-06-25T09:47:56
csltuconfig contains the following files.
['inline', 'SyslogListenerUDP', 'devices', 'dump_thresh', 'BCPLTCP', 'ADinline', 'updates', 'ConfigReporter', 'EventsParserProcessor', 'SyslogListenerTCP', 'limiter', 'Pyro', 'CEFListenerUDP', 'system', 'sdbg', 'EppMonitor', 'spfl', 'sdiag', 'logTranslator', 'JsonListener']
Enter the filename to be restored from csltuconfig_D2019-06-25T09:47:56: system 
File system restored.
```

> **Note: In above command you need to provide input like folder and file name which you want to restore.**

* **reset-statics**

  This command is used to reset the configuration files back to default.

```text
$ dcc setup reset-statics

---
status: Resetting statics complete!
```

* **change-dcc-port**

  This command is used to change default port of your DCC.

```
$ dcc setup change-dcc-port

Please enter the new DCC API port: 80
```

* **change-dcc-admin-password**

  This is used to change default password of your DCC.

```text
$ dcc setup change-dcc-admin-password

Please enter the username: dccmin
Please enter the new password: 
Please re-enter the new password: 
Password changed successfully.
```

**system**

This command list out options to check cpu, memory and disk utilisation, DNIF system information, IP address and network status, system package information, process status, system status and DNIF version details.

```text
$ dcc system

Usage: dcc system [OPTIONS] COMMAND [ARGS]...

  DNIF system stats

Options:
  --help  Show this message and exit.

Commands:
  cpu           return cpu status
  disk          return disk status
  dnif-version  return DNIF version
  info          return DNIF system information
  interface     return addresses associated to each NIC
  memory        return memory status
  network       return network status
  network-io    return network io associated to each NIC
  processes     return process status
  status        return system status
```

* **cpu**

  This command will return the CPU utilisation of the Adapter.

```text
$ dcc system cpu

---
cpu:
  cpu_percent:
    cpu_percent: 100.0
  cpu_stats:
    ctx_switches: 185415118615
    interrupts: 1838502350
    soft_interrupts: 23232422523
    syscalls: 0
  cpu_times:
    guest: 0.0
    guest_nice: 0.0
    idle: 29972217.73
    iowait: 55726.0
    irq: 0.0
    nice: 3706.49
    softirq: 889550.92
    steal: 33037189.85
    system: 7072251.77
    user: 12059515.9
```

* **disk**

  This command will return the disk utilisation of the Adapter.

```text
$ dcc system disk

---
disk:
  disk_io_counters:
    busy_time: 64908248
    read_bytes: 712082698752
    read_count: 17730110
    read_merged_count: 3885
    read_time: 918289936
    write_bytes: 575249674240
    write_count: 24160481
    write_merged_count: 26258819
    write_time: 203854156
  disk_partitions:
  - device: /dev/xvda1
    fstype: /dnif
    mountpoint: /dnif
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/resolv.conf
    mountpoint: /etc/resolv.conf
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/hostname
    mountpoint: /etc/hostname
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/hosts
    mountpoint: /etc/hosts
    opts: rw,relatime,discard,data=ordered
  disk_usage:
    free: 1.12 GB
    percent: 85.5
    total: 7.69 GB
    used: 6.56 GB
```

* **memory**

  This command will return the memory utilisation of the Adapter.

```text
$ dcc system memory

---
memory:
  swap_memory:
    free: 0.0 Byte
    percent: 0
    sin: 0.0 Byte
    sout: 0.0 Byte
    total: 0.0 Byte
    used: 0.0 Byte
  virtual_memory:
    active: 1.43 GB
    available: 3.04 GB
    buffers: 299.91 MB
    cached: 2.42 GB
    free: 657.77 MB
    inactive: 1.30 GB
    percent: 21.1
    shared: 40.77 MB
    slab: 464.75 MB
    total: 3.86 GB
    used: 511.80 MB
```

* **info**

  This command will return DNIF system information.

```text
$ dcc system info

---
dnif_file:
  deployKey: YAJRDR
  localIPv4Address: 172.31.30.39
  systemClassType: AD
  systemID: AVYHCVGZrcGbt3iNksi9
  systemName: GLOBAL-SCOPE
```

* **interface**

  This command will return IP addresses associated to each NIC.

```text
$ dcc system interface

---
ip_address:
  docker0:
  - address: 172.17.0.1
    broadcast: 172.17.0.1
    family: AddressFamily.AF_INET
    netmask: 255.255.0.0
    ptp: None
  - address: fe80::42:faff:fedb:44c6%docker0
    broadcast: None
    family: AddressFamily.AF_INET6
    netmask: 'ffff:ffff:ffff:ffff::'
    ptp: None
  - address: 02:42:fa:db:44:c6
    broadcast: ff:ff:ff:ff:ff:ff
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
  eth0:
  - address: 172.31.30.39
    broadcast: 172.31.31.255
    family: AddressFamily.AF_INET
    netmask: 255.255.240.0
    ptp: None
  - address: fe80::78:4bff:fea8:e9c8%eth0
    broadcast: None
    family: AddressFamily.AF_INET6
    netmask: 'ffff:ffff:ffff:ffff::'
    ptp: None
  - address: 02:78:4b:a8:e9:c8
    broadcast: ff:ff:ff:ff:ff:ff
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
  lo:
  - address: 127.0.0.1
    broadcast: None
    family: AddressFamily.AF_INET
    netmask: 255.0.0.0
    ptp: None
  - address: ::1
    broadcast: None
    family: AddressFamily.AF_INET6
    netmask: ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff
    ptp: None
  - address: 00:00:00:00:00:00
    broadcast: None
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
```

* **network**

  This command will return network status.

```text
$ dcc system network

---
network:
  nicstats:
    docker0:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '1500'
      speed: '0'
    eth0:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '9001'
      speed: '0'
    lo:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '65536'
      speed: '0'
```

* **network-io**

  This command will return network io associated to each NIC.

```text
$ dcc system network-io

---
interface IO:
  docker0:
  - bytes_recv: 7566734
    bytes_sent: 1197614189
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 128475
    packets_sent: 143675
  eth0:
  - bytes_recv: 91271544985
    bytes_sent: 54111553108
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 166839941
    packets_sent: 174258970
  lo:
  - bytes_recv: 9786391535352
    bytes_sent: 9786391535352
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 144067191361
    packets_sent: 144067191361
```

* **processes**

  This command will return status of system processes for Adapter.

```text
$ dcc system processes

---
process:
  process_list:
  - create_time: '2019-06-21 12:03:56'
    memory_percent: 0.06961522753994223
    name: bash
    pid: 1
    ppid: 0
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:56'
    memory_percent: 0.06694532534735922
    name: rsyslogd
    pid: 49
    ppid: 1
    status: sleeping
    terminal: null
    username: syslog
  - create_time: '2019-06-21 12:03:59'
    memory_percent: 0.21458102807056056
    name: redis-server
    pid: 89
    ppid: 1
    status: running
    terminal: null
    username: redis
  - create_time: '2019-06-21 12:03:59'
    memory_percent: 0.05923227456878608
    name: cron
    pid: 124
    ppid: 1
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:59'
    memory_percent: 0.4881965601771233
    name: supervisord
    pid: 148
    ppid: 1
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 1.3733185796515877
    name: python
    pid: 151
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 0.4856255432509322
    name: python
    pid: 152
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 0.832416172487548
    name: python
    pid: 153
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 0.8329105988195078
    name: python
    pid: 154
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 0.4103738555266481
    name: python
    pid: 155
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 0.41739470944047746
    name: python
    pid: 156
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 0.15683203249765393
    name: sshd
    pid: 157
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 0.41225267558809536
    name: python
    pid: 159
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 1.3545303790371146
    name: python
    pid: 160
    ppid: 148
    status: running
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:00'
    memory_percent: 0.464661866775836
    name: python
    pid: 161
    ppid: 148
    status: running
    terminal: null
    username: root
  - create_time: '2019-06-21 12:04:01'
    memory_percent: 0.31504845872479537
    name: python
    pid: 200
    ppid: 156
    status: running
    terminal: null
    username: root
  - create_time: '2019-06-25 09:39:01'
    memory_percent: 0.4371717627188702
    name: python
    pid: 9489
    ppid: 148
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 09:22:49'
    memory_percent: 0.16494062434179493
    name: sshd
    pid: 22133
    ppid: 157
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 09:22:57'
    memory_percent: 0.09285326514205362
    name: bash
    pid: 22683
    ppid: 22133
    status: sleeping
    terminal: /dev/pts/0
    username: root
  - create_time: '2019-06-25 10:13:20'
    memory_percent: 0.9152820257240133
    name: python
    pid: 26559
    ppid: 22683
    status: running
    terminal: /dev/pts/0
    username: root
```

* **status**

  This command will return up-time of Adapter server and active system user.

```text
$ dcc system status

---
platorm:
  boot_time: '2018-03-23 06:35:57'
  users:
  - host: 127.0.0.1
    name: root
    pid: 22683
    started: '2019-06-25 09:23:12'
    terminal: pts/0
```

* **dnif-version**

  This command will return DNIF version details.

```text
$ dcc system dnif-version

version: 8.1.1
```

