---
title: Correlator 
permalink: /docs/Managing-Your-Deployment/DCC/Correlator/
section: Managing-Your-Deployment
sub-section: DCC
---
# Correlator

For to download DCC is available centrally at [http://dnif.it/downloads/tools/dcc-latest.tar.gz](http://dnif.it/downloads/tools/dcc-latest.tar.gz).

Copy the provided`dcc-latest.tar.gz`file on your DNIF Correlator server in the `/dnif/{Deploy-Key}/`folder.

**Getting started with DCC**

To execute DCC commands, login into [Correlator container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh), extract the`dcc-latest.tar.gz` into `{Depoy-Key}` folder and traverse to `/dnif/{Depoy-Key}/dcc` folder.

Execute below command to enable virtual environment,

```text
$ cd /dnif/{Depoy-Key}/dcc
$ source init.sh

++ echo 'Installing python3-dev'
Installing python3-dev
++ sudo dpkg -i ./Packages/libpython3.5.deb
(Reading database ... 37647 files and directories currently installed.)
Preparing to unpack ./Packages/libpython3.5.deb ...
Unpacking libpython3.5:amd64 (3.5.2-2ubuntu0~16.04.4) over (3.5.2-2ubuntu0~16.04.4) ...
Setting up libpython3.5:amd64 (3.5.2-2ubuntu0~16.04.4) ...
Processing triggers for libc-bin (2.23-0ubuntu9) ...
++ sudo dpkg -i ./Packages/libpython3.5-dev.deb
Selecting previously unselected package libpython3.5-dev:amd64.
(Reading database ... 37647 files and directories currently installed.)
Preparing to unpack .../Packages/libpython3.5-dev.deb ...
Unpacking libpython3.5-dev:amd64 (3.5.2-2ubuntu0~16.04.4) ...
Setting up libpython3.5-dev:amd64 (3.5.2-2ubuntu0~16.04.4) ...
++ sudo dpkg -i ./Packages/python3.5-dev.deb
Selecting previously unselected package python3.5-dev.
(Reading database ... 37771 files and directories currently installed.)
Preparing to unpack ./Packages/python3.5-dev.deb ...
Unpacking python3.5-dev (3.5.2-2ubuntu0~16.04.4) ...
Setting up python3.5-dev (3.5.2-2ubuntu0~16.04.4) ...
++ echo 'Installing virtualenv'
Installing virtualenv
++ pip install ./Packages/virtualenv-16.4.3-py2.py3-none-any.whl
Processing ./Packages/virtualenv-16.4.3-py2.py3-none-any.whl
Installing collected packages: virtualenv
Successfully installed virtualenv-16.4.3
```

Execute below command to deactivate virtual environment.

```text
$ deactivate
```

After enabling the virtual environment on Correlator, execute below command to start with DCC,

```text
$ dcc 

Usage: dcc [OPTIONS] COMMAND [ARGS]...

Options:
  --version  Show the version and exit.
  --help     Show this message and exit.

Commands:
  checks   DCC basic checks recipes.
  diag     diagnostics functions
  service  service stats
  setup    Creates backup and restore of configuarations
  system   DNIF system stats
```

DCC is a command-line utility and it helps you perform basic checks, diagnostics,  check service status, system information, create and restore backup.

**checks**

Its provides list of commands to perform basic checks on Adapter, Datastore, Correlator and A10 components.

```text
$ dcc checks

Usage: dcc checks [OPTIONS] COMMAND [ARGS]...

  DCC basic checks recipes.

Options:
  --help  Show this message and exit.

Commands:
  a10  return A10 component status
  ad   return AD component status
  cr   return CR component status
  ds   return DS component status 
```

* **cr**

  This command helps you view crucial stats for Correlator, like server information \(deployment key, IP address, system type, system ID, system name\), related services status, Correlator to Datastore connectivity, cpu and memory utilisation, NS connectivity status, disk utilisation, DNIF version details, alerts queue, report queue, queue server status and unet connectivity status.

```text
$ dcc checks cr

---
CR:
- CPU Utilization:
    cpu_percent: 100.0
  DISK Utilization:
    free: 2.57 GB
    percent: 66.5
    total: 7.69 GB
    used: 5.10 GB
  MEMORY Utilization:
    memory:
      swap_memory:
        free: 0.0 Byte
        percent: 0
        sin: 0.0 Byte
        sout: 0.0 Byte
        total: 0.0 Byte
        used: 0.0 Byte
      virtual_memory:
        active: 1.55 GB
        available: 2.90 GB
        buffers: 281.39 MB
        cached: 2.40 GB
        free: 557.11 MB
        inactive: 1.21 GB
        percent: 24.7
        shared: 40.82 MB
        slab: 529.31 MB
        total: 3.86 GB
        used: 653.58 MB
- Queue server status:
    status:
      ping: true
- Alert queue status:
    length:
      emailer: 0
  Report queue status:
    length:
      reportqueue: 0
- CR to DS connectivity:
    status:
      DS_22: Port 22 for DS  is reachable from CR.
      DS_8340: Port 8340 for DS  is reachable from CR.
      DS_8350: Port 8350 for DS  is reachable from CR.
      DS_8351: Port 8351 for DS  is reachable from CR.
      DS_8355: Port 8355 for DS  is reachable from CR.
      DS_8361: Port 8361 for DS  is not reachable from CR.
      DS_8362: Port 8362 for DS  is not reachable from CR.
      NS: CR to DS connection is working.
- Connectivity status:
    ns_connectivity:
      CR:service-api: 172.31.30.126:8362
      DS:service-nameserver: 172.31.17.48:8350
      DS:service-rpc: 172.31.17.48:8351
      DS:service-rpc-api: 172.31.17.48:8355
- CR services status:
    cr-thread-pool:
      pid: '142'
      status: RUNNING
      uptime: 4 days, 1:06:20
    service-api:
      pid: '211'
      status: RUNNING
      uptime: 4 days, 1:06:14
    service-debug:
      pid: '143'
      status: RUNNING
      uptime: 4 days, 1:06:20
    service-emaild:
      pid: '141'
      status: RUNNING
      uptime: 4 days, 1:06:20
    service-ohw:
      pid: '140'
      status: RUNNING
      uptime: 4 days, 1:06:20
    service-scheduler:
      pid: '137'
      status: RUNNING
      uptime: 4 days, 1:06:20
    service-sshd:
      pid: '138'
      status: RUNNING
      uptime: 4 days, 1:06:20
- UNET connectivity status:
    status: Connected
- CR server information:
    dnif_file:
      deployKey: XBDV6R
      localIPv4Address: 172.31.30.126
      systemClassType: CR
      systemID: AVYHC_b-rcGbt3iNksj4
      systemName: GLOBAL-REPO
  DNIF version:
    version: 8.1.1
```

**diag**

It lists down connectivity status, queues status and worker process stats.

```text
$ dcc diag

Usage: dcc diag [OPTIONS] COMMAND [ARGS]...

  diagnostics functions

Options:
  --help  Show this message and exit.

Commands:
  connectivity  DNIF connectivity stats
  datacluster   DNIF DS (datastore) stats
  queues        DNIF queues stats
  worker        DNIF worker stats
```

* **connectivity**

This command lists out options like connectivity checks between Correlator to Datastore, it also checks UNET and NS connectivity.

```text
$ dcc diag connectivity

Usage: dcc diag connectivity [OPTIONS] COMMAND [ARGS]...

  DNIF connectivity stats

Options:
  --help  Show this message and exit.

Commands:
  ad-ds   AD to DS
  cr-ds   CR to DS
  status  status
  unet    unet
```

* **cr-ds**

  This command checks connectivity for Correlator.

```text
$ dcc diag connectivity cr-ds

---
status:
  DS_22: Port 22 for DS  is reachable from CR.
  DS_8340: Port 8340 for DS  is reachable from CR.
  DS_8350: Port 8350 for DS  is reachable from CR.
  DS_8351: Port 8351 for DS  is reachable from CR.
  DS_8355: Port 8355 for DS  is reachable from CR.
  DS_8361: Port 8361 for DS  is not reachable from CR.
  DS_8362: Port 8362 for DS  is not reachable from CR.
  NS: CR to DS connection is working.
```

* **status**

  This command will return the status of processes within NS services.

```text
$ dcc diag connectivity status

---
ns_connectivity:
  CR:service-api: 172.31.30.126:8362
  DS:service-nameserver: 172.31.17.48:8350
  DS:service-rpc: 172.31.17.48:8351
  DS:service-rpc-api: 172.31.17.48:8355
```

* **unet**

  This command checks the connectivity with UNET \(api2.netmonastery.com\) from Correlator.

```text
$ dcc diag connectivity unet

---
status: Connected
```

* **queues**

This command lists alerts queue count, queue sever keys, queue server status.

```text
$ dcc diag queues

Usage: dcc diag queues [OPTIONS] COMMAND [ARGS]...

  DNIF queues stats

Options:
  --help  Show this message and exit.

Commands:
  show-ingestor-queue      return dataqueue length
  show-inward-queue        return evtmem length
  show-keys                return queue keys
  show-notification-queue  return emailer length
  show-queue-status        return queue status
  show-report-queue        return reportqueue length
```

* **show-notification-queue**

  This command will return alert queue length.

```text
 $ dcc diag queues show-notification-queue

---
length:
  emailer: 0
```

* **show-keys**

  This command will return the _keys_ information for queue server.

```text
$ dcc diag queues show-keys

---
keys:
- next_dt
```

* **show-queue-status**

  This command will return the status of the queue server.

```text
$ dcc diag queues show-queue-status

---
status:
  ping: true
```

* **worker**

This command lists out options like worker active jobs, worker broker status, worker queues, worker registered tasks, worker reserved jobs.

```text
$ dcc diag worker

Usage: dcc diag worker [OPTIONS] COMMAND [ARGS]...

  DNIF worker stats

Options:
  --help  Show this message and exit.

Commands:
  show-active-jobs       return the worker active jobs
  show-queues            return the worker queues
  show-registered-tasks  return the worker registered tasks
  show-reserved-jobs     return the worker reserved jobs
  show-schduled          return the worker scheduled
  show-stats             return the worker info
  show-status            return the broker status
```

* **show-active-jobs**

  This command will return the worker active jobs.

```text
$ dcc diag worker show-active-jobs

---
celery@worker-node2: []
```

* **show-status**

  This command will return the worker broker status.

```text
$ dcc diag worker show-status

---
celery@worker-node2:
  ok: pong
```

* **show-queues**

  This command will return the worker queues.

```text
 $ dcc diag worker show-queues

celery@worker-node2:
- alias: null
  auto_delete: false
  binding_arguments: null
  bindings: []
  durable: true
  exchange:
    arguments: null
    auto_delete: false
    delivery_mode: 2
    durable: true
    name: celery
    passive: false
    type: direct
  exclusive: false
  name: celery
  no_ack: false
  queue_arguments: null
  routing_key: celery
```

* **show-registered-tasks**

  This command will return the worker registered tasks.

```text
$ dcc diag worker show-registered-tasks

---
celery@worker-node2:
- iliCR.long_search
- iliCR.submitsearch
```

* **show-reserved-jobs**

  This command will return the worker reserved jobs.

```text
$ dcc diag worker show-reserved-jobs

celery@worker-node2: []
```

* **show-schduled**

  This command will return the worker jobs which are scheduled.

```text
$ dcc diag worker show-schduled

celery@worker-node2: []
```

* **show-stats**

  This command will return the worker information.

```text
$ dcc diag worker show-stats

 broker:
    alternates: []
    connect_timeout: 4
    failover_strategy: round-robin
    heartbeat: null
    hostname: localhost
    insist: false
    login_method: null
    port: 6379
    ssl: false
    transport: redis
    transport_options: {}
    uri_prefix: null
    userid: null
    virtual_host: '1'
  clock: '350611'
  pid: 142
  pool:
    max-concurrency: 4
    max-tasks-per-child: 1
    processes:
    - 196
    - 199
    - 200
    - 201
    put-guarded-by-semaphore: false
    timeouts:
    - 0
    - 0
    writes:
      all: ''
      avg: 0.00%
      inqueues:
        active: 0
        total: 4
      raw: ''
      total: 0
  prefetch_count: 4
  rusage:
    idrss: 0
    inblock: 0
    isrss: 0
    ixrss: 0
    majflt: 0
    maxrss: 83504
    minflt: 38470
    msgrcv: 0
    msgsnd: 0
    nivcsw: 216880
    nsignals: 0
    nswap: 0
    nvcsw: 914016
    oublock: 8
    stime: 63.692
    utime: 539.268
  total: {}
```

**service**

This command provides a list of options for Correlator services management and options for to check the Correlator services logs.

```text
$ dcc service

Usage: dcc service [OPTIONS] COMMAND [ARGS]...

  service stats

Options:
  --help  Show this message and exit.

Commands:
  list            used to retrive list of allowed services
  restart         used to restart service
  set-log-level   used to set log level
  show-eps-count  used to tail all running listener services log
  start           used to start service
  status          returns service status
  stop            used to stop service
  tail            used to tail service log
```

* **list**

  This command retrieve the list of all Correlator services.

```text
$ dcc service list

---
services:
- cr-thread-pool
- service-ohw
- service-api
- service-scheduler
- service-emaild
- service-debug
- service-sshd
```

* **status**

  This command is used to check status of Correlator services.

  * Syntax

    `$ dcc service status {service_name}`

```text
$ dcc service status service-api

---
service-api:
  pid: '211'
  status: RUNNING
  uptime: 4 days, 1:50:49
```

* **start**

  This command is used to start Correlator services.

  * Syntax

    `$ dcc service start {service_name}`

```text
$ dcc service start service-api

service-api:
  Status: started
```

* **stop**

  This command is used to stop Correlator services.

  * Syntax

    `$ dcc service stop {service_name}`

```text
$ dcc service stop service-api

service-api:
  Status: stopped
```

* **restart**

  This command is used to restart Correlator services.

  * Syntax

    `$ dcc service restart {service_name}`

```text
$ dcc service restart service-api

---
service-api:
  status:
  - stopped
  - started
```

* **tail**

  This command ****print the last 10 number of lines of log of the given service

  * Syntax

    `$ dcc service tail {service_name}`

```text
$ dcc service tail service-api

service-api:
- '2019-05-02 06:53:02,465 INFO CRAPI: log level set to 1'
- '2019-05-02 06:53:02,981 INFO CRAPI: Ready to recieve'
- '2019-05-02 06:58:29,998 INFO CRAPI: log level set to 1'
- '2019-05-02 06:58:30,505 INFO CRAPI: Ready to recieve'
- '2019-05-02 06:59:02,898 INFO CRAPI: log level set to 1'
- '2019-05-02 06:59:03,411 INFO CRAPI: Ready to recieve'
- '2019-05-04 10:49:43,765 INFO CRAPI: log level set to 1'
- '2019-05-04 10:49:44,273 INFO CRAPI: Ready to recieve'
- '2019-05-04 10:50:10,345 INFO CRAPI: log level set to 1'
- '2019-05-04 10:50:10,855 INFO CRAPI: Ready to recieve'
```

* **set-log-level**

  This command is used to set log level as per your requirement.

  * Syntax

    `$ dcc service set-log-level {service_name} {level_name}`

```text
$ dcc service set-log-level service-api info

service-api: set loglevel 1
```

| level name | level number |
| :--- | :--- |
| debug | 0 |
| info | 1 |
| warning | 4 |
| error | 5 |
| critical | 6 |

**setup**

This command provides a list of options like backup and restore management, functionality of change port and password of dcc admin and reset Correlator configuration

```text
$ dcc setup

Usage: dcc setup [OPTIONS] COMMAND [ARGS]...

  Creates backup and restore of configuarations

Options:
  --help  Show this message and exit.

Commands:
  backup                     Creates backup
  backup-list                Returns backup list
  change-dcc-admin-password  Change DCC Admin Password
  change-dcc-port            Change Flask API Port
  reset-statics              Resets the system to default configs
  restore                    Restores backup file based on date
```

* **backup**

  This command takes the backup of Correlator configuration files and stores it into _config\_backup_ folder which is available under deploy key folder.

```text
$ dcc setup backup

File backup comepleted. Config direcotory does not exist! (AD and CR server doesn't have a config direcotory.)
 Csltuconfig backed up successfully.
```

* **backup-list**

  This command will return the list of backups which were created as a result of backup command.

```text
$ dcc setup backup-list 

---
result:
- - csltuconfig_D2019-06-25T14:08:43
```

* **restore**

  This command is used to restore the configuration files created during backup

```text
$ dcc setup restore

Write foldername after doing backup-list.: csltuconfig_D2019-06-25T14:08:43
csltuconfig contains the following files.
['correlator', 'CRFList', 'WebCRAPI', 'CRinline', 'system', 'notifemail', 'perf', 'CRAPI', 'Pyro', 'CRSch']
Enter the filename to be restored from csltuconfig_D2019-06-25T14:08:43: system
File system restored.
```

> **Note: In above command you need to provide input like folder and file name which you want to restore.**

* **reset-statics**

  This command is used to reset the configuration files back to default.

```text
$ dcc setup reset-statics

status: Resetting statics complete!
```

* **change-dcc-port**

  This command is used to change default port of your DCC.

```
$ dcc setup change-dcc-port

Please enter the new DCC API port: 80
```

* **change-dcc-admin-password**

  This command is used to change default password of your DCC.

```text
$ dcc setup change-dcc-admin-password

Please enter the username: dccmin
Please enter the new password: 
Please re-enter the new password: 
Password changed successfully.
```

**system**

This command list out options to check cpu, memory and disk utilisation, DNIF system information, IP address and network status, system package information, process status, system status and DNIF version details.

```text
$ dcc system

Usage: dcc system [OPTIONS] COMMAND [ARGS]...

  DNIF system stats

Options:
  --help  Show this message and exit.

Commands:
  cpu           return cpu status
  disk          return disk status
  dnif-version  return DNIF version
  info          return DNIF system information
  interface     return addresses associated to each NIC
  memory        return memory status
  network       return network status
  network-io    return network io associated to each NIC
  processes     return process status
  status        return system status
```

* **cpu**

  cpu command will return the CPU utilisation of the Correlator.

```text
$ dcc system cpu

---
cpu:
  cpu_percent:
    cpu_percent: 100.0
  cpu_stats:
    ctx_switches: 276835069546
    interrupts: 12800809758
    soft_interrupts: 24422586407
    syscalls: 0
  cpu_times:
    guest: 0.0
    guest_nice: 0.0
    idle: 26036691.55
    iowait: 27788.09
    irq: 0.0
    nice: 1871.08
    softirq: 867473.61
    steal: 1236571801.6
    system: 2571430.56
    user: 6936926.18
```

* **disk**

  This command will return the disk utilisation of the Correlator.

```text
$ dcc system disk

---
disk:
  disk_io_counters:
    busy_time: 43223240
    read_bytes: 664177516544
    read_count: 22383384
    read_merged_count: 4669
    read_time: 492582192
    write_bytes: 549423661056
    write_count: 20232665
    write_merged_count: 21614411
    write_time: 102991528
  disk_partitions:
  - device: /dev/xvda1
    fstype: /dnif
    mountpoint: /dnif
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/resolv.conf
    mountpoint: /etc/resolv.conf
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/hostname
    mountpoint: /etc/hostname
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/hosts
    mountpoint: /etc/hosts
    opts: rw,relatime,discard,data=ordered
  disk_usage:
    free: 2.57 GB
    percent: 66.5
    total: 7.69 GB
    used: 5.10 GB
```

* **memory**

  This command will return the memory utilisation of the Correlator.

```text
$ dcc system memory

---
memory:
  swap_memory:
    free: 0.0 Byte
    percent: 0
    sin: 0.0 Byte
    sout: 0.0 Byte
    total: 0.0 Byte
    used: 0.0 Byte
  virtual_memory:
    active: 1.55 GB
    available: 2.91 GB
    buffers: 281.42 MB
    cached: 2.40 GB
    free: 561.40 MB
    inactive: 1.21 GB
    percent: 24.6
    shared: 40.82 MB
    slab: 529.72 MB
    total: 3.86 GB
    used: 648.70 MB
```

* **info**

  This command will return DNIF system information.

```text
$ dcc system info

---
dnif_file:
  deployKey: XBDV6R
  localIPv4Address: 172.31.30.126
  systemClassType: CR
  systemID: AVYHC_b-rcGbt3iNksj4
  systemName: GLOBAL-REPO
```

* **interface**

  This command will return addresses associated to each NIC.

```text
$ dcc system interface

---
ip_address:
  docker0:
  - address: 172.17.0.1
    broadcast: 172.17.0.1
    family: AddressFamily.AF_INET
    netmask: 255.255.0.0
    ptp: None
  - address: 02:42:23:4c:97:93
    broadcast: ff:ff:ff:ff:ff:ff
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
  eth0:
  - address: 172.31.30.126
    broadcast: 172.31.31.255
    family: AddressFamily.AF_INET
    netmask: 255.255.240.0
    ptp: None
  - address: fe80::83:aeff:fed5:ab9e%eth0
    broadcast: None
    family: AddressFamily.AF_INET6
    netmask: 'ffff:ffff:ffff:ffff::'
    ptp: None
  - address: 02:83:ae:d5:ab:9e
    broadcast: ff:ff:ff:ff:ff:ff
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
  lo:
  - address: 127.0.0.1
    broadcast: None
    family: AddressFamily.AF_INET
    netmask: 255.0.0.0
    ptp: None
  - address: ::1
    broadcast: None
    family: AddressFamily.AF_INET6
    netmask: ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff
    ptp: None
  - address: 00:00:00:00:00:00
    broadcast: None
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
```

* **network**

  This command will return network status.

```text
$ dcc system network

---
network:
  nicstats:
    docker0:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '1500'
      speed: '0'
    eth0:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '1500'
      speed: '0'
    lo:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '65536'
      speed: '0'
```

* **network-io**

  This command will return network io associated to each NIC.

```text
$ dcc system network-io

---
interface IO:
  docker0:
  - bytes_recv: 0
    bytes_sent: 0
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 0
    packets_sent: 0
  eth0:
  - bytes_recv: 62816214086
    bytes_sent: 13221823911
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 89991809
    packets_sent: 83665119
  lo:
  - bytes_recv: 14309127622902
    bytes_sent: 14309127622902
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 206157678898
    packets_sent: 206157678898
```

* **processes**

  This command will return status of system processes for Correlator.

```text
$ dcc system processes

---
process:
  process_list:
  - create_time: '2019-06-21 12:03:52'
    memory_percent: 0.07307671769840972
    name: bash
    pid: 1
    ppid: 0
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:52'
    memory_percent: 0.07594441027385476
    name: rsyslogd
    pid: 39
    ppid: 1
    status: sleeping
    terminal: null
    username: syslog
  - create_time: '2019-06-21 12:03:53'
    memory_percent: 0.26402548884269816
    name: redis-server
    pid: 80
    ppid: 1
    status: running
    terminal: null
    username: redis
  - create_time: '2019-06-21 12:03:53'
    memory_percent: 0.059133798624694194
    name: cron
    pid: 115
    ppid: 1
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:54'
    memory_percent: 0.4801901774607274
    name: supervisord
    pid: 134
    ppid: 1
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:55'
    memory_percent: 1.9651605017868692
    name: python
    pid: 137
    ppid: 134
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:55'
    memory_percent: 0.1531743379091159
    name: sshd
    pid: 138
    ppid: 134
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:55'
    memory_percent: 1.9755435266289978
    name: python
    pid: 140
    ppid: 134
    status: running
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:55'
    memory_percent: 0.5906457845908
    name: python
    pid: 141
    ppid: 134
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:55'
    memory_percent: 2.0643431105169165
    name: celery
    pid: 142
    ppid: 134
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:55'
    memory_percent: 0.41907865981848497
    name: python
    pid: 143
    ppid: 134
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:58'
    memory_percent: 1.8368065375479845
    name: celery
    pid: 196
    ppid: 142
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:58'
    memory_percent: 1.8368065375479845
    name: celery
    pid: 199
    ppid: 142
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:58'
    memory_percent: 1.8369054234988618
    name: celery
    pid: 200
    ppid: 142
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:03:58'
    memory_percent: 1.8370043094497392
    name: celery
    pid: 201
    ppid: 142
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 14:04:15'
    memory_percent: 0.515591347874842
    name: python
    pid: 330
    ppid: 134
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 14:17:59'
    memory_percent: 0.9141017299108246
    name: python
    pid: 547
    ppid: 30205
    status: running
    terminal: /dev/pts/0
    username: root
  - create_time: '2019-06-25 10:21:29'
    memory_percent: 0.16276627514422515
    name: sshd
    pid: 30196
    ppid: 138
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 10:21:31'
    memory_percent: 0.0926561359721379
    name: bash
    pid: 30205
    ppid: 30196
    status: sleeping
    terminal: /dev/pts/0
    username: root
```

* **status**

  This command will return system status.

```text
$ dcc system status

---
platorm:
  boot_time: '2018-07-19 09:25:57'
  users:
  - host: 127.0.0.1
    name: root
    pid: 30205
    started: '2019-06-25 10:20:48'
    terminal: pts/0
```

* **dnif-version**

  This command will return DNIF version details.

```text
$ dcc system dnif-version

---
version: 8.1.1
```

