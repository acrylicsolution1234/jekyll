---
title: Datastore
permalink: /docs/Managing-Your-Deployment/DCC/Datastore/
section: Managing-Your-Deployment
sub-section: DCC
---
# Datastore

For to download DCC is available centrally at [http://dnif.it/downloads/tools/dcc-latest.tar.gz](http://dnif.it/downloads/tools/dcc-latest.tar.gz).

Copy the provided`dcc-latest.tar.gz`file on your DNIF Datastore server in the `/dnif/{Deploy-Key}/`folder.

**Getting started with DCC**

To execute DCC commands, login into [Datastore container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh), extract the`dcc-latest.tar.gz` into `{Deploy-Key}` folder and traverse to `/dnif/{Depoy-Key}/dcc` folder.

Execute below command to enable virtual environment,

```text
$ cd /dnif/{Depoy-Key}/dcc
$ source init.sh

++ echo 'Installing python3-dev'
Installing python3-dev
++ sudo dpkg -i ./Packages/libpython3.5.deb
(Reading database ... 37647 files and directories currently installed.)
Preparing to unpack ./Packages/libpython3.5.deb ...
Unpacking libpython3.5:amd64 (3.5.2-2ubuntu0~16.04.4) over (3.5.2-2ubuntu0~16.04.4) ...
Setting up libpython3.5:amd64 (3.5.2-2ubuntu0~16.04.4) ...
Processing triggers for libc-bin (2.23-0ubuntu9) ...
++ sudo dpkg -i ./Packages/libpython3.5-dev.deb
Selecting previously unselected package libpython3.5-dev:amd64.
(Reading database ... 37647 files and directories currently installed.)
Preparing to unpack .../Packages/libpython3.5-dev.deb ...
Unpacking libpython3.5-dev:amd64 (3.5.2-2ubuntu0~16.04.4) ...
Setting up libpython3.5-dev:amd64 (3.5.2-2ubuntu0~16.04.4) ...
++ sudo dpkg -i ./Packages/python3.5-dev.deb
Selecting previously unselected package python3.5-dev.
(Reading database ... 37771 files and directories currently installed.)
Preparing to unpack ./Packages/python3.5-dev.deb ...
Unpacking python3.5-dev (3.5.2-2ubuntu0~16.04.4) ...
Setting up python3.5-dev (3.5.2-2ubuntu0~16.04.4) ...
++ echo 'Installing virtualenv'
Installing virtualenv
++ pip install ./Packages/virtualenv-16.4.3-py2.py3-none-any.whl
Processing ./Packages/virtualenv-16.4.3-py2.py3-none-any.whl
Installing collected packages: virtualenv
Successfully installed virtualenv-16.4.3
```

Execute below command to deactivate virtual environment.

```text
$ deactivate
```

After enabling the virtual environment on Datastore, execute below command to start with DCC,

```text
$ dcc 

Usage: dcc [OPTIONS] COMMAND [ARGS]...

Options:
  --version  Show the version and exit.
  --help     Show this message and exit.

Commands:
  checks   DCC basic checks recipes.
  diag     diagnostics functions
  service  service stats
  setup    Creates backup and restore of configuarations
  system   DNIF system stats
```

DCC is a command-line utility and it help you to perform basic checks, diagnostics, check services status, system information, creates and restore backup.

**checks**

Its provides list of commands to perform basic checks on Adapter, Datastore, Correlator and A10 components.

```text
$ dcc checks

Usage: dcc checks [OPTIONS] COMMAND [ARGS]...

  DCC basic checks recipes.

Options:
  --help  Show this message and exit.

Commands:
  a10  return A10 component status
  ad   return AD component status
  cr   return CR component status
  ds   return DS component status
```

* **ds**

  This command helps you to view crucial status of Datastore, like server information \(deployment key, IP address, system type, system ID, system name\), related services status, current date index details, DB health status, DB server status, cpu and memory utilisation, disk utilisation, DNIF version details, data queue status, queue server status and UNET connectivity status.

```text
$ dcc checks ds

---
DS:
- CPU Utilization:
    cpu_percent: 100.0
  DISK Utilization:
    free: 260.89 MB
    percent: 96.7
    total: 7.69 GB
    used: 7.42 GB
  MEMORY Utilization:
    memory:
      swap_memory:
        free: 0.0 Byte
        percent: 0
        sin: 0.0 Byte
        sout: 0.0 Byte
        total: 0.0 Byte
        used: 0.0 Byte
      virtual_memory:
        active: 1.17 GB
        available: 835.76 MB
        buffers: 204.04 MB
        cached: 595.27 MB
        free: 377.36 MB
        inactive: 284.37 MB
        percent: 78.8
        shared: 41.21 MB
        slab: 251.62 MB
        total: 3.86 GB
        used: 2.71 GB
- Queue server status:
    status:
      ping: true
  data queue value:
    length:
      dataqueue: 0
- DB server status:
    result:
      status: OK
- DB server health status: green
- DS services status:
    ds-thread-pool:
      pid: '290'
      status: RUNNING
      uptime: 3 days, 22:40:30
    listener-ad:
      pid: '18515'
      status: RUNNING
      uptime: '4:09:45'
    service-api:
      pid: '673'
      status: RUNNING
      uptime: 3 days, 21:56:29
    service-emaild:
      pid: '298'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-events-collector:
      pid: '289'
      status: RUNNING
      uptime: 3 days, 22:40:30
    service-eventstore:
      pid: '291'
      status: RUNNING
      uptime: 3 days, 22:40:30
    service-ingest:01:
      pid: '295'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-ingest:02:
      pid: '293'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-ingest:03:
      pid: '294'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-ingest:04:
      pid: '296'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-nameserver:
      pid: '300'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-ohw:
      pid: '302'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-report-engine:
      pid: '301'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-rpc:
      pid: '288'
      status: RUNNING
      uptime: 3 days, 22:40:30
    service-rpc-api:
      pid: '297'
      status: RUNNING
      uptime: 3 days, 22:40:29
    service-sshd:
      pid: '299'
      status: RUNNING
      uptime: 3 days, 22:40:29
- DB server current index:
    dsdb-20190625:
      settings:
        index:
          blocks:
            read_only_allow_delete: 'true'
          creation_date: '2019-06-25T06:17:12'
          number_of_replicas: '0'
          number_of_shards: '1'
          provided_name: dsdb-20190625
          refresh_interval: 15s
          uuid: NhWTxALFRPenutAykX1vQA
          version:
            created: '6020499'
- UNET connectivity status:
    status: Connected
- DNIF version:
    version: 8.1.1
  DS server information:
    dnif_file:
      clusterName: GLOBAL-REPO
      deployKey: MX76IH
      systemClassType: DS
      systemID: AV9ToTGFgSGP0Rd3h8Zl
      systemName: GLOBAL-DS
```

**diag**

It lists down connectivity status, data cluster management, queues status and worker process stats.

```text
$ dcc diag

Usage: dcc diag [OPTIONS] COMMAND [ARGS]...

  diagnostics functions

Options:
  --help  Show this message and exit.

Commands:
  connectivity  DNIF connectivity stats
  datacluster   DNIF DS (datastore) stats
  queues        DNIF queues stats
  worker        DNIF worker stats
```

* **connectivity**

  This command lists out the options like connectivity checks for ****UNET and NS.

```text
$ dcc diag connectivity

Usage: dcc diag connectivity [OPTIONS] COMMAND [ARGS]...

  DNIF connectivity stats

Options:
  --help  Show this message and exit.

Commands:
  ad-ds   AD to DS
  cr-ds   CR to DS
  status  status
  unet    unet
```

* **status**

  This command will return the status of processes within NS services.

```text
$ dcc diag connectivity status

---
ns_connectivity:
  CR:service-api: 172.31.30.126:8362
  DS:service-nameserver: 172.31.17.48:8350
  DS:service-rpc: 172.31.17.48:8351
  DS:service-rpc-api: 172.31.17.48:8355
```

* **unet**

  This command checks the connectivity with UNET\(api2.netmonastery.com\) from Datastore.

```text
$ dcc diag connectivity unet

---
status: Connected
```

* **datacluster**

This command lists out option for database management like open/close/delete index, shards allocation details, current date index details, database health status, information of each index,  list out open/close index, master node information, cluster topology, cluster nodes statistics, list out cluster level changes, view of index shard recoveries, list out segments in the shards, database version details, list out currently executing tasks, list out cluster wide thread pool statistics per node.

```text
$ dcc diag datacluster

Usage: dcc diag datacluster [OPTIONS] COMMAND [ARGS]...

  DNIF DS (datastore) stats

Options:
  --help  Show this message and exit.

Commands:
  allocation            return shards allocation of each data node and disk...
  close-index           close an index
  current-index         return the current date index
  delete-index          delete an index
  health                return cluster health
  indices               return cross-section information of each index
  open-index            open an index
  show-closed-indices   return the list of closed indices
  show-master-node      return master node information
  show-node-stats       return detailed cluster nodes statistics
  show-node-topology    return cluster topology
  show-open-indices     return the list of open indices
  show-pending-tasks    return a list of any cluster-level changes
  show-recovery-status  return view of index shard recoveries
  show-running-tasks    return currently executing tasks on one or more...
  show-segments         return info about the segments in the shards of an...
  show-thread-pool      return cluster wide thread pool statistics per node
  status                return the datacluster status information.
```

* **allocation**

  This command will return shards allocation of each data node and disk space information.

```text
$ dcc diag datacluster allocation

result:
- disk.avail: 3gb
  disk.indices: 573.6kb
  disk.percent: '72'
  disk.total: 11.2gb
  disk.used: 8.1gb
  host: 172.31.17.48
  ip: 172.31.17.48
  node: w7nROLF
  shards: '7'
```

* **close-index**

  This command is used to close an index from the database.

  * Syntax:

    `$ dcc diag datacluster close-index {index-name}`

```text
$ dcc diag datacluster close-index dsdb-20190502

Index : dsdb-20190502 will be closed.
Do you want to continue? [y/N]: y
---
acknowledged: true
```

* **open-index** 

  This command is used to open an index form the database.

  * Syntax:

    `$ dcc diag datacluster open-index {index-name}`

```text
$ dcc diag datacluster open-index dsdb-20190502

Index : dsdb-20190502 wil be Open.
Do you want to continue? [y/N]: y
---
acknowledged: true
shards_acknowledged: true
```

* **delete-index**

  This command is used to delete an index from the database.

  * Syntax:

    `$ dcc diag datacluster delete-index {index-name}`

```text
$ dcc diag datacluster delete-index dsdb-201904029

Index : dsdb-20190429 will be Deleted.
Do you want to continue? [y/N]: y
---
{"acknowledged": true}
```

* **current-index**

  This command will return the current date index information.

```text
$ dcc diag datacluster current-index

---
dsdb-20190626:
  settings:
    index:
      creation_date: '2019-06-25T09:38:42'
      number_of_replicas: '0'
      number_of_shards: '1'
      provided_name: dsdb-20190626
      refresh_interval: 15s
      uuid: QfSEoYT1TBmYRxa9qTK6NA
      version:
        created: '6020499'
```

* **health**

  This command will return the health of database cluster.

```text
$ dcc diag datacluster health

---
active_primary_shards: 7
active_shards: 7
active_shards_percent_as_number: 100.0
cluster_name: GLOBAL-REPO
delayed_unassigned_shards: 0
initializing_shards: 0
number_of_data_nodes: 1
number_of_in_flight_fetch: 0
number_of_nodes: 1
number_of_pending_tasks: 0
relocating_shards: 0
status: green
task_max_waiting_in_queue_millis: 0
timed_out: false
unassigned_shards: 0
```

* **indices**

  This command will return cross-section information of each index.

```text
$ dcc diag datacluster indices

---
result:
- docs.count: '279778'
  docs.deleted: '0'
  health: green
  index: dsdb-20190503
  pri: '1'
  pri.store.size: 114.4mb
  rep: '0'
  status: open
  store.size: 114.4mb
  uuid: 2QhmVBuQT5WMnHRxwdk2Xw
- docs.count: '1206'
  docs.deleted: '7'
  health: green
  index: ctdp_conf
  pri: '1'
  pri.store.size: 5.3mb
  rep: '0'
  status: open
  store.size: 5.3mb
  uuid: ZBRa1fh4QtCBKhnwF6Ypig
- docs.count: '20'
  docs.deleted: '0'
  health: green
  index: dsdb-20190530
  pri: '1'
  pri.store.size: 53.2kb
  rep: '0'
  status: open
  store.size: 53.2kb
  uuid: k0EirdKhT2qPYhOcXpXMWg
- docs.count: '0'
  docs.deleted: '0'
  health: green
  index: dsdb-20190425
  pri: '1'
  pri.store.size: 263b
  rep: '0'
  status: open
  store.size: 263b
  uuid: BxIROBEZTGCdTno6r8KYoQ
- docs.count: '77697'
  docs.deleted: '0'
  health: green
  index: dsdb-20190422
  pri: '1'
  pri.store.size: 40.9mb
  rep: '0'
  status: open
  store.size: 40.9mb
  uuid: TNZISvZ-Ty6lZJO7iAX1mg
- docs.count: '100132'
  docs.deleted: '0'
  health: green
  index: dsdb-20190413
  pri: '1'
  pri.store.size: 75.6mb
  rep: '0'
  status: open
  store.size: 75.6mb
  uuid: T67ftuELRzGpaVwZrHDpIQ
- docs.count: '1'
  docs.deleted: '0'
  health: green
  index: dsdb-20190521
  pri: '1'
  pri.store.size: 7.7kb
  rep: '0'
  status: open
  store.size: 7.7kb
  uuid: gcXvnsqDTxmHKAxgO1rIpQ
- docs.count: '4381'
  docs.deleted: '0'
  health: green
  index: ustats
  pri: '1'
  pri.store.size: 332.9kb
  rep: '0'
  status: open
  store.size: 332.9kb
  uuid: hCPs9JV1QhaadGU9kvNBbw
- docs.count: '3'
  docs.deleted: '0'
  health: green
  index: dsdb-20190618
  pri: '1'
  pri.store.size: 14.1kb
  rep: '0'
  status: open
  store.size: 14.1kb
  uuid: 3UIWWZP9TfuDx_AfP1rJxA
- docs.count: '153833'
  docs.deleted: '0'
  health: green
  index: dsdb-20190412
  pri: '1'
  pri.store.size: 108.3mb
  rep: '0'
  status: open
  store.size: 108.3mb
  uuid: 9N1vRbCuS6WRn4ykv2T6ng
- docs.count: '182881'
  docs.deleted: '0'
  health: green
  index: dsdb-20190509
  pri: '1'
  pri.store.size: 77.8mb
  rep: '0'
  status: open
  store.size: 77.8mb
  uuid: Aug3SCwqQt2FkVv1Twu63w
- docs.count: '120255'
  docs.deleted: '0'
  health: green
  index: dsdb-20190411
  pri: '1'
  pri.store.size: 59.5mb
  rep: '0'
  status: open
  store.size: 59.5mb
  uuid: UUPsNt2URGyoViLt9o1ejA
- docs.count: '4'
  docs.deleted: '0'
  health: green
  index: dsdb-20190620
  pri: '1'
  pri.store.size: 26.5kb
  rep: '0'
  status: open
  store.size: 26.5kb
  uuid: sNnWGnjpR6Cxn_alDumiow
- docs.count: '6697'
  docs.deleted: '0'
  health: green
  index: dsdb-20190328
  pri: '1'
  pri.store.size: 6.7mb
  rep: '0'
  status: open
  store.size: 6.7mb
  uuid: NGJ1JqKxT9yZeejJ0wYXqA
- docs.count: '4'
  docs.deleted: '0'
  health: green
  index: dsdb-20190621
  pri: '1'
  pri.store.size: 20.3kb
  rep: '0'
  status: open
  store.size: 20.3kb
  uuid: 9IGrMZ4lTCyB7-AOf7H6Vw
- docs.count: '23897'
  docs.deleted: '0'
  health: green
  index: dsdb-20190327
  pri: '1'
  pri.store.size: 13.5mb
  rep: '0'
  status: open
  store.size: 13.5mb
  uuid: lfo60Nh1Rj2EQQikuiXTew
- docs.count: '0'
  docs.deleted: '0'
  health: green
  index: dsdb-20190424
  pri: '1'
  pri.store.size: 263b
  rep: '0'
  status: open
  store.size: 263b
  uuid: pSCQSuj2T62GgJ5nwi8kZw
```

* **show-closed-indices**

  This command will return the list of closed indices.

```text
$ dcc diag datacluster show-closed-indices

---
blocks:
  indices:
    dsdb-20190329:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190330:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190331:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190415:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190416:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190417:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190418:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190419:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190504:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
    dsdb-20190513:
      '4':
        description: index closed
        levels:
        - read
        - write
        retryable: false
cluster_name: GLOBAL-REPO
compressed_size_in_bytes: 29090
```

* **show-open-indices**

  This command will return the list of open indices.

```text
$ dcc diag datacluster show-open-indices

---
metadata:
  indices:
    cases:
      state: open
    ctdp_conf:
      state: open
    dsdb-20190523:
      state: open
    dsdb-20190524:
      state: open
    dsdb-20190529:
      state: open
    dsdb-20190530:
      state: open
    dsdb-20190531:
      state: open
    dsdb-20190601:
      state: open
    dsdb-20190602:
      state: open
    dsdb-20190603:
      state: open
    dsdb-20190618:
      state: open
    dsdb-20190619:
      state: open
    dsdb-20190620:
      state: open
    dsdb-20190621:
      state: open
    dsdb-20190622:
      state: open
    dsdb-20190624:
      state: open
    dsdb-20190625:
      state: open
    dsdb-20190626:
      state: open
    dsdb-20190627:
      state: open
    intel:
      state: open
    logging:
      state: open
    sysdata:
      state: open
    ustats:
      state: open
```

* **show-master-node**

  This command will return master node information.

```text
$ dcc diag datacluster show-master-node

result:
- host: 172.31.17.48
  id: w7nROLFlS66StR8rVI1NDQ
  ip: 172.31.17.48
  node: w7nROLF
```

* **show-node-topology**

  This command will return cluster topology.

```text
$ dcc diag datacluster show-node-topology

result:
- cpu: '54'
  heap.percent: '14'
  ip: 172.31.17.48
  load_15m: '1.32'
  load_1m: '1.31'
  load_5m: '1.31'
  master: '*'
  name: w7nROLF
  node.role: mdi
  ram.percent: '97'
```

* **show-node-stats**

  This command will return detailed cluster nodes statistics.

```text
$ dcc diag datacluster show-node-stats

---
_nodes:
  failed: 0
  successful: 1
  total: 1
cluster_name: GLOBAL-REPO
nodes:
  nEO4iaXDS9Cio7FI5rbdJw:
    adaptive_selection:
      nEO4iaXDS9Cio7FI5rbdJw:
        avg_queue_size: 0
        avg_response_time_ns: 1773920
        avg_service_time_ns: 2615959
        outgoing_searches: 0
        rank: '1.8'
    breakers:
      accounting:
        estimated_size: 2.4mb
        estimated_size_in_bytes: 2612579
        limit_size: 1.5gb
        limit_size_in_bytes: 1621884928
        overhead: 1.0
        tripped: 0
      fielddata:
        estimated_size: 0b
        estimated_size_in_bytes: 0
        limit_size: 928mb
        limit_size_in_bytes: 973130956
        overhead: 1.03
        tripped: 0
      in_flight_requests:
        estimated_size: 0b
        estimated_size_in_bytes: 0
        limit_size: 1.5gb
        limit_size_in_bytes: 1621884928
        overhead: 1.0
        tripped: 0
      parent:
        estimated_size: 2.4mb
        estimated_size_in_bytes: 2612579
        limit_size: 1gb
        limit_size_in_bytes: 1135319449
        overhead: 1.0
        tripped: 0
      request:
        estimated_size: 0b
        estimated_size_in_bytes: 0
        limit_size: 928mb
        limit_size_in_bytes: 973130956
        overhead: 1.0
        tripped: 0
    discovery:
      cluster_state_queue:
        committed: 0
        pending: 0
        total: 0
      published_cluster_states:
        compatible_diffs: 0
        full_states: 0
        incompatible_diffs: 0
    fs:
      data:
      - available_in_bytes: 23581188096
        free_in_bytes: 25530646528
        mount: /dnif (/dev/sda1)
        path: /dnif/VfeqjAr1NJvDSgCO/data/nodes/0
        total_in_bytes: 37912903680
        type: ext4
      io_stats:
        devices:
        - device_name: sda1
          operations: 32644
          read_kilobytes: 864244
          read_operations: 22027
          write_kilobytes: 568344
          write_operations: 10617
        total:
          operations: 32644
          read_kilobytes: 864244
          read_operations: 22027
          write_kilobytes: 568344
          write_operations: 10617
      least_usage_estimate:
        available_in_bytes: 23581188096
        path: /dnif/VfeqjAr1NJvDSgCO/data/nodes/0
        total_in_bytes: 37912903680
        used_disk_percent: 37.80168278580134
      most_usage_estimate:
        available_in_bytes: 23581188096
        path: /dnif/VfeqjAr1NJvDSgCO/data/nodes/0
        total_in_bytes: 37912903680
        used_disk_percent: 37.80168278580134
      timestamp: 1561534383142
      total:
        available_in_bytes: 23581188096
        free_in_bytes: 25530646528
        total_in_bytes: 37912903680
    host: 172.31.17.48
    http:
      current_open: 12
      total_opened: 129
    indices:
      completion:
        size_in_bytes: 0
      docs:
        count: 1565892
        deleted: 7
      fielddata:
        evictions: 0
        memory_size_in_bytes: 0
      flush:
        total: 46
        total_time_in_millis: 3946
      get:
        current: 0
        exists_time_in_millis: 0
        exists_total: 0
        missing_time_in_millis: 0
        missing_total: 0
        time_in_millis: 0
        total: 0
      indexing:
        delete_current: 0
        delete_time_in_millis: 0
        delete_total: 0
        index_current: 0
        index_failed: 0
        index_time_in_millis: 176
        index_total: 97
        is_throttled: false
        noop_update_total: 0
        throttle_time_in_millis: 0
      merges:
        current: 0
        current_docs: 0
        current_size_in_bytes: 0
        total: 2
        total_auto_throttle_in_bytes: 1048576000
        total_docs: 295908
        total_size_in_bytes: 23757729
        total_stopped_time_in_millis: 0
        total_throttled_time_in_millis: 0
        total_time_in_millis: 2795
      query_cache:
        cache_count: 0
        cache_size: 0
        evictions: 0
        hit_count: 0
        memory_size_in_bytes: 0
        miss_count: 0
        total_count: 0
      recovery:
        current_as_source: 0
        current_as_target: 0
        throttle_time_in_millis: 0
      refresh:
        listeners: 0
        total: 363
        total_time_in_millis: 651
      request_cache:
        evictions: 0
        hit_count: 30
        memory_size_in_bytes: 3419
        miss_count: 25
      search:
        fetch_current: 0
        fetch_time_in_millis: 450
        fetch_total: 308
        open_contexts: 0
        query_current: 0
        query_time_in_millis: 730
        query_total: 375
        scroll_current: 0
        scroll_time_in_millis: 0
        scroll_total: 0
        suggest_current: 0
        suggest_time_in_millis: 0
        suggest_total: 0
      segments:
        count: 167
        doc_values_memory_in_bytes: 48276
        file_sizes: {}
        fixed_bit_set_memory_in_bytes: 0
        index_writer_memory_in_bytes: 0
        max_unsafe_auto_id_timestamp: 1561498502695
        memory_in_bytes: 2612579
        norms_memory_in_bytes: 10112
        points_memory_in_bytes: 46195
        stored_fields_memory_in_bytes: 373104
        term_vectors_memory_in_bytes: 0
        terms_memory_in_bytes: 2134892
        version_map_memory_in_bytes: 0
      store:
        size_in_bytes: 855702695
      translog:
        operations: 378
        size_in_bytes: 89778
        uncommitted_operations: 87
        uncommitted_size_in_bytes: 20880
      warmer:
        current: 0
        total: 165
        total_time_in_millis: 0
    ingest:
      pipelines: {}
      total:
        count: 0
        current: 0
        failed: 0
        time_in_millis: 0
    ip: 172.31.17.48:8340
    jvm:
      buffer_pools:
        direct:
          count: 44
          total_capacity_in_bytes: 134908208
          used_in_bytes: 134908209
        mapped:
          count: 325
          total_capacity_in_bytes: 842048029
          used_in_bytes: 842048029
      classes:
        current_loaded_count: 11637
        total_loaded_count: 11637
        total_unloaded_count: 0
      gc:
        collectors:
          old:
            collection_count: 2
            collection_time_in_millis: 120
          young:
            collection_count: 7
            collection_time_in_millis: 238
      mem:
        heap_committed_in_bytes: 1621884928
        heap_max_in_bytes: 1621884928
        heap_used_in_bytes: 402782640
        heap_used_percent: 24
        non_heap_committed_in_bytes: 104267776
        non_heap_used_in_bytes: 97172736
        pools:
          old:
            max_in_bytes: 1307836416
            peak_max_in_bytes: 1307836416
            peak_used_in_bytes: 199677560
            used_in_bytes: 199677560
          survivor:
            max_in_bytes: 34865152
            peak_max_in_bytes: 34865152
            peak_used_in_bytes: 34865152
            used_in_bytes: 21015256
          young:
            max_in_bytes: 279183360
            peak_max_in_bytes: 279183360
            peak_used_in_bytes: 279183360
            used_in_bytes: 182089824
      threads:
        count: 48
        peak_count: 57
      timestamp: 1561534383142
      uptime_in_millis: 2453358
    name: nEO4iaX
    os:
      cpu:
        load_average:
          15m: 4.25
          1m: 4.75
          5m: 4.62
        percent: 74
      mem:
        free_in_bytes: 187404288
        free_percent: 5
        total_in_bytes: 4143362048
        used_in_bytes: 3955957760
        used_percent: 95
      swap:
        free_in_bytes: 4241711104
        total_in_bytes: 4292866048
        used_in_bytes: 51154944
      timestamp: 1561534383142
    process:
      cpu:
        percent: 0
        total_in_millis: 35580
      max_file_descriptors: 65536
      mem:
        total_virtual_in_bytes: 6307721216
      open_file_descriptors: 333
      timestamp: 1561534383142
    roles:
    - master
    - data
    - ingest
    script:
      cache_evictions: 0
      compilations: 0
    thread_pool:
      bulk:
        active: 0
        completed: 97
        largest: 4
        queue: 0
        rejected: 0
        threads: 4
      fetch_shard_started:
        active: 0
        completed: 49
        largest: 8
        queue: 0
        rejected: 0
        threads: 1
      fetch_shard_store:
        active: 0
        completed: 0
        largest: 0
        queue: 0
        rejected: 0
        threads: 0
      flush:
        active: 0
        completed: 92
        largest: 2
        queue: 0
        rejected: 0
        threads: 1
      force_merge:
        active: 0
        completed: 0
        largest: 0
        queue: 0
        rejected: 0
        threads: 0
      generic:
        active: 0
        completed: 4383
        largest: 4
        queue: 0
        rejected: 0
        threads: 4
      get:
        active: 0
        completed: 0
        largest: 0
        queue: 0
        rejected: 0
        threads: 0
      index:
        active: 0
        completed: 0
        largest: 0
        queue: 0
        rejected: 0
        threads: 0
      listener:
        active: 0
        completed: 0
        largest: 0
        queue: 0
        rejected: 0
        threads: 0
      management:
        active: 1
        completed: 349
        largest: 3
        queue: 0
        rejected: 0
        threads: 3
      refresh:
        active: 0
        completed: 23749
        largest: 2
        queue: 0
        rejected: 0
        threads: 2
      search:
        active: 0
        completed: 786
        largest: 7
        queue: 0
        rejected: 0
        threads: 7
      snapshot:
        active: 0
        completed: 0
        largest: 0
        queue: 0
        rejected: 0
        threads: 0
      warmer:
        active: 0
        completed: 0
        largest: 0
        queue: 0
        rejected: 0
        threads: 0
    timestamp: 1561534383120
    transport:
      rx_count: 0
      rx_size_in_bytes: 0
      server_open: 0
      tx_count: 0
      tx_size_in_bytes: 0
    transport_address: 172.31.17.48:8340
```

* **show-pending-tasks**

  This command returns a list of any cluster-level changes \(e.g. create index, update mapping, allocate or fail shard\) which have not yet been executed.

```text
$ dcc diag datacluster show-pending-tasks

---
result: []
```

* **show-recovery-status**

  This command will return view of index shard recoveries.

```text
$ dcc diag datacluster show-recovery-status

result:
- bytes: '0'
  bytes_percent: 0.0%
  bytes_recovered: '0'
  bytes_total: '0'
  files: '0'
  files_percent: 0.0%
  files_recovered: '0'
  files_total: '0'
  index: ctdp_conf
  repository: n/a
  shard: '0'
  snapshot: n/a
  source_host: n/a
  source_node: n/a
  stage: done
  target_host: 172.31.17.48
  target_node: w7nROLF
  time: 113ms
  translog_ops: '0'
  translog_ops_percent: 100.0%
  translog_ops_recovered: '0'
  type: empty_store
- bytes: '0'
  bytes_percent: 0.0%
  bytes_recovered: '0'
  bytes_total: '0'
  files: '0'
  files_percent: 0.0%
  files_recovered: '0'
  files_total: '0'
  index: cases
  repository: n/a
  shard: '0'
  snapshot: n/a
  source_host: n/a
  source_node: n/a
  stage: done
  target_host: 172.31.17.48
  target_node: w7nROLF
  time: 14ms
  translog_ops: '0'
  translog_ops_percent: 100.0%
  translog_ops_recovered: '0'
  type: empty_store
```

* **show-segments**

  This command will return info about the segments in the shards of an index.

```text
$ dcc diag datacluster show-segments

result:
- committed: 'true'
  compound: 'true'
  docs.count: '1'
  docs.deleted: '0'
  generation: '0'
  index: ctdp_conf
  ip: 172.31.17.48
  prirep: p
  searchable: 'true'
  segment: _0
  shard: '0'
  size: 6kb
  size.memory: '2535'
  version: 7.2.1
- committed: 'true'
  compound: 'false'
  docs.count: '109'
  docs.deleted: '0'
  generation: '120'
  index: ustats
  ip: 172.31.17.48
  prirep: p
  searchable: 'true'
  segment: _3c
  shard: '0'
  size: 12.6kb
  size.memory: '2892'
  version: 7.2.1
```

* **status**

  This command will return the database service status.

```text
$ dcc diag datacluster status

---
result:
  status: OK
```

* **show-running-tasks**

  This command will return currently executing tasks on one or more nodes in the cluster.

```text
$ dcc diag datacluster show-running-tasks

result:
- action: cluster:monitor/tasks/lists
  ip: 172.31.17.48
  node: w7nROLF
  parent_task_id: '-'
  running_time: 16.5ms
  start_time: '1556883156777'
  task_id: w7nROLFlS66StR8rVI1NDQ:51670
  timestamp: '11:32:36'
  type: transport
- action: cluster:monitor/tasks/lists[n]
  ip: 172.31.17.48
  node: w7nROLF
  parent_task_id: w7nROLFlS66StR8rVI1NDQ:51670
  running_time: 2.8ms
  start_time: '1556883156790'
  task_id: w7nROLFlS66StR8rVI1NDQ:51671
  timestamp: '11:32:36'
  type: direct
```

* **show-thread-pool**

  This command will return cluster wide thread pool statistics per node.

```text
$ dcc diag datacluster show-thread-pool

result:
- active: '0'
  name: bulk
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: fetch_shard_started
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: fetch_shard_store
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: flush
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: force_merge
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: generic
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: get
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: index
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: listener
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '1'
  name: management
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: refresh
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: search
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: snapshot
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
- active: '0'
  name: warmer
  node_name: w7nROLF
  queue: '0'
  rejected: '0'
```

* **queues**

This command lists inward queue count,  queue keys, queue server status.

```text
$ dcc diag queues

Usage: dcc diag queues [OPTIONS] COMMAND [ARGS]...

  DNIF queues stats

Options:
  --help  Show this message and exit.

Commands:
  show-ingestor-queue      return dataqueue length
  show-inward-queue        return evtmem length
  show-keys                return queue keys
  show-notification-queue  return emailer length
  show-queue-status        return queue status
  show-report-queue        return reportqueue length
```

* **show-ingestor-queue**

  This command will return data queue length.

```text
$ dcc diag queues show-ingestor-queue

---
length:
  dataqueue: 120
```

* **show-report-queue**

  This command will return report queue length.

```text
$ dcc diag queues show-report-queue

---
length:
  reportqueue: 0
```

* **show-keys**

  This command will return the _keys_ information for redis server.

```text
$ dcc diag queues show-keys

---
keys:
- _kombu.binding.celeryev
- _kombu.binding.celery.pidbox
- calper
- DataQueue
- _kombu.binding.celery
- ucall-20190624
- ucall-20190625
```

* **show-queue-status**



  This command will return the status of the queue server.

```text
$ dcc diag queues show-queue-status

---
status:
  ping: true
```

* **worker**

This command lists out options like worker active jobs, worker broker status, worker queues, worker registered tasks, worker reserved jobs and worker information.

```text
$ dcc diag worker

Usage: dcc diag worker [OPTIONS] COMMAND [ARGS]...

  DNIF worker stats

Options:
  --help  Show this message and exit.

Commands:
  show-active-jobs       return the worker active jobs
  show-queues            return the worker queues
  show-registered-tasks  return the worker registered tasks
  show-reserved-jobs     return the worker reserved jobs
  show-schduled          return the worker scheduled
  show-stats             return the worker info
  show-status            return the broker status
```

* **show-active-jobs**

  This command will return the worker active jobs.

```text
$ dcc diag worker show-active-jobs

---
celery@master-node: []
```

* **show-status**

  This command will return the worker broker status.

```text
$ dcc diag worker show-status

---
celery@master-node:
  ok: pong
```

* **show-queues**

  This command will return the worker queues.

```text
 $ dcc diag worker show-queues

---
celery@master-node:
- alias: null
  auto_delete: false
  binding_arguments: null
  bindings: []
  durable: true
  exchange:
    arguments: null
    auto_delete: false
    delivery_mode: 2
    durable: true
    name: celery
    passive: false
    type: direct
  exclusive: false
  name: celery
  no_ack: false
  queue_arguments: null
  routing_key: celery
```

* **show-registered-tasks**

  This command will return the worker registered tasks.

```text
$ dcc diag worker show-registered-tasks

---
celery@master-node:
- WebApp.dnifApp.long_search
```

* **show-reserved-jobs**

  This command will return the worker reserved jobs.

```text
$ dcc diag worker show-reserved-jobs

---
celery@master-node: []
```

* **show-schduled**

  This command will return the worker jobs which are scheduled.

```text
$ dcc diag worker show-schduled

---
celery@master-node: []
```

* **show-stats**

  This command will return the worker information.

```text
$ dcc diag worker show-stats

---
celery@master-node:
  broker:
    alternates: []
    connect_timeout: 4
    failover_strategy: round-robin
    heartbeat: null
    hostname: localhost
    insist: false
    login_method: null
    port: 6379
    ssl: false
    transport: redis
    transport_options: {}
    uri_prefix: null
    userid: null
    virtual_host: '0'
  clock: '109760'
  pid: 244
  pool:
    max-concurrency: 4
    max-tasks-per-child: N/A
    processes:
    - 415
    - 416
    - 417
    - 418
    put-guarded-by-semaphore: false
    timeouts:
    - 0
    - 0
    writes:
      all: ''
      avg: 0.00%
      inqueues:
        active: 0
        total: 4
      raw: ''
      total: 0
  prefetch_count: 16
  rusage:
    idrss: 0
    inblock: 6888
    isrss: 0
    ixrss: 0
    majflt: 63
    maxrss: 94068
    minflt: 47104
    msgrcv: 0
    msgsnd: 0
    nivcsw: 110794
    nsignals: 0
    nswap: 0
    nvcsw: 260011
    oublock: 8
    stime: 20.752
    utime: 73.728
  total: {}
```

**service**

This command provides a list of options for Datastore services management and option for to check the Datastore services logs.

```text
$ dcc service

Usage: dcc service [OPTIONS] COMMAND [ARGS]...

  service stats

Options:
  --help  Show this message and exit.

Commands:
  list            used to retrive list of allowed services
  restart         used to restart service
  set-log-level   used to set log level
  show-eps-count  used to tail all running listener services log
  start           used to start service
  status          returns service status
  stop            used to stop service
  tail            used to tail service log
```

* **list**

  This command will retrieves the list of Datastore services.

```text
$ dcc service list

---
services:
- service-report-engine
- ds-thread-pool
- service-ingest:01
- service-ingest:02
- service-ingest:03
- service-ingest:04
- service-api
- service-rpc
- service-ohw
- service-events-collector
- service-rpc-api
- service-eventstore
- listener-ad
- service-nameserver
- service-emaild
- service-sshd
```

* **status**

  This command is used to check status of Datastore services.

  * Syntax

    `$ dcc service status {service_name}`

```text
$ dcc service status service-ingest

---
service-ingest:01:
  pid: '295'
  status: RUNNING
  uptime: 4 days, 0:21:53
service-ingest:02:
  pid: '293'
  status: RUNNING
  uptime: 4 days, 0:21:53
service-ingest:03:
  pid: '294'
  status: RUNNING
  uptime: 4 days, 0:21:53
service-ingest:04:
  pid: '296'
  status: RUNNING
  uptime: 4 days, 0:21:53
```

* **start**

  This command is used to start Datastore services.

  * Syntax

          `$ dcc service start {service_name}`

```text
$ dcc service start service-ingest

---
service-ingest:01:
  Status: started
service-ingest:02:
  Status: started
service-ingest:03:
  Status: started
service-ingest:04:
  Status: started
```

* **stop**

  This command is used to stop Datastore services.

  * Syntax

    `$ dcc service stop {service_name}`

```text
$ dcc service stop service-ingest

---
service-ingest:01:
  Status: stopped
service-ingest:02:
  Status: stopped
service-ingest:03:
  Status: stopped
service-ingest:04:
  Status: stopped
```

* **restart**

  This command is used to restart Datastore services..

  * Syntax

    `$ dcc service restart {service_name}`

```text
$ dcc service restart ingestor

---
service-ingest:01:
  status:
  - stopped
  - started
service-ingest:02:
  status:
  - stopped
  - started
service-ingest:03:
  status:
  - stopped
  - started
service-ingest:04:
  status:
  - stopped
  - started
```

* **tail**

  This command ****print the last 10 number of lines of log of the given service.

  * Syntax

    `$ dcc service tail {service_name}`

```text
$ dcc service tail service-ingest

service-ingest:
- '2019-05-04 05:48:22,715 INFO ingestor: Compression in percentage 50.7563590209'
- '2019-05-04 05:48:22,731 INFO ingestor: ! Time taken to process data 0.078134059906
  !'
- '2019-05-04 05:48:28,160 INFO ingestor: Compression in percentage 57.5891870768'
- '2019-05-04 05:48:28,173 INFO ingestor: ! Time taken to process data 0.0370299816132
  !'
- '2019-05-04 05:48:28,200 INFO ingestor: Compression in percentage 50.2497170661'
- '2019-05-04 05:48:28,214 INFO ingestor: ! Time taken to process data 0.0411322116852
  !'
- '2019-05-04 05:48:33,717 INFO ingestor: Compression in percentage 56.030395119'
- '2019-05-04 05:48:33,747 INFO ingestor: ! Time taken to process data 0.0715749263763
  !'
- '2019-05-04 05:48:33,792 INFO ingestor: Compression in percentage 49.9728932832'
- '2019-05-04 05:48:33,819 INFO ingestor: ! Time taken to process data 0.0689098834991
  !'
```

* **set-log-level**

  This command is used to set log level as per your requirement.

  * Syntax

    `$ dcc service set-log-level {service_name} {level_name}`

```text
$ dcc service set-log-level service-ingest info

---
service-ingest: set loglevel 1
```

| level name | level number |
| :--- | :--- |
| debug | 0 |
| info | 1 |
| warning | 4 |
| error | 5 |
| critical | 6 |

**setup**

This command provides a list of options like backup and restore management, functionality of change port and password of dcc admin and reset Datastore configuration

```text
$ dcc setup

Usage: dcc setup [OPTIONS] COMMAND [ARGS]...

  Creates backup and restore of configuarations

Options:
  --help  Show this message and exit.

Commands:
  backup                     Creates backup
  backup-list                Returns backup list
  change-dcc-admin-password  Change DCC Admin Password
  change-dcc-port            Change Flask API Port
  reset-statics              Resets the system to default configs
  restore                    Restores backup file based on date
```

* **backup**

  This command takes the backup of Datastore configuration files and stores it into _config\_backup_ folder which is available under deploy key folder.

```text
$ dcc setup backup

File backup comepleted. Config backed up successfully.
 Csltuconfig backed up successfully.
```

* **backup-list**

  This command will return the list of backups which were created as a result of backup command.

```text
$ dcc setup backup-list 

---
result:
- - csltuconfig_D2019-06-25T12:20:40
  - config_D2019-06-25T12:20:40
```

* **restore**

  This command is used to restore the configuration files created during backup.

```text
$ dcc setup restore

Write foldername after doing backup-list.: csltuconfig_D2019-06-25T12:20:40
csltuconfig contains the following files.
['Pyro', 'din', 'dataprocessor', 'Ecurator', 'importstore', 'CRFList', 'correlator', 'NameServer', 'DSAPI', 'WebAPI', 'notifemail', 'agent29', 'DSinline', 'system', 'ingestor', 'nameserverstat']
Enter the filename to be restored from csltuconfig_D2019-06-25T12:20:40: system  
File system restored.
```

> **Note: In above command you need to provide input like folder and file name which you want to restore.**

* **reset-statics**

  This command is used to reset the configuration files back to default.

```text
$ dcc setup reset-statics

---
status: Resetting statics complete!
```

* **change-dcc-port**

  This command is used to change default port of your DCC.

```
$ dcc setup change-dcc-port

Please enter the new DCC API port: 80
```

* **change-dcc-admin-password**

  This command is used to change default password of your DCC.

```text
$ dcc setup change-dcc-admin-password

Please enter the username: dccmin
Please enter the new password: 
Please re-enter the new password: 
Password changed successfully.
```

**system**

This command list out options to check cpu, memory and disk utilisation, DNIF system information, IP address and network status, system package information, process status, system status and DNIF version details.

```text
$ dcc system

Usage: dcc system [OPTIONS] COMMAND [ARGS]...

  DNIF system stats

Options:
  --help  Show this message and exit.

Commands:
  cpu           return cpu status
  disk          return disk status
  dnif-version  return DNIF version
  info          return DNIF system information
  interface     return addresses associated to each NIC
  memory        return memory status
  network       return network status
  network-io    return network io associated to each NIC
  processes     return process status
  status        return system status
```

* **cpu**

  This command will return the CPU utilisation of the Datastore.

```text
$ dcc system cpu

---
cpu:
  cpu_percent:
    cpu_percent: 100.0
  cpu_stats:
    ctx_switches: 243397732840
    interrupts: 4015312878
    soft_interrupts: 22735695438
    syscalls: 0
  cpu_times:
    guest: 0.0
    guest_nice: 0.0
    idle: 45190542.89
    iowait: 9295.94
    irq: 0.0
    nice: 139.97
    softirq: 921.66
    steal: 8076228909.48
    system: 98776.29
    user: 287369.82
```

* **disk**

  This command will return the disk utilisation of the Datastore.

```text
$ dcc system disk

---
disk:
  disk_io_counters:
    busy_time: 36976408
    read_bytes: 189067558912
    read_count: 9623098
    read_merged_count: 3050
    read_time: 26769356
    write_bytes: 757819215872
    write_count: 32819258
    write_merged_count: 37084703
    write_time: 109882028
  disk_partitions:
  - device: /dev/xvda1
    fstype: /dnif
    mountpoint: /dnif
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/resolv.conf
    mountpoint: /etc/resolv.conf
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/hostname
    mountpoint: /etc/hostname
    opts: rw,relatime,discard,data=ordered
  - device: /dev/xvda1
    fstype: /etc/hosts
    mountpoint: /etc/hosts
    opts: rw,relatime,discard,data=ordered
  disk_usage:
    free: 255.44 MB
    percent: 96.8
    total: 7.69 GB
    used: 7.43 GB
```

* **memory**

  This command will return the memory utilisation of the Datastore.

```text
$ dcc system memory

---
memory:
  swap_memory:
    free: 0.0 Byte
    percent: 0
    sin: 0.0 Byte
    sout: 0.0 Byte
    total: 0.0 Byte
    used: 0.0 Byte
  virtual_memory:
    active: 1.11 GB
    available: 857.61 MB
    buffers: 205.96 MB
    cached: 443.84 MB
    free: 537.57 MB
    inactive: 214.59 MB
    percent: 78.3
    shared: 41.24 MB
    slab: 225.20 MB
    total: 3.86 GB
    used: 2.70 GB
```

* **info**

  This command will return DNIF system information.

```text
$ dcc system info

---
dnif_file:
      clusterName: GLOBAL-REPO
      deployKey: MX76IH
      systemClassType: DS
      systemID: AV9ToTGFgSGP0Rd3h8Zl
      systemName: GLOBAL-DS
```

* **interface**

  This command will return IP addresses associated to each NIC.

```text
$ dcc system interface

---
ip_address:
  docker0:
  - address: 172.17.0.1
    broadcast: 172.17.0.1
    family: AddressFamily.AF_INET
    netmask: 255.255.0.0
    ptp: None
  - address: 02:42:59:51:47:c3
    broadcast: ff:ff:ff:ff:ff:ff
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
  eth0:
  - address: 172.31.17.48
    broadcast: 172.31.31.255
    family: AddressFamily.AF_INET
    netmask: 255.255.240.0
    ptp: None
  - address: fe80::b3:deff:fe4c:dcd4%eth0
    broadcast: None
    family: AddressFamily.AF_INET6
    netmask: 'ffff:ffff:ffff:ffff::'
    ptp: None
  - address: 02:b3:de:4c:dc:d4
    broadcast: ff:ff:ff:ff:ff:ff
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
  lo:
  - address: 127.0.0.1
    broadcast: None
    family: AddressFamily.AF_INET
    netmask: 255.0.0.0
    ptp: None
  - address: ::1
    broadcast: None
    family: AddressFamily.AF_INET6
    netmask: ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff
    ptp: None
  - address: 00:00:00:00:00:00
    broadcast: None
    family: AddressFamily.AF_PACKET
    netmask: None
    ptp: None
```

* **network**

  This command will return network status.

```text
$ dcc system network

---
network:
  nicstats:
    docker0:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '1500'
      speed: '0'
    eth0:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '9001'
      speed: '0'
    lo:
      duplex: NicDuplex.NIC_DUPLEX_UNKNOWN
      isup: 'True'
      mtu: '65536'
      speed: '0'
```

* **network-io**

  This command will return network io associated to each NIC.

```text
$ dcc system network-io

---
interface IO:
  docker0:
  - bytes_recv: 0
    bytes_sent: 0
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 0
    packets_sent: 0
  eth0:
  - bytes_recv: 94393598651
    bytes_sent: 92962830147
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 254694788
    packets_sent: 227510879
  lo:
  - bytes_recv: 9422003310851
    bytes_sent: 9422003310851
    dropin: 0
    dropout: 0
    errin: 0
    errout: 0
    packets_recv: 136707350728
    packets_sent: 136707350728
```

* **processes**

  This command will return status of system processes for Datastore.

```text
$ dcc system processes

---
process:
  process_list:
  - create_time: '2019-06-21 11:46:01'
    memory_percent: 0.05151922379021293
    name: bash
    pid: 1
    ppid: 0
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:46:02'
    memory_percent: 0.0575512250401227
    name: rsyslogd
    pid: 45
    ppid: 1
    status: sleeping
    terminal: null
    username: syslog
  - create_time: '2019-06-21 11:46:04'
    memory_percent: 0.23346811395142558
    name: redis-server
    pid: 105
    ppid: 1
    status: running
    terminal: null
    username: redis
  - create_time: '2019-06-21 11:46:18'
    memory_percent: 0.0583423071712584
    name: cron
    pid: 229
    ppid: 1
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:31'
    memory_percent: 0.44102828810815675
    name: supervisord
    pid: 285
    ppid: 1
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:32'
    memory_percent: 0.6775618453177332
    name: python
    pid: 288
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:32'
    memory_percent: 0.3353199383351479
    name: python
    pid: 289
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:32'
    memory_percent: 2.1467991333695253
    name: celery
    pid: 290
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:32'
    memory_percent: 0.6959545048666383
    name: python
    pid: 291
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:33'
    memory_percent: 1.3166573220089925
    name: python
    pid: 297
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:33'
    memory_percent: 0.515785549500481
    name: python
    pid: 298
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:33'
    memory_percent: 0.09957746325670715
    name: sshd
    pid: 299
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:33'
    memory_percent: 0.4993705952794152
    name: python
    pid: 300
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:33'
    memory_percent: 1.3547281495698984
    name: python
    pid: 301
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:33'
    memory_percent: 0.39287116337527056
    name: python
    pid: 302
    ppid: 285
    status: running
    terminal: null
    username: root
  - create_time: '2019-06-21 11:47:41'
    memory_percent: 0.45734435706283066
    name: python
    pid: 370
    ppid: 288
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:48:02'
    memory_percent: 2.0589890168134617
    name: celery
    pid: 463
    ppid: 290
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:48:03'
    memory_percent: 2.0879623998663073
    name: celery
    pid: 464
    ppid: 290
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:48:04'
    memory_percent: 2.0769861352967993
    name: celery
    pid: 465
    ppid: 290
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 11:48:05'
    memory_percent: 2.064328821198628
    name: celery
    pid: 466
    ppid: 290
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-21 12:31:33'
    memory_percent: 2.2973025088180936
    name: python
    pid: 673
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 06:18:17'
    memory_percent: 0.8992626125685151
    name: python
    pid: 18515
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 06:18:21'
    memory_percent: 0.6634212522236824
    name: python
    pid: 18521
    ppid: 18515
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 06:18:21'
    memory_percent: 0.8323172872211558
    name: python
    pid: 18525
    ppid: 18515
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 10:20:55'
    memory_percent: 0.16414954221065922
    name: sshd
    pid: 19538
    ppid: 299
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 10:21:07'
    memory_percent: 0.09265549460926971
    name: bash
    pid: 19547
    ppid: 19538
    status: sleeping
    terminal: /dev/pts/0
    username: root
  - create_time: '2019-06-25 11:22:00'
    memory_percent: 48.29872843435947
    name: java
    pid: 19948
    ppid: 1
    status: sleeping
    terminal: /dev/pts/0
    username: elasticsearch
  - create_time: '2019-06-25 12:17:59'
    memory_percent: 0.5995413701344741
    name: python
    pid: 20305
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 12:17:59'
    memory_percent: 0.596970353208283
    name: python
    pid: 20306
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 12:18:00'
    memory_percent: 0.596970353208283
    name: python
    pid: 20307
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 12:18:00'
    memory_percent: 0.5985525174705545
    name: python
    pid: 20308
    ppid: 285
    status: sleeping
    terminal: null
    username: root
  - create_time: '2019-06-25 12:58:38'
    memory_percent: 0.9147875993920535
    name: python
    pid: 20473
    ppid: 19547
    status: running
    terminal: /dev/pts/0
    username: root
```

* **status**

  This command will return up-time of Datastore and active system users.

```text
$ dcc system status

---
platorm:
  boot_time: '2018-03-23 06:36:02'
  users:
  - host: 127.0.0.1
    name: root
    pid: 19547
    started: '2019-06-25 10:20:48'
    terminal: pts/0
```

* **dnif-version**

  This command will return DNIF version details.

```text
$ dcc system dnif-version

---
version: 8.1.1
```



