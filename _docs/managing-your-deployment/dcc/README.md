---
title: DCC 
permalink: /docs/Managing-Your-Deployment/DCC/DCC/
section: Managing-Your-Deployment
sub-section: DCC
---
# DCC

DCC stands for **DNIF Command and Control.**

DCC is a command line / API utility that provides,

* Diagnostics 
* Service management 
* Setup / reconfiguration 
* Backup / Index management

> **This feature is available only for DNIF version &gt;= 7.3.3.**



