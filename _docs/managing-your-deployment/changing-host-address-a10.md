---
title: Changing Host Address - A10
permalink: /docs/Managing-Your-Deployment/Changing-Host-Address-A10/
section: Managing-Your-Deployment
---

# Changing Host Address - A10

There are times when you might have installed your A10 on a specific host address, and then you are required to change your host address. 

Steps below take you through the steps to change your host address.

* Change your directory to `/dnif` considering this is where your installation was carried out, you should see a file called `docker-compose.yml` in the folder.

```text
$ cd /dnif
```

* Bring down your docker container

```text
$ sudo docker-compose down
```

* Change your directory to `{Deploy-Key}/csltuconfig`, the deploy key was used in the `docker-compose.yml` it should look something like `bkw9uyhP7GqlAjMP`.

```text
$ cd bkw9uyhP7GqlAjMP/csltuconfig
```

* Edit the `Pyro` file and replace `host` and `nhost` fields with the new host address.

```text
{"Pyro": {"nameserver": {"nhost": "192.168.86.54", "host": "192.168.86.54", "key": "n342@duush.com"}}}
```

* Edit the `system` file and replace the `localip` with the new host address.

```text
{"system": {"localip": "192.168.86.54", "retention": 45, "systemid": "AWhrlUg3M4U-pGX7BlJx", "ScopeList": ["AWhrlUg6M4U-pGX7BlJz"], "shardcount": 1}}
```

This procedure is only applicable to the A10

