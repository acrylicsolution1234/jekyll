---
title: Restoring Indexes 
permalink: /docs/Managing-Your-Deployment/Restoring-Indexes/
section: Managing-Your-Deployment
---

# Restoring Indexes

### DNIF data backup restore on DNIF A10 Setup

For to backup restore we need to setup a A10 Free Tier \(100 GB\) setup so that we can restore that backup and verify it.

Refer links for [DNIF signup](https://dnif.it/docs/guides/getting-started/signup.html) and [DNIF A10 setup](https://docs.dnif.it/admin-guide/~/edit/drafts/-Lb75fwbcnBjRzdkNcYT/quick-start-guide/installing-dnif) deployment.

Once the A10 Setup is ready, copy the required data index folder from backup server to A10 server. 

Move the data index folder to `/dnif/{Deploy-Key}/data/nodes/0/indices/` location.

Login into [DNIF A10 container](https://docs.dnif.it/admin-guide/managing-your-deployment/access-dnif-container-via-ssh) and assign group and user "elasticsearch" to the index folder.

```text
 $ chown -R elasticsearch {IndexName}  
 $ chgrp -R elasticsearch {IndexName}
```

Stop and start elasticsearch service using following command,

```text
$ /etc/init.d/elasticsearch stop
$ /etc/init.d/elasticsearch start
```

Open the index for the date which is listed in es.cat.indices\(\) for to retrieve the data using following commands,

```text
$ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.cat.health() 
u'1553843124 07:05:24 NET-MON green 1 1 119 119 0 0 0 \n'
>>> m = es.cat.indices()
>>> for i in m.split('\n'):
...     print i
... 
      close dsdb-20190106                                          
green open  ctdp_conf            1 0   18758   920 114.1mb 114.1mb 
      close dsdb-20180916                                          
green open  dsdb-20190215        1 0  105411     0  47.7mb  47.7mb 
green open  dsdb-20181201        1 0   70188     0  28.7mb  28.7mb 
>>> es.indices.open(dsdb-20190106)
{u'acknowledged': True}
```

List out the indices, the required index should be green, check the status using following commands,

```text
  $ python
>>> from elasticsearch import Elasticsearch
>>> es = Elasticsearch([{'port':8240}])
>>> es.cat.health() 
u'1553843124 07:05:24 NET-MON green 1 1 119 119 0 0 0 \n'
>>> m = es.cat.indices()
>>> for i in m.split('\n'):
...     print i
... 
green open  dsdb-20190106        1 0   74046     0  29.6mb  29.6mb 
green open  ctdp_conf            1 0   18758   920 114.1mb 114.1mb 
      close dsdb-20180916                                          
green open  dsdb-20190215        1 0  105411     0  47.7mb  47.7mb 
green open  dsdb-20181201        1 0   70188     0  28.7mb  28.7mb
```

If restored backup index is in green state, retrieved the backup data from the DNIF A10 setup console.



