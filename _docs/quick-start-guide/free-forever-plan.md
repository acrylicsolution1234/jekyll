---
title: Free Forever Plan 
permalink: /docs/Quick-Start-Guide/Free-Forever-Plan/
description: 'We mean what we say, it is truly free forever.'
section: Quick-Start-Guide
---

# Free Forever Plan

> You get the _**full platform without any feature limitation**_ on the free forever plan.

## Everything this includes \(but not limited to\) -

* Ability to use all query directives
* Download and use any of the available plugins \(enrichment/ lookup/ trigger/ ml\)
* No limitation on building workbooks or modules
* Access to all public packages available on the DNIF Cloud
* Ability to create and contribute to DNIF Cloud as well
* Access to connectors and parsers through DNIF Cloud

## The only three limitations that are -

1. You can only deploy the A10 version \(All-in-One\)
2. 100Gb of usage comes free every month with the free forever plan
3. Free Forever does not include email or telephonic support

The usage counter \(obviously\) resets at the end of every month.

## Signing-Up

* Start by clicking on the "[Free Forever](https://dnif.it/signup.html?plan=freeforever)" plan on the [DNIF Website](https://dnif.it)

![Free Forever signup link on the DNIF website.](../.gitbook/assets/free-forever-link.png)

* Setup your account, use a unique string identifier in the `Scope Name` field, you should receive an email to validate your email address

![Signup form, your email address will also become your username.](../.gitbook/assets/sign-up-form.png)

* Check your registered email address for a confirmation email from DNIF, click on the `Activate Account` link to validate your email address

![Validate your email address.](../.gitbook/assets/activate-account.png)

* Setup your quota configuration, pick `Free Tier` from the `Select Plan` option
* In the `quota configuration` select `Stop indexing data after I reach 100GB`

![Configure how your setup should behave after you exhaust your quota.](../.gitbook/assets/free-tier-setup.png)

* `Install DNIF` - contains your license file, you will need this before you install
* `Sign in to the DNIF Web Console` - contains your authentication to the DNIF Console

![Emails you should receive after you validate your email address.](../.gitbook/assets/emails-from-dnif.png)

This completes your sign up process, you now have the authentication parameters for the web console and the license files to continue with the installation.

