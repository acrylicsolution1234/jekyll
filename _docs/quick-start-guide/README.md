---
title: Quick Start Guide 
permalink: /docs/Quick-Start-Guide/Quick-Start-Guide/
redirect_from: /docs/index.html
section: Quick-Start-Guide
description: 'Thank you for choosing to work with DNIF, welcome aboard!'
---

# Quick Start Guide

If you'd like to get started with the "free forever" plan, this is where you can get started.

1. [Sign-up](free-forever-plan.md) for the "free forever" plan
2. Setup docker and [install DNIF](installing-dnif.md)
3. Setting up licensing and [signing in]()



