---
title: Using the Web Console 
permalink: /docs/Quick-Start-Guide/Using-the-Web-Console/
section: Quick-Start-Guide
---
# Using the Web Console

## One Time Password

You should have received an email titled _Sign in to the DNIF Web Console._ In this email, you will find an **Auth Secret** which should look something like `MPWDPVLZIWOCDMTA`. We will use this auth secret to generate our One Time Password \(OTP\).

![Installing Authenticator from the Chrome Web Store](../.gitbook/assets/authenticator.gif)

## Signing-in

The DNIF Web Console is available centrally at [go.dnif.it](https://go.dnif.it) Use your authentication details and your OTP to sign in. The OTP is valid for _30 seconds_ and the authentication process has to be completed before the OTP expires.

![Login to the DNIF WebConsole using the OTP](../.gitbook/assets/godnifit-login.gif)

## Connecting with the Datastore

After you sign in, you need to connect to your datastore. You will need the local IP address of your datastore, which in this example is `192.168.86.54`.

* Click on **Management** &gt; **Connections** you should find your datastore listed there, click on the default IP address to edit. Save the new address.
* You will find a **red link icon** next to the refresh icon. Click on this red link to accept the self signed certificate from your datastore. This will ensure that all transactions between your workstation and the datastore remains encrypted.

![Connecting to your Datastore](../.gitbook/assets/ds-connect%20%283%29.gif)

Next, it might be a good idea to load up a sample dataset and get your hands dirty.

