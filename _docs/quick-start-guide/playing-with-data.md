---
title:  Playing with Data 
permalink: /docs/Quick-Start-Guide/Playing-with-Data/
description: Time to get our hands dirty with some data.
section: Quick-Start-Guide
---

# Playing with Data

We begin with downloading sample Apache Web-server data. We have it parsed out in a `csv` format \([download](https://dnif.it/artifacts/weblogs.csv)\). The idea is to load up this data in a custom _event store_ and start working with it. 

## Create an Event Store

First, we create an event store to hold the sample data we have. By default, all the ingested data is placed in the `event` index. However, for this demo, we will create an event store called `weblogs`.

![Create an Event Store and Upload Sample Data](../.gitbook/assets/create-event-store.gif)

## Create a Package

Next, we create a private package in the package repository. Think of this as an empty folder to hold entities like widgets, dashboards, workbooks, templates, etc. This private package is local \(at the moment\) and entities within this package are applied to the current scope which in our case is `BLUEMOUNTAIN-REPO`.

![Setup a new Package in the Package Repository](../.gitbook/assets/create-package.gif)

## Build a Widget

Let's create a visual with our data. Here, we create a pie chart to show distribution of the countries we are receiving events from. The query for this is as given below -

```text
_fetch * from weblogs group count_unique $SrcCN limit 100
>>_checkif str_compare $SrcCN eq '-' exclude
```

![Create a new Pie Chart](../.gitbook/assets/create-widget.gif)

## Setup your Dashboard

Finally, we create our dashboard and add our new widget to it. The dashboard can house several such widgets with each widget refreshing at an individual time interval.

![Build a new Dashboard](../.gitbook/assets/setup-dashboard.gif)

## Playing with Data

Here is a list of queries you can try in the search interface.

* Get a list of top Source IP Addresses

```text
_fetch * from weblogs group count_unique $SrcIP limit 100
```

* Fetch a list of top countries and their AS Numbers

```text
_fetch * from weblogs group count_unique $SrcCN, $SrcAS limit 100
```

* Get a list of URLs that don't exist on the website

```text
_fetch * from weblogs where $HTTPRetCode=404 group count_unique $URL limit 100
```



