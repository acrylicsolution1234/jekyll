---
title: Installing DNIF 
permalink: /docs/Quick-Start-Guide/Installing-DNIF/
description: 'Install docker, install DNIF and you are ready to roll ~'
section: Quick-Start-Guide
---

# Installing DNIF

All DNIF Components come in the form of [docker](https://www.docker.com/) containers \(more on [why docker](https://www.docker.com/why-docker)\). Oh, we did docker just so that it's easy for you to install and maintain. In this guide, we are going to install docker and DNIF on a standard [Ubuntu Server 16.04 LTS](http://releases.ubuntu.com/16.04/ubuntu-16.04.5-server-amd64.iso) version installed with OpenSSH.


Installing Ubuntu is out the scope of this document, however for this guide we will allocate all available space to the `/` \(root\) partition.

## Installing Docker

Setting up docker on the host can be customised. You can lookup [docker instructions](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce) for installing the software, or just follow us through this page to get started.

* Updating your operating system, `apt-get update` will download all the latest in package repositories and get you loaded up. You might be prompted for your password.

```text
$ sudo apt-get update
```

* Install required packages to get started.

```text
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
```

* Add dockers official GPG key to your repository.

```text
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

* Validate your key, ensure the outcome is the same as below.

```text
$ sudo apt-key fingerprint 0EBFCD88
pub   4096R/0EBFCD88 2017-02-22
      Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid                  Docker Release (CE deb) <docker@docker.com>
sub   4096R/F273FCD8 2017-02-22
```

* Add the repository to your host.

```text
$ sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
```

* Update your packages, again.

```text
$ sudo apt-get update
```

* Finally, you are ready to install `docker-ce`

```text
$ sudo apt-get install docker-ce
```

At this point you have successfully installed docker on your host. Next, you should try the customary `hello world` to test if everything is working at the docker end. If you don't see any errors, you are good to go.

```text
$ sudo docker run hello-world
```

## Installing DNIF

* Let's begin by setting up a home folder, this is where all the data and configurations are going to be stored. This folder will hold everything and will be mapped with the docker A10 container.

```text
$ sudo mkdir /dnif
$ cd /dnif
```

* Just trying to make things easy, we built out a `docker-compose` file that can help you configure the image, but first we will install `docker-compose` and for doing that that we install `python-pip` and then straighten our `locales` settings.

```text
$ sudo apt-get install -y python-pip
```

* Install `docker-compose`

```text
$ sudo pip install docker-compose
```

* Create an A10 container image configuration or the docker-compose file.

```text
$ sudo nano docker-compose.yml
```

* Contents of `docker-compose.yml` should be

```text
version: '2.0'
services:
 comp-ship-a10:
   image: "dnif/ship-a10:latest"
   network_mode: "host"
   cap_add:
    - NET_ADMIN
   ports:
    - "127.0.0.1:826:22/tcp"
    - "443:443/tcp"
    - "8350:8350/tcp"
    - "8351:8351/tcp"
    - "8355:8355/tcp"
    - "8340:8340/tcp"
    - "9234:9234/tcp"
    - "9236:9236/tcp"
    - "514:514/udp"
    - "514:514/tcp"
    - "8362:8362/tcp"
   volumes:
    - /dnif:/dnif
   environment:
    - "DKEY=DEPLOY_KEY"
    - "CRIP=127.0.0.1"
    - "bootstrap_memory_lock=true"
   ulimits:
    memlock:
     soft: -1
     hard: -1
   container_name: comp-dnif-a10
```

Be sure to replace `DEPLOY_KEY` with the Deploy Key supplied in the email. It should look something like `bkw9uyhP7GqlAjMP`


**Note** - we are using `/dnif` as the installation folder, if you did go off the guide, make sure to make those adjustments to the `docker-compose.yml` file.

#### Increase the vm.max\_map\_count parameter

* If you are using a VM, the default operating system limits on mmap counts is likely to be too low, which may result in out of memory exceptions. You may need to increase the `vm.max_map_count`parameter.
* You can increase the limits by running the following command as `root`

```text
$ sysctl -w vm.max_map_count=262144
```

* To set this value permanently, update the `vm.max_map_count` setting in `/etc/sysctl.conf`

```text
$ vim /etc/sysctl.conf
vm.max_map_count=262144
```

* Reload the config as root

```text
$ sysctl -p
```

* Check the new value using the following command

```text
$ cat /proc/sys/vm/max_map_count
```

* Run `docker-compose` to download and start the image

```text
$ sudo docker-compose up -d
```

* Finally, ensure that our container has started up. The following command should give you a list of all the running containers and you should see your container in the `running` state.

```text
$ docker ps
```

Once you have your container booted up, head out to the [Getting Started]() page to log into the DNIF Console. If you did have issues getting your container started, head over to the [troubleshooting]() section for assistance. You could also read up on common issues at the [DNIF Forum](https://stack.dnif.it). 

