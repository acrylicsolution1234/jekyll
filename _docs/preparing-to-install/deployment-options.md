---
title: Deployment Options 
permalink: /docs/Preparing-to-Install/Deployment-Options/
section: Preparing-to-Install
---

# Deployment Options

DNIF deployment is easy and scaling up or scaling down based on the demands of the environment is easier. DNIF was designed with flexibility with integrations in mind.

### Standalone Deployment

If you are experimenting with or testing DNIF and if your data volume is _below 1.5 TB per month_, then the standalone deployment is what you're looking for. The free [forever plan](https://dnif.it/signup.html?plan=freeforever) \(free tier\) is available in the standalone deployment model only.

![Standalone Deployment](../.gitbook/assets/stand-alone-deployment%20%281%29.png)

A10 stands for All-in-One which includes all three \([adapter](../components/adapter-ad.md), [datastore](../components/datastore-ds.md) and [correlator](../components/correlator-cr.md)\) components required by the platform, integrated into a single instance.

All upgrades from the standalone deployment to the standard or enterprise deployment will require a full reinstall and migration, while the other models can easily be upgraded or extended.

The A10 setup allows you to have a retention policy, but it does not have the capability to provide any in-product redundancy or high availability.

### **Distributed Deployment**

If you have tried the product and are ready to go mainstream, the distributed deployment that is tiered, will allow you to grow based on your needs.

![Standard Deployment](../.gitbook/assets/standard-deployment.png)

The distributed installation includes a single instance of each component making the deployment tiered and distributed. This deployment model can be scaled out by adding additional components to the cluster and growing the overall capacity of the setup. 

This model also allows you to distribute the components across locations to achieve shared or aggregated topologies. Additionally, it allows you to scale effectively in any enterprise requirement. 

The distributed deployment does not provide in-product redundancy or high availability.

The datastore can be scaled by adding new datastore components and building a cluster. This change does not require downtime, but just reconfiguration.

### Enterprise Deployment

If you are considering scaling your standard deployment and have a bump in your log volume, need redundancy and want to watch compliance using DNIF, Enterprise deployment is your best option.

![Horizontally scalable enterprise deployment](../.gitbook/assets/distributed-setup.png)

The Enterprise Deployment includes multiple Adapters that are load balanced to ensure high throughput and fault tolerance. This deployment also includes a cluster of Datastores that can scale as you need more storage and performance. 

The datastore cluster can provide inbuilt redundancy at the software level, this can withstand a certain degree of failure within the cluster.

Larger number of datastores also positively impact the performance, higher the number of datastores better the performance. Performance is calculated by the amount of data each datastore manages in the cluster, more compute per data volume, the better the performance.

### **DEPLOYMENT ARCHITECTURES**

DNIF was designed to support complex architectures. It has the capability to quickly deploy simple single location environments while also being able to scale out to complex service provider scenarios without compromising on the features or the capability of the platform.

#### **ON PREMISE DEPLOYMENT**

Customers requiring to aggregate and process events without having to transmit data to a third party location \(or provider\) are required to have an on premise \(on-prem\) deployment.

![](../.gitbook/assets/image%20%2848%29.png)

DNIF is able to provide a configuration where all components are installed within the infrastructure and the analysts are able to process data locally. This on-prem solution could be either a standalone, standard or enterprise deployment \(see Deployment Options for different configurations\).

A similar setup could also be ported to a public or private cloud. The on-prem architecture provides an unrestricted environment that you can control.

#### **AGGREGATED DEPLOYMENTS**

In many cases, you would like to aggregate your data to a central location, maybe your datacenter or your analytics hub. In these situations, you would like to keep deployments light on the remote \(branch\) locations, while shipping log events to the central hub.  


![](../.gitbook/assets/image%20%2866%29.png)

DNIF has the capability to aggregate data across multiple adapters and index data into a single datastore. All the data can be made available under a single scope or be indexed under multiple scopes. Irrespective of the indexing configuration, all the data can be analyzed from a single console window.

#### **DISTRIBUTED DEPLOYMENTS**

Enterprise environments that are spread across multiple geographical locations require to aggregate data locally while searching and analyzing data at a central operations center.

![](../.gitbook/assets/image%20%2853%29.png)

DNIF enables you to aggregate data from multiple locations \(deployments\) into a single operations console. The subscription model also allows you to deploy multiple datastores without having to worry about unit cost of correlation engines. Each branch location can be deployed as standalone, standard or enterprise deployments options, allowing it to scale down or up depending upon the data requirements of the environment.

The operations staff must have direct access to the datastore \(or A10 device\) in order to fetch data to the screen. This model is also an ideal option for Managed Service Providers \(MSP\) where a shared operations team could remotely monitor multiple customers using a single window. This model also helps customers retain data on-premise which is a key compliance requirement.

#### **MULTI-TENANT DEPLOYMENTS**

MSPs and infrastructure providers who would like to offer analytics as a cloud service need a platform that can host multiple customers on a single infrastructure. Data access requirements can vary for different providers. Some cases require each customer to have individual access to their data, while in some cases, the MSP is required to provide a central operations center across customers.

DNIF can deploy multi-tenant architectures that enables providers to setup cloud models that have the following features:

* The service provider can have a single console across customers
* Each customer can have individual console access, being able to search, analyze, correlate, visualize and report on only their data
* The MSP can have billing setup for individual customers
* Each customer can apply custom rules, dashboards and reports
* The MSP can sync common rules, reports and dashboards across customers

![](../.gitbook/assets/image%20%289%29.png)

