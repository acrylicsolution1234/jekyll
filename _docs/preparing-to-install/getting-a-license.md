---
title: Getting a License 
permalink: /docs/Preparing-to-Install/Getting-a-License/
section: Preparing-to-Install
---

# Getting a License

## About Licenses

A license file typically comprises of following information:

* Data retention period for your setup.
* Monthly log ingestion limit.

> **Note: This file is encrypted and any tampering to this file will halt the data ingestion process for the respective setup entirely.**

For All-In-One\(A10\) setup, the **license** file will be sent to the registered email address post signing up for the FREE FOREVER plan.

For all the paid plans and distributed setups, the **license** information will be provided by DNIF support.

If you already have the license file, here is a guide to help you install the license: [Installing license](https://docs.dnif.it/admin-guide/configuration/installing-licenses).

\_\_

