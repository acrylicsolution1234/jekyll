---
title: Network Pre-Requisites 
permalink: /docs/Preparing-to-Install/Network-Pre-Requisites/
section: Preparing-to-Install
---
# Network Pre-Requisites

In this article, you will find a complete list of network requirements which you might find quite helpful in order to facilitate a trouble-free installation.

#### OUTBOUND ACCESS TO UNET \(UMBRELLA NETWORK\)

UNET provides user authentication, the ability to synchronise content from the repository, and collects usage information for billing.

Outbound access to UNET services are an absolute must, as the devices will cease to operate if connectivity isn’t established for more than 48 hours. The installed devices are required to connect via port 80 and 443.

| Protocol | Source IP | Source Port | Direction | Destination IP | Destination Port |
| :--- | :--- | :--- | :--- | :--- | :--- |
| TCP | DS, AD, CR, A10 | Any | Egress | api2.netmonastery.com \[52.72.25.136\] | 80 |
| TCP | DS, AD, CR, A10 | Any | Egress | api2.netmonastery.com \[52.72.25.136\] | 443 |

> **Note – UNET connections remain persistent for long periods and therefore any kind of connection throttling will result in the failure of the connectivity services.**

Commands to check outbound access to the UNET server are as follows:

```text
$ telnet 52.72.25.136 80
$ telnet 52.72.25.136 443
```

Press 'Ctrl + \]' and then type 'quit' to close the telnet terminal.

#### OUTBOUND ACCESS TO PUBLIC DNS

The installed devices require access to the public DNS services.

If you have a DNS Server configured already, then configure its services accordingly. Otherwise, you can find the configuration details for your reference below:

| Protocol | Source IP | Source Port | Direction | Destination IP | Destination Port |
| :--- | :--- | :--- | :--- | :--- | :--- |
| UDP | DS, AD, CR, A10 | Any | Egress | 4.2.2.2 | 53 |
| UDP | DS, AD, CR, A10 | Any | Egress | 8.8.8.8 | 53 |

You can use the _dig command_ and _host command_ to verify that your configured local or public DNS working:

**host** : host command is a utility that performs DNS lookups.

```text
$ host <Domain Name> <Public-DNS-Server-IP> 
or
$ host <Domain Name> <Local-DNS-Server-IP> 
```

**dig** : dig is a tool that is used for querying DNS nameservers for information about host addresses, mail exchanges, nameservers, and related information.

```text
$ dig @<Public-DNS-Server-IP> <Domain Name>
or
$ dig @<local-DNS-Server-IP> <Domain Name>
```

#### NTP SERVER

If you have an NTP Server configured already on your network, then configure its services accordingly. Otherwise, you can find the configuration details for your reference below:

| Protocol | Source | Source Port | Direction | Destination | Destination Port |
| :--- | :--- | :--- | :--- | :--- | :--- |
| TCP/UDP | DS, CR, AD, A10 | Any | Egress | tick.clara.net | 123 |

Check the connectivity to the NTP server from DNIF server using following command:

```text
$ ntpq -np $IP
```

#### **AD to DS and CR to DS COMMUNICATION**

Allow ports in the range 8350/TCP to 8370/TCP\(bidirectional\) on Datastore server to be accessed from other DNIF components \(applicable only if there’s a firewall between any DNIF component and Datastore\).

To check the communication between AD and DS, use the following command on the Adapter:

```text
$ telnet <DS Server IP> 8350
$ telnet <DS Server IP> 8351
$ telnet <DS Server IP> 8352
$ telnet <DS Server IP> 8355
$ telnet <DS Server IP> 8370
```

Press Ctrl + \] and then type quit to close the telnet terminal.

To check the communication between CR and DS, use the following command on the Correlator:

```text
$ telnet <DS Server IP> 8340
$ telnet <DS Server IP> 8350
$ telnet <DS Server IP> 8351
$ telnet <DS Server IP> 8355
$ telnet <DS Server IP> 8361
$ telnet <DS Server IP> 8362
```

Press Ctrl + \] and then type 'quit' to close the telnet terminal.

#### **DATASTORE CLUSTER COMMUNICATION**

Allow ports 8240 and 8340 on all Datastore nodes which are configured in the cluster. All cluster nodes should be able to locally communicate with each other and other DNIF components.

**Allow ports 514/UDP, 514/TCP, 9234/TCP \(HTTP\), 9236/TCP \(HTTPS\), 2325/TCP, 161/UDP on Adapter servers from syslog sources since syslog sources will send logs on port 514 of the Adapter server.**

> **Note: If Adapter Loadbalancer is deployed on your environment, then allow above ports on Loadbalancer server from syslog sources since syslog sources will send logs on port 514 of the loadbalancer server.**

**DNIF console \(from your system’s client browser\) accesses data via SSL port 443 of Datastore server. Hence, your system will have to give access to port 443 on the Datastore server.**

#### SMTP SERVER

You will need a local SMTP server to send out reports, alerts, and updates.

If you’re performing the DNIF Enterprise installation which entails individual component installation, SMTP configuration communication between the components must happen independently.

To check SMTP server connectivity use the following command:

```text
$ telnet <SMTP-Server-IP> <SMTP-Port>
```

Press Ctrl + \] and then type 'quit' to close the telnet terminal.

#### DOCKER PREREQUISITES <a id="docker-prerequisites"></a>

One needs to make sure that DNIF servers have access to the following mentioned links before diving into the docker installation:

```text
https://www.docker.com/community-edition
*.docker.io
*.docker.com
```

To check if the above links are accessible from the DNIF server, we can use the following commands:

```text
$ wget https://www.docker.com/community-edition
$ wget https://hub.docker.com/
```

