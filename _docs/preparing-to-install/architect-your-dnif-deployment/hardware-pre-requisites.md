---
title: Hardware Pre-Requisites 
permalink: /docs/Preparing-to-Install/Architect-your-DNIF-Deployment/Hardware-Pre-Requisites/
section: Preparing-to-Install
sub-section: Architect-your-DNIF-Deployment
---
# Hardware Pre-Requisites

The hardware required for most deployments depends upon -

* The total monthly log expected
* The number of days in hot and cold retention
* The number of replicated copies required.

In case your partner or system integrator has already provided you with a reckoner, you can use it as a guide. However, if you need your requirements to first be thoroughly assessed, contact your system integrator for help.

For the operating system, we recommend that you install the latest 64-bit\( 16.04 LTS\) version of `Ubuntu`.

> **Note - The hardware ready reckoner only provides an indicative stack required to run DNIF. You are free to start slow and upgrade your hardware as your usage builds up. DNIF is built on a big data framework and therefore it can scale in phases.**

## Checking of hardware pre-requisites 

#### Steps to be followed to check if your server is physical or virtual from command-line:

lshw \(Hardware Listener\) is a small tool that provides detailed information about the hardware configuration of the machine. It can report exact memory configuration, firmware version, mainboard configuration, CPU version and speed, cache configuration, bus speed, etc.

* lshw command output in VMware Workstation:

```text
$ lshw -class system | grep product
product: VMware Virtual Platform ()
```

* lshw command output in VirtualBox:

```text
$ lshw -class system | grep product
product: VirtualBox
```

* lshw command output in Physical Server:

```text
lshw -class system | grep product
product: HP 348 G4 (3TU24PA#ACJ)
```

#### Command to check disk space from command-line:

`df` command ****shows the amount of disk space used and available on Linux file systems.

```
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
none             58G   40G   19G  69% /
tmpfs           7.9G     0  7.9G   0% /dev
tmpfs           7.9G     0  7.9G   0% /sys/fs/cgroup
/dev/vda1        58G   40G   19G  69% /dnif
shm              64M   24K   64M   1% /dev/shm
tmpfs           7.9G     0  7.9G   0% /sys/firmware
```

#### Command to check size and availability of RAM from command-line:

You can check the RAM size you have on your machine using the following command -

```text
$ free -h
              total        used        free      shared  buff/cache   available
Mem:           7.7G        4.4G        1.2G        519M        2.0G        2.5G
Swap:          2.0G          0B        2.0G
```

#### Command to find number of CPU cores from command-line:

There might be several ways to find out the number of CPU Cores in a system. Here are a few methods -

* **nproc** is a simple Unix command to print the number of processing units available in your system. It is part of GNU Core utils, so it comes pre-installed with all modern Linux operating systems.

```text
$ nproc
4
```

As seen in the result above, the processor has 4 cores.

* The **“lscpu”** command is used to display the information about your CPU in human-readable format.

```text
$ lscpu
Architecture:        x86_64
CPU op-mode(s):      32-bit, 64-bit
Byte Order:          Little Endian
CPU(s):              4
On-line CPU(s) list: 0-3
Thread(s) per core:  2
Core(s) per socket:  2
Socket(s):           1
```

