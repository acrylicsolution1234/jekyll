---
title: Architect your DNIF Deployment 
permalink: /docs/Preparing-to-Install/Architect-your-DNIF-Deployment/Architect-your-DNIF-Deployment/
section: Preparing-to-Install
sub-section: Architect-your-DNIF-Deployment
---
# Architect your DNIF Deployment

Infrastructure plays a crucial role in analytics and if planned well, it helps organisations build their threat hunting efforts and maturity over time seamlessly with optimum ROI. Security teams in the long run can simply focus on what matters rather than solve data management and governance issues all day.

### What you need to know before you plan

* Monthly Volume calculations is an estimate:
  * Actual Volumes vary for each environment.
  * Also depends on what events you plan to collect.
  * Volumes may vary over time.
* Sizing is about finding the right balance between performance and expectation:
  * Infra requirements depend on type of use.
  * Expectation of performance plays a key role.
* Start small and scale as you grow is the best policy.

### List out your requirements before you plan

* Create a list of log sources to be integrated.
* Number of locations and spread of devices across locations.
* Network complexity - Number of Segments / Architecture.
* Categorize each device in your device list as per its use - High, Med, Low.
* Online retention period.
* High Availability requirements.
* Type of use - compliance / monitoring / threat hunting.

### Why fret over the hardware?

* Key Dependencies on hardware:
  * Performance
  * Availability of the solution
  * Scalability of the solution

### Estimating log volume based on infrastructure design 

You can calculate using two different methods:

* **Based on Device List**

  * Use log volume estimator sheet\(most device vendors have this\).
  * Account for each log source type - OS, Middleware, App as separate sources.
  * Account for category of device use - L/M/H.

* **Based on expected EPS \(Comparative Calculation\)**
  * What is the EPS number - Sustained Peak / Average EPS?
  * Average EPS ~ 0.6 x Sustained Peak EPS
  * Average Log Size - Range from 500b to 1000b
  * Monthly Log Vol \(MB\) = Avg EPS x 86.4 x 30 x Avg Log Size \(bytes\)

## Sizing of the Adapter

### Characteristics of Adapter

* Function - Real-time processing.
* Key Requirements - Fast compute and memory \(to manage bursts\).
* Processing Type - Parallel and multi-threaded.
* Storage Requirement - Minimal / Needed to cache logs \(Local Disk\).
* High Availability - Load Balancing or IP Failover clustering.
* Recommended Size:
  * Small Node \(Upto 2000 EPS\) - 4 Cores, 32GB RAM, 200GB
  * Large Node \(Upto 4000 EPS\) - 8 Cores, 64GB RAM, 500GB
* Physical Server / Virtual Server.

### Design the solution for Adapter

* Minimum Adapters needed = Sustained Peak EPS / EPS per Node \(Small / Large\)
* Key Points:
  * Place Adapters close to log sources \(Remote Locations / Network Segments\).
  * Prefer smaller and multiple adapter nodes.
  * Add Adapter nodes at each location for High Availability \(if needed\).
* Architecture for High Availability
  * Use external udp load balancer \(example - NGNIX\).
  * Configure IP failover using OS Clustering.
* Backup Requirements - Plan periodic OS / App backups.

## Sizing of the Datastore

### Characteristics of Datastore

* Function - Data processing, query response, high IOPS.
* Key requirements - Compute and fast IOPS.
* Processing type - Scale-out cluster, multi threaded.
* Storage requirement - High performance storage \(Local Disk / SAN Disk\).
* High availability - Infrastructure level or DS replica configuration.
* Recommended size:
  * Small Node \(Upto 1 - 2 TB Data\) - 4 Cores, 32GB, 1 - 2 TB Disk
  * Large Node \(Upto 3 - 5 TB Data\) - 8 Cores, 64GB, 3 - 5 TB Disk
* Physical server / Virtual server.

### Design the solution for Datastore

* Online datastore size = Monthly Log Vol x Online Retention \(Months\)
* No of DS Nodes = Online DS size / Data size per Node \(3 to 5 TB\)
* Key points:
  * Scale out nodes with increase in online data size.
  * Do not scale each DS node beyond 8 Cores, 64GB RAM.
  * Include one DS Query Node beyond 3 DS nodes for query performance.
  * DS Query node has a similar hardware config as DS Data node with upto 200GB disk space.
* High availability is considered at Infra Level \(VM Level\) with external SAN.
* In case of failure of DS node, data is cached on the Adapters till node is re-started.
* Optimize physical servers using virtualization.

### Note on Performance

* Disk configuration directly impacts query performance.
* Typical IO requirement - 300 to 500 IOPS per TB of storage.
* IOPS depends on number of users and queries.
* Recent data \(15 days\) is typically accessed frequently.
* Balance using SSD + SAS tiering for optimal performance configuration \(SAN\).
* Backup scripts can move data to a location for backup / archival.

## Sizing of the Correlator

### Characteristics of Correlator

* Function - Query processing, job scheduling, automation etc.
* Key requirements - Compute.
* Processing type - Standalone nodes.
* Storage requirements - Minimal.
* High availability - Typically stand alone VM node.
* Physical Server / Virtual instance.

> **Note: CR node requirement scales with use of ML models, SOAR etc.**

## Design the overall solution

* Enlist sizing and number of nodes for each component - AD / DS / CR.
* Note the adapters needed across locations / network segments
* Factor for High Availability.
* Provide nodes for backup / restore / UAT as needed.
* Design the infrastructure based on total nodes needed.
* Optimize using server virtualization.
* Configure the SAN \(if needed\) based on retention and IO requirements.
* Leverage automated tiering, RAID for reliability on SAN.
* Propose backup / archival solution separately if needed.

## Sample of infrastructure design

**Inputs:**

Sustained Peak EPS - 5000 \(Loc 1\), 2000 \(Loc 2\).

Online Retention - 3 months.

High Availability Needed - Yes.

**Calculations:**

Monthly Log Volume = 0.6 x 7000 x 86.4 x 30 x 700 = 8 TB \(Roundup\).

Online Data = 24 TB.

**Components of the Solution:**

Adapter \(Location 1\) - 3 Nodes \(4 Cores, 32GB RAM, 200Gb\) with HA.

Adapter \(Location 2\) - 2 Nodes \(4 Cores, 32GB RAM, 200Gb\) with HA.

Datastore - 5 Nodes \(8 Cores, 64GB RAM, 5TB Disk\) with Infra HA.

CR & Query Node - 2 Nodes \(8 Cores, 64GB RAM, 200GB Disk\).  


