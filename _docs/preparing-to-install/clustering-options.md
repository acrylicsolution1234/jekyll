---
title: Clustering Options 
permalink: /docs/Preparing-to-Install/Clustering-Options/
section: Preparing-to-Install
---

# Clustering Options

Load balanced for Adapter

HA condition for Adapter

Datastore HA using replication

Data node/query node

Data lake configuration - es cluster, head

