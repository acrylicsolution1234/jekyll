---
layout: post
title:  Release Note &#8212; v7.3.0
date:   2018-04-15 09:00:10
categories: guides

author:  jburks
---
 In this blog, we reveal software release notes for DNIF version v7.3.0 that fixes bugs & delivers a new set of features within the web console. Click here to know more.
