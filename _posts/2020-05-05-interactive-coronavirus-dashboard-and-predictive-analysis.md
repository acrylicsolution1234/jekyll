---
layout: post
title:  Interactive coronavirus dashboard and predictive analysis
date:   2020-04-06 07:45:06
author: Dhwani
categories: siem
images: images/blog/select-the-right-security-tool.png
excerpt: DNIF  provides a COVID-19 dynamic dashboard for all its customers to be well updated about the worldwide COVID-19 attack numbers.


---

The Coronavirus spares no one, regardless of the size, origin, economic factors of the country, COVID-19 since January 2020 has seen a rapid rise affecting the economy severely. As of April 7, 2020, nearly 465,915 COVID-19 cases have been [documented](https://experience.arcgis.com/experience/685d0ace521648f8a5beeeee1b9125cd), although many more mild cases have likely gone undiagnosed. The virus has killed over 21,031 people from across 199 countries or territories.

The impact of the Coronavirus over the world is defined by a single word: Lockdown. The shops across the streets, cinema halls, public group meetings, sports and other public modes of entertainment, everything has come to a stand still. The liquidity in the economy is seeing a slowdown . The organizations across the world have prompted employees to work from home. This remotely working system is brought in place to reduce social contact and avoid the spread.

With work from home in place, this time is seen as the biggest risk in the cyber security space. Due to this pandemic, we are prone to the reliance on digital tools and that in turn becomes a medium for hackers to get through the data and misuse the data.

A pandemic such as this, of which not much is known  we are bound to source information about the virus leading to outbound clicks. Additionally, we also have a lot of emails from medical centres, messages and invoices coming our way. All this can be a risk if not evaluated well. The Internet has almost instantly become the channel for effective human interaction and the primary way we work, contact and support one another.

A robust cyber security platform is an essential today considering increased dependency of the internet which will lead to increased cost of failure.

Today, with the internet being used excessively for video calls to stay in touch with friends and family, entertainment and meetings due to work from home. The data is prone to be vulnerable to cyber threat.

Therefore, it is essential to evaluate the links to the websites before clicking on those. Websites such as [Worldometer](https://experience.arcgis.com/experience/685d0ace521648f8a5beeeee1b9125cd) were also not spared from cyberattacks.

![worldometer] [PIC-1]

We have created a dashboard for all our customers to get timely updates about Coronavirus cases across the globe. This dashboard is in collaboration with the World Health Organization and other reputed health organizations. This dashboard gives detailed information about the total number of cases across the globe along with number of deaths and recoveries. It also simplifies the breakdown by giving details of the country under the same tab. The heat map generated also gives a visual representation of the cases as shown in the following screenshot.

![DNIF Covid19 Dashboard] [PIC-2]

#### Know more about integration of our Covidâ€™19 dashboard

**Here are a few steps to help you checkout this dashboard:**
- Login to your Data Store, Correlator, and A10 containers.

-  In order to access DNIF containers via SSH move to the */dnif/<Deployment-key/lookup_plugins*  folder path.

```
$cd /dnif/CnxxxxxxxxxxxxV8/lookup_plugins/
```

- Extract the covide19 files suing the command: `tar -xvzf covid19.tar.gz`
- Sync Covid 19 Package.
- Execute this query to populate the data:

```
_fetch * from event where $Duration=1h group count_unique $ScopeID limit 1
>>_lookup covid19 get_data coronavirus
>>_store in_disk covid_19 dataset stack_replace
```


>***Note***: *In case data is not populated restart given services*.


#### Predictive analysis dashboard

The dashboard below gives a country wise analysis of the COVID-19 spread today and a prediction of how far it could possibly go based on the current data. These statistics are just giving an insight of how two weeks from now the Coronavirus seems to grow. This is a little way to help you brace yourselves and make provisions accordingly.

![DNIF Preditive Dashboard] [PIC-3]

Just like how we improvised on our basic habits and routines considering the pandemic, a change in our online behaviour might help in mitigating the cyber risk.  


If thereâ€™s anything we can do to help then feel free to [reach out](https://dnif.it/docs/contact-support.html) to us at [support@dnif.it](mailto:support@dnif.it) weâ€™re always happy to help.

<a href="https://dnif.it/campaigns/five-effective-siem-use-cases.html" target="\_blank"><img src="/images/five-most-effective-siem-use-cases.png" alt="five effective siem use cases"></a>


[PIC-1]: {{baseurl}}/images/blog/worldometer-cyber-attack.jpg
[PIC-2]: {{baseurl}}/images/blog/dnif-covid19-dashboard.png
[PIc-3]: {{baseurl}}/images/blog/dnif-predictive-dashboard.png
