---
layout: post
title:  How To Integrate OSQuery And DNIF
date:   2017-04-02 11:22:06
author:  jburks
categories: guides
images: images/blog/events/fb-osquery.png
excerpt: In this blog, we reveal software release notes for DNIF version v7.3.0 that fixes bugs & delivers a new set of features within the web console. Click here to know more.
---
 
  OSQuery is an OS instrumental framework that can work with Linux, Windows, OS X(macOS) and FreeBSD platforms to perform a low-level monitoring and analytics on both performant and intuitive.